function svgReady() {
    "use strict";

    var 
        svgElement = document.querySelectorAll("svg")[0],
        circleIndicators = document.querySelectorAll("circle.initial-point-indicator"),
        scale = document.querySelectorAll("g.scale")[0],
        buttonElement = document.querySelectorAll("div.button span")[0],
        i;
    
    function toggleScale() {
        scale.classList.toggle("hidden");
        for(i=0;i<circleIndicators.length;i+=1) {
            circleIndicators[i].classList.toggle("hidden");
        }
    }
    
    buttonElement.addEventListener("click", function() {
        toggleScale();
    }, false);
    
}

document.addEventListener("DOMContentLoaded", function () {
    "use strict";
    svgReady();
}, false);
