function profileFormReady() {
    multiAccordionElement();

    var
        countries,
        states,
        cities,
        i,
        j,
        k;

    countries = ["China", "India", "United States", "Brazil", "Pakistan"];

    states = new Array(countries.length);
    states[0] = ["Hebei Province", "Shanxi Province", "Inner Mongolia Autonomous Region"];
    states[1] = ["Andhra Pradesh", "Goa", "Gujarat", "Maharashtra"];
    states[2] = ["California", "Florida", "Massachusetts", "Texas", "New York"];
    states[3] = ["Goiás", "São Paulo", "Minas Gerais"];
    states[4] = ["Balochistan", "Sindh", "Azad Kashmir", "Gilgit–Baltistan"];

    cities = new Array(states.length);
    for (i = 0; i < states.length; i += 1) {
        cities[i] = new Array(states[i].length);
    }

    cities[0][0] = ["Shijiazhuang", "Tangshan", "Baoding", "Qinhuangdao", "Zhangjiakou", "Handan", "Xingtai", "Langfang"];
    cities[0][1] = ["Taiyuan", "Datong", "Changzhi", "Yangquan"];
    cities[0][2] = ["Baotou", "Chifeng", "Ordos"];

    cities[1][0] = ["Vijaywada", "Vishakhapatnam", "Guntur", "Nellore", "Tenali"];
    cities[1][1] = ["Panjim", "Vasco da Gama", "Mapusa", "Calangute", "Baga", "Old Goa", "Candolim"];
    cities[1][2] = ["Gandhinagar", "Ahmedabad", "Kachchh", "Surat", "Dwarka"];
    cities[1][3] = ["Mumbai", "Thane", "Nagpur", "Nashik", "Pune", "Allahabad"];

    cities[2][0] = ["San Francisco", "San Diego", "San Jose", "Los Angeles", "Oakland"];
    cities[2][1] = ["Miami", "Orlando", "Daytona", "St. Petersberg", "Hollywood", "Panama City"];
    cities[2][2] = ["Boston", "Waltham", "Lowell", "Netwon", "Plymouth", "Lynn"];
    cities[2][3] = ["Houston", "Dallas", "Arlington", "Irving", "Texas City"];
    cities[2][4] = ["Newark", "Jersey City", "Atlantic City", "Hackensack", "Teaneck", "Hoboken"];

    cities[3][0] = ["Goiânia", "Aparecida de Goiânia", "Anápolis", "Luziânia", "Rio Verde"];
    cities[3][1] = ["São Paulo"];
    cities[3][2] = ["Mariana", "Congonhas", "Uberada"];

    cities[4][0] = ["Qwetta", "Gwadar", "Sibi", "Pasni"];
    cities[4][1] = ["Karachi", "Sukkur", "Hyderabad", "Mithi"];
    cities[4][2] = ["Mirpur", "Muzaffarabad", "Rawalkot"];
    cities[4][3] = ["Gilgit", "Chilas", "Karimabad", "Sust"];

    var
        address = {
            country: document.querySelectorAll("div.conditional-select")[0],
            state: document.querySelectorAll("div.conditional-select")[1],
            city: document.querySelectorAll("div.conditional-select")[2]
        },
        optionsHolder = {
            country: address.country.querySelectorAll("div.options-holder")[0],
            state: address.state.querySelectorAll("div.options-holder")[0],
            city: address.city.querySelectorAll("div.options-holder")[0]
        },
        clonedNodes = {
            country: undefined,
            state: undefined,
            city: undefined
        },
        clonedEmptyNode = function () {
            var
                divTags = {},
                spanTags = {};

            divTags.optionDiv = document.createElement("div");
            divTags.optionDiv.classList.add("option");
            divTags.optionTooltipDiv = document.createElement("div");
            divTags.optionTooltipDiv.classList.add("option-tooltip", "hidden");
            spanTags.optionSpan = document.createElement("span");
            spanTags.optionTooltipSpan = document.createElement("span");
            divTags.optionTooltipDiv.appendChild(spanTags.optionTooltipSpan);
            divTags.optionDiv.appendChild(spanTags.optionSpan);
            divTags.optionDiv.appendChild(divTags.optionTooltipDiv);
            return divTags.optionDiv;
        };

    function generateChildCityNodes() {
        while (optionsHolder.city.firstChild) {
            optionsHolder.city.removeChild(optionsHolder.city.firstChild);
        }
        optionsHolder.city.appendChild(clonedEmptyNode());
        for (k = 0; k < cities[i][j].length - 1; k += 1) {
            clonedCityNode = optionsHolder.city.children[0].cloneNode(true);
            optionsHolder.city.appendChild(clonedCityNode);
        }
        for (k = 0; k < cities[i][j].length; k += 1) {
            optionsHolder.city.children[k].children[0].textContent = cities[i][j][k];
        }
        optionsHolder.city.parentElement.querySelectorAll("div.selected-option span")[0].textContent = cities[i][j][0];
    }

    function populateCityList() {
        for (i = 0; i < countries.length; i += 1) {
            if (address.country.querySelectorAll("div.selected-option span")[0].textContent === countries[i]) {
                for (j = 0; j < states[i].length; j += 1) {
                    if (this.children[0].textContent === states[i][j]) {
                        generateChildCityNodes();
                    }
                }
            }
        }
    }

    function generateChildStateNodes() {
        while (optionsHolder.state.firstChild) {
            optionsHolder.state.removeChild(optionsHolder.state.firstChild);
        }
        optionsHolder.state.appendChild(clonedEmptyNode());
        for (j = 0; j < states[i].length - 1; j += 1) {
            clonedNodes.state = optionsHolder.state.children[0].cloneNode(true);
            optionsHolder.state.appendChild(clonedNodes.state);
        }
        for (j = 0; j < states[i].length; j += 1) {
            optionsHolder.state.children[j].children[0].textContent = states[i][j];
        }
        optionsHolder.state.parentElement.querySelectorAll("div.selected-option span")[0].textContent = states[i][0];
        for (i = 0; i < optionsHolder.state.children.length; i += 1) {
            optionsHolder.state.children[i].addEventListener("click", populateCityList, false);
        }
    }

    function populateStateList() {
        for (i = 0; i < this.parentElement.children.length; i += 1) {
            if (this.children[0].textContent === countries[i]) {
                generateChildStateNodes();
            }
        }
        optionsHolder.city.previousElementSibling.children[0].textContent = "";
        while (optionsHolder.city.firstChild) {
            optionsHolder.city.removeChild(optionsHolder.city.firstChild);
        }
        optionsHolder.city.appendChild(clonedEmptyNode());
    }

    for (i = 0; i < countries.length - 1; i += 1) {
        clonedNodes.country = optionsHolder.country.children[0].cloneNode(true);
        optionsHolder.country.appendChild(clonedNodes.country);
    }

    for (i = 0; i < countries.length; i += 1) {
        optionsHolder.country.children[i].children[0].textContent = countries[i];
    }

    for (i = 0; i < optionsHolder.country.children.length; i += 1) {
        optionsHolder.country.children[i].addEventListener("click", populateStateList, false);
    }

    for (i = 0; i < optionsHolder.state.children.length; i += 1) {
        optionsHolder.state.children[i].addEventListener("click", populateCityList, false);
    }

    selectElement();

}

document.addEventListener("DOMContentLoaded", function () {
    profileFormReady();
}, false);
