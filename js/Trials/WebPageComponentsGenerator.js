function pageReady() {
    "use strict";
    var
        consoleElement = document.querySelectorAll("div.fixed-console")[0],
        generateButton = {
            element: document.querySelectorAll("div.generate-button")[0],
            component: ""
        },
        selectDropdown = {
            element: document.querySelectorAll("div.select-dropdown")[0],
            selectedElement: document.querySelectorAll("div.selected-option")[0],
            optionHolder: document.querySelectorAll("div.options-holder")[0],
            arrowElements: document.querySelectorAll("div.select-arrow span"),
            isActive: false
        },
        navigationObject = {
            generatorElement: document.querySelectorAll("div.navigation-generator")[0],
            elements: document.querySelectorAll("div.navigation-field"),
            numberOfLevels: document.querySelectorAll("div.navigation-field div.field-input input[type=\"range\"]")[0]
        },
        carouselObject = {
            generatorElement: document.querySelectorAll("div.carousel-generator")[0],
            elements: document.querySelectorAll("div.carousel-field"),
            numberOfSlides: document.querySelectorAll("div.carousel-field div.field-input input[type=\"range\"]")[0]
        },
        modalObject = {
            generatorElement: document.querySelectorAll("div.modal-generator")[0],
            elements: document.querySelectorAll("div.modal-field")
        },
        tableObject = {
            generatorElement: document.querySelectorAll("div.table-generator")[0],
            elements: document.querySelectorAll("div.table-field"),
            numberOfLevels: document.querySelectorAll("div.table-field div.field-input input[type=\"range\"]")[0]

        },
        webForm = {
            mandatoryFields: document.querySelectorAll("div.form-field"),
            nonMandatoryFields: document.querySelectorAll("div.field-holder"),
            inputRangeElements: document.querySelectorAll("input[type=\"range\"]"),
            inputTextElements: document.querySelectorAll("input[type=\"text\"]"),
            inputTextAreas: document.querySelectorAll("textarea"),
            submitButton: document.querySelectorAll("div.form-field div.field-button")[0]
        },
        downloadAnchor = {
            markupLink: document.createElement("a"),
            stylesheetLink: document.createElement("a"),
            scriptLink: document.createElement("a")
        },
        i,
        j;

    function displayRangeOutput(element) {
        element.nextElementSibling.textContent = element.value;
        return element.value;
    }

    function expandSelectDropdown() {
        selectDropdown.selectedElement.classList.add("hidden");
        selectDropdown.selectedElement.classList.remove("visible");
        selectDropdown.optionHolder.classList.add("visible");
        selectDropdown.optionHolder.classList.remove("hidden");
        selectDropdown.arrowElements[0].classList.add("hidden");
        selectDropdown.arrowElements[0].classList.remove("visible");
        selectDropdown.arrowElements[1].classList.add("visible");
        selectDropdown.arrowElements[1].classList.remove("hidden");
    }

    function closeSelectDropdown() {
        selectDropdown.selectedElement.classList.add("visible");
        selectDropdown.selectedElement.classList.remove("hidden");
        selectDropdown.optionHolder.classList.add("hidden");
        selectDropdown.optionHolder.classList.remove("visible");
        selectDropdown.arrowElements[0].classList.add("visible");
        selectDropdown.arrowElements[0].classList.remove("hidden");
        selectDropdown.arrowElements[1].classList.add("hidden");
        selectDropdown.arrowElements[1].classList.remove("visible");
    }

    function displayMandatoryFields() {
        for (i = 0; i < webForm.nonMandatoryFields.length; i += 1) {
            webForm.nonMandatoryFields[i].classList.add("hidden");
            webForm.nonMandatoryFields[i].classList.remove("visible");
        }
        for (i = 0; i < webForm.mandatoryFields.length; i += 1) {
            webForm.mandatoryFields[i].classList.remove("hidden");
        }
    }

    function clearGeneratedNodes() {
        while (navigationObject.generatorElement.firstChild) {
            navigationObject.generatorElement.removeChild(navigationObject.generatorElement.firstChild);
        }
        while (tableObject.generatorElement.firstChild) {
            tableObject.generatorElement.removeChild(tableObject.generatorElement.firstChild);
        }
    }

    function generateChildNodes(componentName, rangeValue) {
        var
            textNodes = [],
            inputNodes = [],
            rangeNodes = [],
            spanNodes = [],
            fieldLabel = [],
            fieldInput = [],
            fieldHolder = [];
        clearGeneratedNodes();
        for (i = 0; i < rangeValue; i += 1) {
            fieldHolder[i] = document.createElement("div");
            switch (componentName) {
                case "navigation":
                    // fieldHolder[i].classList.add("field-holder", "navigation-field");
                    // for (j = 0; j < 2; j += 1) {
                    // switch (j) {
                    // case 0:
                    // textNodes[j] = document.createTextNode("Number of elements in level " + (i + 1));
                    // spanNodes[j] = document.createElement("span");
                    // spanNodes[j].appendChild(textNodes[j]);
                    // fieldLabel[j] = document.createElement("div");
                    // fieldLabel[j].classList.add("field-label");
                    // fieldLabel[j].appendChild(spanNodes[j]);
                    // fieldHolder[i].appendChild(fieldLabel[j]);
                    // break;
                    // case 1:
                    // rangeNodes[j - 1] = document.createElement("input");
                    // rangeNodes[j - 1].setAttribute("type", "range");
                    // rangeNodes[j - 1].setAttribute("min", "1");
                    // rangeNodes[j - 1].setAttribute("max", "8");
                    // rangeNodes[j - 1].setAttribute("value", "2");
                    // rangeNodes[j - 1].addEventListener("change", function () {
                    // displayRangeOutput(this);
                    // }, false);
                    // textNodes[j - 1] = document.createTextNode(rangeNodes[j - 1].value);
                    // spanNodes[j - 1] = document.createElement("span");
                    // spanNodes[j - 1].appendChild(textNodes[j - 1]);
                    // fieldInput[j - 1] = document.createElement("div");
                    // fieldInput[j - 1].classList.add("field-input");
                    // fieldInput[j - 1].appendChild(rangeNodes[j - 1]);
                    // fieldInput[j - 1].appendChild(spanNodes[j - 1])
                    // fieldHolder[i].appendChild(fieldInput[j - 1]);
                    // break;
                    // }
                    // navigationObject.generatorElement.appendChild(fieldHolder[i]);
                    // }
                    // rangeNodes = [];
                    // spanNodes = [];
                    // fieldLabel = [];
                    // fieldInput = [];
                    // fieldHolder = [];
                    break;

                case "table":
                    fieldHolder[i].classList.add("field-holder", "table-field");
                    for (j = 0; j < 2; j += 1) {
                        switch (j) {
                            case 0:
                                textNodes[j] = document.createTextNode("Heading of Column " + (i + 1));
                                spanNodes[j] = document.createElement("span");
                                spanNodes[j].appendChild(textNodes[j]);
                                fieldLabel[j] = document.createElement("div");
                                fieldLabel[j].classList.add("field-label");
                                fieldLabel[j].appendChild(spanNodes[j]);
                                fieldHolder[i].appendChild(fieldLabel[j]);
                                break;
                            case 1:
                                inputNodes[j - 1] = document.createElement("input");
                                inputNodes[j - 1].setAttribute("type", "text");
                                fieldInput[j - 1] = document.createElement("div");
                                fieldInput[j - 1].classList.add("field-input");
                                fieldInput[j - 1].appendChild(inputNodes[j - 1]);
                                fieldHolder[i].appendChild(fieldInput[j - 1]);
                                break;
                        }
                        tableObject.generatorElement.appendChild(fieldHolder[i]);
                    }
                    break;

                default:
                    consoleElement.innerHTML = "Development in progress";
            }
            generateButton.element.classList.add("visible");
            generateButton.element.classList.remove("hidden");
        }
    }

    function navigationFields() {
        displayMandatoryFields();
        generateButton.element.classList.add("hidden");
        generateButton.element.classList.remove("visible");
        for (i = 0; i < navigationObject.elements.length; i += 1) {
            navigationObject.elements[i].classList.add("visible");
            navigationObject.elements[i].classList.remove("hidden");
        }
        navigationObject.numberOfLevels.addEventListener("change", function () {
            displayRangeOutput(this);
            generateChildNodes("navigation", displayRangeOutput(this));
        }, false);
        generateButton.component = "navigation";
    }

    function carouselFields() {
        displayMandatoryFields();
        generateButton.element.classList.add("hidden");
        generateButton.element.classList.remove("visible");
        for (i = 0; i < carouselObject.elements.length; i += 1) {
            carouselObject.elements[i].classList.add("visible");
            carouselObject.elements[i].classList.remove("hidden");
        }
        carouselObject.numberOfSlides.addEventListener("change", function () {
            displayRangeOutput(this);
            var downloadButtons = document.querySelectorAll("div.download-buttons-holder")[0].cloneNode(false);
            document.querySelectorAll("div.container")[0].removeChild(document.querySelectorAll("div.download-buttons-holder")[0]);
            document.querySelectorAll("div.container")[0].appendChild(downloadButtons);
            generateButton.element.classList.add("visible");
            generateButton.element.classList.remove("hidden");
        }, false);
        generateButton.component = "carousel";
        return displayRangeOutput(carouselObject.numberOfSlides);
    }

    function modalFields() {
        displayMandatoryFields();
        for (i = 0; i < modalObject.elements.length; i += 1) {
            modalObject.elements[i].classList.add("visible");
            modalObject.elements[i].classList.remove("hidden");
        }
        generateButton.element.classList.add("visible");
        generateButton.element.classList.remove("hidden");
        generateButton.component = "modal";
    }

    function tableFields() {
        displayMandatoryFields();
        generateButton.element.classList.add("hidden");
        generateButton.element.classList.remove("visible");
        for (i = 0; i < tableObject.elements.length; i += 1) {
            tableObject.elements[i].classList.add("visible");
            tableObject.elements[i].classList.remove("hidden");
        }
        tableObject.numberOfLevels.addEventListener("change", function () {
            displayRangeOutput(this);
            generateChildNodes("table", displayRangeOutput(this));
        }, false);
        generateButton.component = "table";
        return displayRangeOutput(tableObject.numberOfLevels);
    }

    function displayContextualFields(event) {
        switch (event.target.textContent) {
            case "Navigation Bar":
                navigationFields();
                break;
            case "Carousel":
                carouselFields();
                break;
            case "Modal Box":
                modalFields();
                break;
            case "Flexbox Table":
                tableFields();
                break;
            default:
                consoleElement.innerHTML = "Development in Progress";
        }
    }

    function selectOption(event) {
        selectDropdown.optionHolder.classList.add("hidden");
        selectDropdown.optionHolder.classList.remove("visible");
        selectDropdown.selectedElement.children[0].textContent = event.target.textContent;
        selectDropdown.selectedElement.classList.add("visible");
        selectDropdown.selectedElement.classList.remove("hidden");
        selectDropdown.arrowElements[0].classList.add("visible");
        selectDropdown.arrowElements[0].classList.remove("hidden");
        selectDropdown.arrowElements[1].classList.add("hidden");
        selectDropdown.arrowElements[1].classList.remove("visible");

        displayContextualFields(event);
        clearGeneratedNodes();
    }

    function generateNavigationBar() {

    }

    function generateCarousel(numberOfSlides) {
        var
            carouselDocument = document.implementation.createHTMLDocument("Carousel"),
            carouselDocumentHeadElements = {};

        carouselDocumentHeadElements.metaTag = document.createElement("meta");
        carouselDocumentHeadElements.metaTag.setAttribute("charset", "utf-8");
        carouselDocument.head.appendChild(carouselDocumentHeadElements.metaTag);

        function generateCarouselCSS() {

            var styleTag = document.createElement("style");
            styleTag.setAttribute("type", "text/css");

            var styleSheetRules = [];
            styleSheetRules[0] = document.createTextNode("@CHARSET \"utf-8\";");
            styleSheetRules[1] = document.createTextNode("* {\
box-sizing: border-box;\
}");
            styleSheetRules[2] = document.createTextNode("*.visible {\
display: flex;\
}");
            styleSheetRules[3] = document.createTextNode("*.hidden {\
display: none;\
}");
            styleSheetRules[4] = document.createTextNode("html,\
body {\
margin: 0rem;\
padding: 0rem;\
}");
            styleSheetRules[5] = document.createTextNode("div {\
display: flex;\
flex-direction: column;\
}");
            styleSheetRules[6] = document.createTextNode("div.carousel {\
position: relative;\
align-items: center;\
}");
            styleSheetRules[7] = document.createTextNode("div.carousel-arrow {\
position: absolute;\
top: 0rem;\
bottom: 0rem;\
justify-content: center;\
align-items: center;\
padding: 1rem;\
cursor: pointer;\
z-index: 2;\
transition: 250ms;\
color: rgba(0, 0, 0, 0.2);\
}");
            styleSheetRules[8] = document.createTextNode("div.carousel-arrow:hover {\
background-color: rgba(255, 255, 255, 0.7);\
color: rgba(0, 0, 0, 1);\
}");
            styleSheetRules[9] = document.createTextNode("div.carousel-arrow span {\
font-size: 2rem;\
}");
            styleSheetRules[10] = document.createTextNode("div#left {\
left: 0rem;\
}");
            styleSheetRules[11] = document.createTextNode("div#right {\
right: 0rem;\
}");
            styleSheetRules[12] = document.createTextNode("div.carousel-contents-holder {\
position: relative;\
align-items: center;\
height: 500px;\
}");
            styleSheetRules[13] = document.createTextNode("div.carousel-content {\
flex-grow: 1;\
overflow: auto;\
}");
            styleSheetRules[14] = document.createTextNode("div.carousel-content span {\
font-family: Verdana;\
line-height: 1.5rem;\
}");
            styleSheetRules[15] = document.createTextNode("div.carousel-buttons-holder {\
position: absolute;\
bottom: 0rem;\
flex-grow: 1;\
flex-direction: row;\
justify-content: center;\
align-items: center;\
padding: 1rem;\
background-color: rgba(0, 0, 0, 0);\
transition: 250ms\
}");
            styleSheetRules[16] = document.createTextNode("div.carousel-buttons-holder:hover {\
background-color: rgba(255, 255, 255, 0.7);\
}");
            styleSheetRules[17] = document.createTextNode("div.carousel-button {\
margin: 0.5rem;\
padding: 0.5rem;\
background-color: rgba(0, 0, 0, 0.2);\
border-radius: 50%;\
cursor: pointer;\
transition: 250ms;\
}");
            styleSheetRules[18] = document.createTextNode("div.carousel-button:hover {\
background-color: rgba(0, 0, 0, 1);\
}");
            styleSheetRules[19] = document.createTextNode("div.active {\
 background-color: rgba(0, 0, 0, 1);\
}");

            for (i = 0; i < styleSheetRules.length; i += 1) {
                styleTag.appendChild(styleSheetRules[i]);
            }
            var stringCSS = styleTag.innerHTML
            var fileCSSBlob = new Blob([stringCSS], {
                type: "text/plain"
            });
            var fileCSS = window.URL.createObjectURL(fileCSSBlob);
            downloadAnchor.stylesheetLink.setAttribute("href", fileCSS);
            downloadAnchor.stylesheetLink.setAttribute("download", "Carousel.css");
            downloadAnchor.stylesheetLink.innerHTML = "Download CSS";
            document.querySelectorAll("div.download-buttons-holder")[0].appendChild(downloadAnchor.stylesheetLink);
            downloadAnchor.stylesheetLink.addEventListener("click", function () {
                this.parentElement.removeChild(this);
            }, false);
        }

        function generateCarouselJS() {

            var scriptTag = document.createElement("script");
            scriptTag.setAttribute("type", "text/javascript");

            var scriptContent = document.createTextNode("function pageReady() {\
    \"use strict\";\
    var\
        carousel = {\
            arrowLeft: document.querySelectorAll(\"div#left\")[0],\
            arrowRight: document.querySelectorAll(\"div#right\")[0],\
            contents: document.querySelectorAll(\"div.carousel-content\"),\
            buttons: document.querySelectorAll(\"div.carousel-button\")\
        },\
        i,\
        j;\
\
    function changeContent(direction) {\
        for (i = 0; i < carousel.contents.length; i += 1) {\
            if (carousel.contents[i].classList.contains(\"visible\")) {\
                carousel.contents[i].classList.add(\"hidden\");\
                carousel.contents[i].classList.remove(\"visible\");\
                carousel.buttons[i].classList.remove(\"active\");\
                if (direction) {\
                    if (i === (carousel.contents.length - 1)) {\
                        carousel.contents[0].classList.add(\"visible\");\
                        carousel.contents[0].classList.remove(\"hidden\");\
                        carousel.buttons[0].classList.add(\"active\");\
                        break;\
                    } else {\
                        carousel.contents[i + 1].classList.add(\"visible\");\
                        carousel.contents[i + 1].classList.remove(\"hidden\");\
                        carousel.buttons[i + 1].classList.add(\"active\");\
                        \
                        break;\
                    }\
                } else {\
                    if (i === 0) {\
                        carousel.contents[carousel.contents.length - 1].classList.add(\"visible\");\
                        carousel.contents[carousel.contents.length - 1].classList.remove(\"hidden\");\
                        carousel.buttons[carousel.contents.length - 1].classList.add(\"active\");\
                        break;\
                    } else {\
                        carousel.contents[i - 1].classList.add(\"visible\");\
                        carousel.contents[i - 1].classList.remove(\"hidden\");\
                        carousel.buttons[i - 1].classList.add(\"active\");\
                        break;\
                    }\
                }\
            }\
        }\
    }\
\
    function changeButtons(event) {\
        for (i = 0; i < carousel.buttons.length; i += 1) {\
            carousel.buttons[i].classList.remove(\"active\");\
        }\
        event.target.classList.add(\"active\");\
        for (i = 0; i < carousel.buttons.length; i += 1) {\
            if (carousel.buttons[i].classList.contains(\"active\")) {\
                carousel.contents[i].classList.add(\"visible\");\
                carousel.contents[i].classList.remove(\"hidden\");\
            } else {\
                carousel.contents[i].classList.add(\"hidden\");\
                carousel.contents[i].classList.remove(\"visible\");\
            }\
        }\
    }\
\
    carousel.arrowLeft.addEventListener(\"click\", function () {\
        changeContent(false);\
    }, false);\
\
    carousel.arrowRight.addEventListener(\"click\", function () {\
        changeContent(true);\
    }, false);\
\
    for (i = 0; i < carousel.buttons.length; i += 1) {\
        carousel.buttons[i].addEventListener(\"click\", function (event) {\
            changeButtons(event);\
        }, false);\
    }\
}\
\
window.addEventListener(\"load\", function () {\
    \"use strict\";\
    pageReady();\
}, false);\
")

            scriptTag.appendChild(scriptContent);
            var stringJS = scriptTag.innerHTML;
            var fileJSBlob = new Blob([stringJS], {
                type: "text/plain"
            });
            var fileJS = window.URL.createObjectURL(fileJSBlob);
            downloadAnchor.scriptLink.setAttribute("href", fileJS);
            downloadAnchor.scriptLink.setAttribute("download", "Carousel.js");
            downloadAnchor.scriptLink.innerHTML = "Download JS";
            document.querySelectorAll("div.download-buttons-holder")[0].appendChild(downloadAnchor.scriptLink);
            downloadAnchor.scriptLink.addEventListener("click", function () {
                this.parentElement.removeChild(this);
            }, false);
        }

        function generateCarouselHTML() {
            var
                divTags = {},
                iTag = document.createElement("i"),
                spanTag = document.createElement("span");

            divTags.containerDiv = document.createElement("div");
            divTags.containerDiv.classList.add("carousel");
            divTags.carouselArrowDivLeft = document.createElement("div");
            divTags.carouselArrowDivLeft.classList.add("carousel-arrow");
            divTags.carouselArrowDivLeft.setAttribute("id", "left");
            iTag.classList.add("fa", "fa-arrow-double-left");
            spanTag.appendChild(iTag);
            divTags.carouselArrowDivLeft.appendChild(spanTag);
            spanTag = document.createElement(spanTag.nodeName);
            iTag = document.createElement(iTag.nodeName);

            divTags.carouselContentsHolderDiv = document.createElement("div");
            divTags.carouselContentsHolderDiv.classList.add("carousel-contents-holder");
            for (i = 0; i < numberOfSlides; i += 1) {
                divTags.carouselContentDiv = document.createElement("div");
                divTags.carouselContentDiv.classList.add("carousel-content");
                if (i === 0) {
                    divTags.carouselContentDiv.classList.add("visible");
                } else {
                    divTags.carouselContentDiv.classList.add("hidden");
                }
                divTags.carouselContentsHolderDiv.appendChild(divTags.carouselContentDiv);
            }
            divTags.carouselButtonsHolderDiv = document.createElement("div");
            divTags.carouselButtonsHolderDiv.classList.add("carousel-buttons-holder");
            divTags.carouselContentsHolderDiv.appendChild(divTags.carouselButtonsHolderDiv);
            for (i = 0; i < numberOfSlides; i += 1) {
                divTags.carouselButtonDiv = document.createElement("div");
                divTags.carouselButtonDiv.classList.add("carousel-button");
                if (i === 0) {
                    divTags.carouselButtonDiv.classList.add("active");
                }
                divTags.carouselButtonsHolderDiv.appendChild(divTags.carouselButtonDiv);
            }

            divTags.carouselArrowDivRight = document.createElement("div");
            divTags.carouselArrowDivRight.classList.add("carousel-arrow");
            divTags.carouselArrowDivRight.setAttribute("id", "right");
            iTag.classList.add("fa", "fa-arrow-double-right");
            spanTag.appendChild(iTag);
            divTags.carouselArrowDivRight.appendChild(spanTag);
            spanTag = document.createElement(spanTag.nodeName);
            iTag = document.createElement(iTag.nodeName);

            divTags.containerDiv.appendChild(divTags.carouselArrowDivLeft);
            divTags.containerDiv.appendChild(divTags.carouselContentsHolderDiv);
            divTags.containerDiv.appendChild(divTags.carouselArrowDivRight);
            carouselDocument.body.appendChild(divTags.containerDiv);

            var textLines = "";
            textLines = carouselDocument.all[0].innerHTML;
            var stringHTML = "<html>" + textLines + "</html>";
            var fileHTMLBlob = new Blob([stringHTML], {
                type: "text/plain"
            });
            var fileHTML = window.URL.createObjectURL(fileHTMLBlob);

            downloadAnchor.markupLink.setAttribute("href", fileHTML);
            downloadAnchor.markupLink.setAttribute("download", "Carousel" + numberOfSlides + ".html");
            downloadAnchor.markupLink.innerHTML = "Download HTML";
            document.querySelectorAll("div.download-buttons-holder")[0].appendChild(downloadAnchor.markupLink);
            downloadAnchor.markupLink.addEventListener("click", function () {
                this.parentElement.removeChild(this);
            }, false);
        }

        generateCarouselCSS();
        carouselDocumentHeadElements.linkTag1 = document.createElement("link");
        carouselDocumentHeadElements.linkTag1.setAttribute("rel", "stylesheet");
        carouselDocumentHeadElements.linkTag1.setAttribute("href", "Carousel.css");
        carouselDocumentHeadElements.linkTag2 = document.createElement("link");
        carouselDocumentHeadElements.linkTag2.setAttribute("rel", "stylesheet");
        carouselDocumentHeadElements.linkTag2.setAttribute("href", "font-awesome.min.css");

        generateCarouselJS();
        carouselDocumentHeadElements.scriptTag = document.createElement("script");
        carouselDocumentHeadElements.scriptTag.setAttribute("type", "text/javascript");
        carouselDocumentHeadElements.scriptTag.setAttribute("src", "Carousel.js");

        carouselDocument.head.appendChild(carouselDocumentHeadElements.linkTag1);
        carouselDocument.head.appendChild(carouselDocumentHeadElements.linkTag2);
        carouselDocument.head.appendChild(carouselDocumentHeadElements.scriptTag);

        generateCarouselHTML();
    }

    function generateModalBox() {
        var
            modalDocument = document.implementation.createHTMLDocument("Modal"),
            modalDocumentHeadElements = {};

        modalDocumentHeadElements.metaTag = document.createElement("meta");
        modalDocumentHeadElements.metaTag.setAttribute("charset", "utf-8");
        modalDocument.head.appendChild(modalDocumentHeadElements.metaTag);

        function generateModalCSS() {
            var styleTag = document.createElement("style");
            styleTag.setAttribute("type", "text/css");

            var styleSheetRules = [];

            styleSheetRules[0] = document.createTextNode("@CHARSET \"utf-8\";");
            styleSheetRules[1] = document.createTextNode("* {\
box-sizing: border-box;\
}");
            styleSheetRules[2] = document.createTextNode("*.visible {\
display: flex;\
}");
            styleSheetRules[3] = document.createTextNode("*.hidden {\
display: none;\
}");
            styleSheetRules[4] = document.createTextNode("html,\
body {\
margin: 0rem;\
padding: 0rem;\
}");
            styleSheetRules[5] = document.createTextNode("div {\
display: flex;\
flex-direction: column;\
}");
            styleSheetRules[6] = document.createTextNode("div.modal {\
flex-direction: row;\
justify-content: center;\
align-items: center;\
}");
            styleSheetRules[7] = document.createTextNode("div.modal-button {\
background-color: rgba(255, 255, 255, 1);\
transition: 250ms;\
border: 1px solid;\
}");
            styleSheetRules[8] = document.createTextNode("div.modal-button:hover {\
background-color: rgba(0, 0, 0, 1);\
color: rgba(255, 255, 255, 1);\
}");
            styleSheetRules[9] = document.createTextNode("div.modal-button:active {\
background-color: rgba(255, 255, 255, 1);\
color: rgba(0, 0, 0, 1);\
}");
            styleSheetRules[10] = document.createTextNode("div.modal-button span {\
font-family: Verdana;\
line-height: 1.5rem;\
padding: 1rem;\
cursor: pointer;\
}");
            styleSheetRules[11] = document.createTextNode("div.modal-container {\
position: fixed;\
top: 0rem;\
bottom: 0rem;\
left: 0rem;\
right: 0rem;\
justify-content: center;\
align-items: center;\
background-color: rgba(0, 0, 0, 0.6);\
}");
            styleSheetRules[12] = document.createTextNode("div.modal-box {\
background-color: rgba(255, 255, 255, 1);\
box-shadow: 10px 10px 20px 0px rgba(0, 0, 0, 1);\
}");
            styleSheetRules[13] = document.createTextNode("div.modal-separator {\
padding: 1px;\
background-color: rgba(190, 190, 190, 1);\
}");
            styleSheetRules[14] = document.createTextNode("div.modal-title-holder {\
flex-direction: row;\
justify-content: space-between;\
margin: 0.5rem;\
}");
            styleSheetRules[15] = document.createTextNode("div.modal-title {\
justify-content: center;\
align-items: center;\
padding: 1rem;\
cursor: default;\
}");
            styleSheetRules[16] = document.createTextNode("div.modal-title span {\
font-family: Verdana;\
line-height: 1.5rem;\
font-size: 1.5rem;\
}");
            styleSheetRules[17] = document.createTextNode("div.modal-close-button {\
justify-content: center;\
align-items: center;\
padding: 1rem;\
cursor: pointer;\
}");
            styleSheetRules[18] = document.createTextNode("div.modal-close-button span {\
font-family: Verdana;\
line-height: 1.5rem;\
font-size: 1.5rem;\
}");
            styleSheetRules[19] = document.createTextNode("div.modal-content-holder {\
margin: 0.5rem;\
min-width: 50vw;\
max-width: 80vw;\
max-height: 50vh;\
}");
            styleSheetRules[20] = document.createTextNode("div.modal-content {\
padding: 1rem;\
cursor: default;\
overflow: auto;\
}");
            styleSheetRules[21] = document.createTextNode("div.modal-content span {\
font-family: Verdana;\
line-height: 1.5rem;\
white-space: pre-wrap;\
}");
            styleSheetRules[22] = document.createTextNode("div.modal-footer-holder {\
margin: 0.5rem;\
}");
            styleSheetRules[23] = document.createTextNode("div.modal-footer {\
padding: 1rem;\
cursor: default;\
}");
            styleSheetRules[24] = document.createTextNode("div.modal-footer span {\
font-family: Verdana;\
line-height: 1.5rem;\
}");

            for (i = 0; i < styleSheetRules.length; i += 1) {
                styleTag.appendChild(styleSheetRules[i]);
            }
            var stringCSS = styleTag.innerHTML
            var fileCSSBlob = new Blob([stringCSS], {
                type: "text/plain"
            });
            var fileCSS = window.URL.createObjectURL(fileCSSBlob);
            downloadAnchor.stylesheetLink.setAttribute("href", fileCSS);
            downloadAnchor.stylesheetLink.setAttribute("download", "Modal.css");
            downloadAnchor.stylesheetLink.innerHTML = "Download CSS";
            document.querySelectorAll("div.download-buttons-holder")[0].appendChild(downloadAnchor.stylesheetLink);
            downloadAnchor.stylesheetLink.addEventListener("click", function () {
                this.parentElement.removeChild(this);
            }, false);
        }

        function generateModalJS() {
            var scriptTag = document.createElement("script");
            scriptTag.setAttribute("type", "text/javascript");

            var scriptContent = document.createTextNode("function pageReady() {\
    \"use strict\";\
    var\
        modal = {\
            trigger: document.querySelectorAll(\"div.modal-button\")[0],\
            container: document.querySelectorAll(\"div.modal-container\")[0],\
            box: document.querySelectorAll(\"div.modal-box\"),\
            closeButton: document.querySelectorAll(\"div.modal-close-button\")[0]\
        };\
\
    function openModalBox() {\
        modal.container.classList.add(\"visible\");\
        modal.container.classList.remove(\"hidden\");\
    }\
\
    function closeModalBox(event) {\
        modal.container.classList.add(\"hidden\");\
        modal.container.classList.remove(\"visible\");\
    }\
\
    modal.trigger.addEventListener(\"click\", function () {\
        openModalBox();\
    }, false);\
\
    modal.closeButton.addEventListener(\"click\", function (event) {\
        closeModalBox();\
    }, false);\
}\
\
window.addEventListener(\"load\", function () {\
    \"use strict\";\
    pageReady();\
});\
");
            scriptTag.appendChild(scriptContent);
            var stringJS = scriptTag.innerHTML;
            var fileJSBlob = new Blob([stringJS], {
                type: "text/plain"
            });
            var fileJS = window.URL.createObjectURL(fileJSBlob);
            downloadAnchor.scriptLink.setAttribute("href", fileJS);
            downloadAnchor.scriptLink.setAttribute("download", "Modal.js");
            downloadAnchor.scriptLink.innerHTML = "Download JS";
            document.querySelectorAll("div.download-buttons-holder")[0].appendChild(downloadAnchor.scriptLink);
            downloadAnchor.scriptLink.addEventListener("click", function () {
                this.parentElement.removeChild(this);
            }, false);
        }

        function generateModalHTML() {
            var
                divTags = {},
                iTag = document.createElement("i"),
                textNodes = [],
                spanTag = document.createElement("span");

            divTags.containerDiv = document.createElement("div");
            divTags.containerDiv.classList.add("modal");

            divTags.modalButtonDiv = document.createElement("div");
            divTags.modalButtonDiv.classList.add("modal-button");
            textNodes[3] = document.createTextNode("Open Modal");
            spanTag.appendChild(textNodes[3]);
            divTags.modalButtonDiv.appendChild(spanTag);
            spanTag = document.createElement(spanTag.nodeName);

            divTags.modalContainerDiv = document.createElement("div");
            divTags.modalContainerDiv.classList.add("modal-container", "hidden");

            divTags.modalBoxDiv = document.createElement("div");
            divTags.modalBoxDiv.classList.add("modal-box");

            divTags.modalTitleHolderDiv = document.createElement("div");
            divTags.modalTitleHolderDiv.classList.add("modal-title-holder");
            divTags.modalTitleDiv = document.createElement("div");
            divTags.modalTitleDiv.classList.add("modal-title");
            textNodes[0] = document.createTextNode(modalObject.elements[0].children[1].children[0].value);
            spanTag.appendChild(textNodes[0]);
            divTags.modalTitleDiv.appendChild(spanTag);
            spanTag = document.createElement(spanTag.nodeName);
            divTags.modalTitleHolderDiv.appendChild(divTags.modalTitleDiv)
            divTags.modalCloseButtonDiv = document.createElement("div");
            divTags.modalCloseButtonDiv.classList.add("modal-close-button");
            iTag.classList.add("fa", "fa-times");
            spanTag.appendChild(iTag);
            divTags.modalCloseButtonDiv.appendChild(spanTag);
            spanTag = document.createElement(spanTag.nodeName);
            iTag = document.createElement(iTag.nodeName);
            divTags.modalTitleHolderDiv.appendChild(divTags.modalCloseButtonDiv);

            divTags.modalSeparatorDiv = [];
            divTags.modalSeparatorDiv[0] = document.createElement("div");
            divTags.modalSeparatorDiv[0].classList.add("modal-separator");
            divTags.modalSeparatorDiv[1] = divTags.modalSeparatorDiv[0].cloneNode(false);

            divTags.modalContentHolderDiv = document.createElement("div");
            divTags.modalContentHolderDiv.classList.add("modal-content-holder");
            divTags.modalContentDiv = document.createElement("div");
            divTags.modalContentDiv.classList.add("modal-content");
            textNodes[1] = document.createTextNode(modalObject.elements[1].children[1].children[0].value);
            spanTag.appendChild(textNodes[1]);
            divTags.modalContentDiv.appendChild(spanTag);
            spanTag = document.createElement(spanTag.nodeName);
            divTags.modalContentHolderDiv.appendChild(divTags.modalContentDiv);

            divTags.modalFooterHolderDiv = document.createElement("div");
            divTags.modalFooterHolderDiv.classList.add("modal-footer-holder");
            divTags.modalFooterDiv = document.createElement("div");
            divTags.modalFooterDiv.classList.add("modal-footer");
            textNodes[2] = document.createTextNode(modalObject.elements[2].children[1].children[0].value);
            spanTag.appendChild(textNodes[2]);
            divTags.modalFooterDiv.appendChild(spanTag);
            spanTag = document.createElement(spanTag.nodeName);
            divTags.modalFooterHolderDiv.appendChild(divTags.modalFooterDiv);

            divTags.modalBoxDiv.appendChild(divTags.modalTitleHolderDiv);
            divTags.modalBoxDiv.appendChild(divTags.modalSeparatorDiv[0]);
            divTags.modalBoxDiv.appendChild(divTags.modalContentHolderDiv);
            divTags.modalBoxDiv.appendChild(divTags.modalSeparatorDiv[1]);
            divTags.modalBoxDiv.appendChild(divTags.modalFooterHolderDiv);

            divTags.modalContainerDiv.appendChild(divTags.modalBoxDiv);

            divTags.containerDiv.appendChild(divTags.modalButtonDiv);
            divTags.containerDiv.appendChild(divTags.modalContainerDiv);

            modalDocument.body.appendChild(divTags.containerDiv);

            var textLines = "";
            textLines = modalDocument.all[0].innerHTML;
            var stringHTML = "<html>" + textLines + "</html>";
            var fileHTMLBlob = new Blob([stringHTML], {
                type: "text/plain"
            });
            var fileHTML = window.URL.createObjectURL(fileHTMLBlob);

            downloadAnchor.markupLink.setAttribute("href", fileHTML);
            downloadAnchor.markupLink.setAttribute("download", "Modal.html");
            downloadAnchor.markupLink.innerHTML = "Download HTML";
            document.querySelectorAll("div.download-buttons-holder")[0].appendChild(downloadAnchor.markupLink);
            downloadAnchor.markupLink.addEventListener("click", function () {
                this.parentElement.removeChild(this);
            }, false);
        }

        generateModalCSS();
        modalDocumentHeadElements.linkTag1 = document.createElement("link");
        modalDocumentHeadElements.linkTag1.setAttribute("rel", "stylesheet");
        modalDocumentHeadElements.linkTag1.setAttribute("href", "Modal.css");
        modalDocumentHeadElements.linkTag2 = document.createElement("link");
        modalDocumentHeadElements.linkTag2.setAttribute("rel", "stylesheet");
        modalDocumentHeadElements.linkTag2.setAttribute("href", "font-awesome.min.css");

        generateModalJS();
        modalDocumentHeadElements.scriptTag = document.createElement("script");
        modalDocumentHeadElements.scriptTag.setAttribute("type", "text/javascript");
        modalDocumentHeadElements.scriptTag.setAttribute("src", "Modal.js");

        modalDocument.head.appendChild(modalDocumentHeadElements.linkTag1);
        modalDocument.head.appendChild(modalDocumentHeadElements.linkTag2);
        modalDocument.head.appendChild(modalDocumentHeadElements.scriptTag);

        generateModalHTML();
        generateButton.element.classList.add("hidden");
        generateButton.element.classList.remove("visible");
    }

    function generateFlexboxTable() {

    }

    function identifyWebComponent(componentName) {
        switch (componentName) {
            case "navigation":
                generateNavigationBar();
                break;
            case "carousel":
                generateCarousel(carouselFields());
                break;
            case "modal":
                if (modalObject.elements[0].children[1].children[0].value === "") {
                    consoleElement.innerHTML = "No Title";
                } else if (modalObject.elements[1].children[1].children[0].value === "") {
                    consoleElement.innerHTML = "No Content";
                } else if (modalObject.elements[2].children[1].children[0].value === "") {
                    consoleElement.innerHTML = "No Footer";
                } else {
                    generateModalBox();
                }
                break;
            case "table":
                generateFlexboxTable(tableFields());
                break;
            default:
                consoleElement.innerHTML = "Not yet Developed";
        }
    }

    selectDropdown.arrowElements[0].addEventListener("click", function () {
        expandSelectDropdown();
    }, false);

    selectDropdown.arrowElements[1].addEventListener("click", function () {
        closeSelectDropdown();
    }, false);

    for (i = 0; i < selectDropdown.optionHolder.children.length; i += 1) {
        selectDropdown.optionHolder.children[i].addEventListener("click", function (event) {
            selectOption(event);
        }, false);
    }

    generateButton.element.addEventListener("click", function () {
        identifyWebComponent(generateButton.component);
    }, false);

}

window.addEventListener("load", function () {
    "use strict";
    pageReady();
}, false);
