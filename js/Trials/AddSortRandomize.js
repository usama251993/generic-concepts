function pageReady(event) {

    "use strict";

    var
        // Section to display messages
        consoleElement = document.querySelectorAll("div.fixed-console")[0],
        
        pageForm = document.querySelectorAll("form")[0],
        inputField = document.querySelectorAll("form input[type=\"text\"]")[0],

        // "Submit" and "Clear last entry" buttons
        formButtons = document.querySelectorAll("form button"),

        // Left section
        inputsContainer = document.getElementById("experiment-raw"),

        // Right section
        resultsContainer = document.getElementById("experiment-modified"),

        // "Acquire", "Randomize" and "Sort" buttons
        actionButtons = document.querySelectorAll("div.buttons button"),

        // Dummy variable
        inputsText = [],

        // Iterator
        i;

    function showAcquire() {

        // Show Acquire Button
        actionButtons[0].classList.add("visible");
        actionButtons[0].classList.remove("hidden");

        // Hide Randomize and Submit Buttona
        actionButtons[1].classList.add("hidden");
        actionButtons[1].classList.remove("visible");
        actionButtons[2].classList.add("hidden");
        actionButtons[2].classList.remove("visible");
    }

    function hideAcquire() {

        // Hide Acquire Buttons
        actionButtons[0].classList.add("hidden");
        actionButtons[0].classList.remove("visible");

        // Show Randomize and Submit Buttons
        actionButtons[1].classList.add("visible");
        actionButtons[1].classList.remove("hidden");
        actionButtons[2].classList.add("visible");
        actionButtons[2].classList.remove("hidden");
    }

    function processFormData() {

        // Local variables for the processFormData Function
        var textField,
            spanElement;

        // Store input text in a variable
        textField = document.createTextNode(inputField.value.trim());

        // Create <span> node
        spanElement = document.createElement("span");
        if (inputField.value.trim() === "" || inputField.value.trim() === null || inputField.value.trim() === undefined) {

            // If no input        
            // Display Message
            consoleElement.innerHTML = "Enter Something in the textbox";
            inputField.focus();
        } else {
            consoleElement.innerHTML = "";

            // Populate the stored text in <span> element
            spanElement.appendChild(textField);

            // Append <span> element to the inputs section
            inputsContainer.appendChild(spanElement);
        }

        // Clear the input field
        inputField.value = "";
        inputField.focus();

        // Show the Acquire button
        showAcquire();
    }

    function clearLastEntry() {

        // If no input
        if (inputsContainer.children.length === 0) {
            consoleElement.innerHTML = "No Entries in the Input Section";
            inputField.focus();
        } else {
            if (resultsContainer.children.length !== 0) {

                // If results exist
                for (i = 0; i < resultsContainer.children.length; i += 1) {
                    if (resultsContainer.children[i].textContent === inputsContainer.children[inputsContainer.children.length - 1].textContent) {

                        // Check for the last entry in results section
                        // Remove the matching entry in results section
                        resultsContainer.removeChild(resultsContainer.children[i]);
                        break;
                    }
                }
            }

            // Remove the last entry in inputs section
            inputsContainer.removeChild(inputsContainer.children[inputsContainer.children.length - 1]);
            inputField.focus();
        }
    }

    function acquireFormData() {

        // Local variables for acquireFormData Function
        var textNodes = [],
            spanNodes = [];

        if (inputsContainer.children.length === 0) {

            // If no input
            consoleElement.innerHTML = "No Entries in the Input Section";
            inputField.focus();
        } else {

            // Remove all the existing entries in results section
            while (resultsContainer.firstChild) {
                resultsContainer.removeChild(resultsContainer.firstChild);
            }
            for (i = 0; i < inputsContainer.children.length; i += 1) {

                // Acquire the input text
                inputsText[i] = inputsContainer.children[i].textContent;

                // Create the text node
                textNodes[i] = document.createTextNode(inputsText[i]);

                // Create the <span> element
                spanNodes[i] = document.createElement("span");

                // Populate the text node in <span> element
                spanNodes[i].appendChild(textNodes[i]);

                // Append the <span> element in the results section
                resultsContainer.appendChild(spanNodes[i]);
            }
        }

        // Clear the variables
        inputsText = [];
        hideAcquire();
    }

    function randomizeText() {

        // Local variables for randomizeText Function
        var inputsText = [],
            randomizedText = [],
            mathRandom;

        // Acquire the input text content and store them in an array
        for (i = 0; i < resultsContainer.children.length; i += 1) {
            inputsText[i] = inputsContainer.children[i].textContent;
        }

        for (i = 0; i < resultsContainer.children.length; i += 1) {

            // Generate array index randomly
            mathRandom = Math.floor((Math.random() * 10) % inputsText.length);

            // Populate the element at randomly generated array index to a variable
            randomizedText[i] = inputsText.splice(mathRandom, 1)[0];

            // Set the value in the results section
            resultsContainer.children[i].textContent = randomizedText[i];
        }

        // Clear the variables
        inputsText = [];
    }

    function sortText() {

        // Local variables for sortText Function
        var resultsText = [],
            sortedText = [],
            orderFlag;

        // Acquire the input text content and store them in an array
        for (i = 0; i < resultsContainer.children.length; i += 1) {
            resultsText[i] = resultsContainer.children[i].textContent;
            inputsText[i] = resultsContainer.children[i].textContent;
        }

        // Store the sorted input values in another array
        inputsText = inputsText.sort();

        for (i = 0; i < resultsText.length; i += 1) {
            if (resultsText[i] === inputsText[i]) {

                // If ascending order is detected, prepare for descending order
                orderFlag = true;
            } else {

                // If any order other than ascending order is detected, prepare for ascending order
                orderFlag = false;
                break;
            }
        }

        if (orderFlag === false) {

            // Sort in ascending order
            sortedText = resultsText.sort();
            orderFlag = true;
        } else {

            // Sort in descending order
            sortedText = resultsText.sort().reverse();
            orderFlag = false;
        }

        // Set the sorted input values in the output section
        for (i = 0; i < resultsContainer.children.length; i += 1) {
            resultsContainer.children[i].textContent = sortedText[i];
        }

        // Clear the cariables
        inputsText = [];
    }

    // --------------- Event Listeners for the page ---------------

    pageForm.addEventListener("submit", function (event) {
        event.preventDefault();
    }, false);

    formButtons[0].addEventListener("click", function () {
        processFormData();
    }, false);

    formButtons[1].addEventListener("click", function () {
        clearLastEntry();
    }, false);

    actionButtons[0].addEventListener("click", function () {
        acquireFormData();
    }, false);

    actionButtons[1].addEventListener("click", function () {
        randomizeText();
    }, false);

    actionButtons[2].addEventListener("click", function () {
        sortText();
    }, false);

    // --------------- Event Listeners : End ---------------

}

document.addEventListener("DOMContentLoaded", function (event) {
    "use strict";
    pageReady(event);
}, false);
