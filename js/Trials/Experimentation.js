/*

01. Shadow DOM
02. window.requestanimationframe
03. Blob
04. Promise
05. HTML IndexedDB
06. JavaScript Debounce
07. Command Pattern

*/

document.addEventListener("DOMContentLoaded", function () {
	"use strict";

	(function () {
		"use strict";
		var
			CSSSheet,
			semanticTags = {},
			markupTags = {},
			dummyVar01,
			dummyVar02,
			dummyVar03,
			dummyVar04,
			dummyVar05,
			dummyVar06,
			dummyVar07,
			dummyVar08,
			dummyVar09,
			dummyVar10,
			dummyVar11,
			dummyVar12,
			dummyVar13,
			dummyVar14,
			dummyVar15,
			dummyVar16,
			dummyVar17,
			dummyVar18,
			dummyVar19,
			dummyVar20,
			i,
			j;

		/* CSS Stylesheet of the document */
		CSSSheet = document.querySelectorAll("link[href*=\"Experimentation.css\"]")[0].sheet;

		/* -------1------- */

		/* Start : Creating and Styling Shadow */

		// dummyVar01 = document.createElement("div");
		// dummyVar02 = dummyVar01.attachShadow({
		// mode: "open"
		// });
		// dummyVar02.innerHTML = "<style>span {padding: 10px; font-size: 1.5rem;} span.coded {background-color: rgba(0, 0, 90, 1); color: rgba(255, 255, 255, 1); font-family: \"Consolas\", monospace;}</style>";
		// dummyVar02.innerHTML += "<span>This content is inside span element created in <span class=\"coded\">#shadow-root (open)</span></span><span>Every span element is CSSed differently <span class=\"coded\">padding: 10px</span> <span class=\"coded\">font-size: 1.5rem</span></span>";
		// 
		// dummyVar03 = document.createElement("div");
		// dummyVar04 = dummyVar03.attachShadow({
		// mode: "closed"
		// });
		// 
		// dummyVar04.innerHTML = "<style>span {padding: 5px; font-size: 20px;} span.coded {background-color: rgba(90, 0, 0, 1); color: rgba(255, 255, 255, 1); font-family: \"Consolas\", monospace;}</style>";
		// dummyVar04.innerHTML += "<span>This content is inside span element created in <span class=\"coded\">#shadow-root (closed)</span></span><span><span class=\"coded\">padding: 5px</span> <span class=\"coded\">font-size: 20px</span></span>";
		// 
		// document.body.appendChild(dummyVar01);
		// document.body.appendChild(dummyVar03);

		// /* End : Creating and Styling Shadow */

		// /* --------------- */

		// /* Start : Separating content from Presentation */

		// /* Step 1 : Hide Presentation Details */
		// /* Custom Method is by using CSS instead of <template> tag */
		// 
		// /* Holds the CSS Text to style the shadow */
		// dummyVar01 = "";
		// 
		// /* Index for div.container {} rules */
		// dummyVar02 = CSSSheet.insertRule("div.container {}", CSSSheet.cssRules.length);
		// //  CSSSheet.cssRules[dummyVar02].style.setProperty("justify-content", "center");
		// CSSSheet.cssRules[dummyVar02].style.setProperty("justify-content", "center");
		// CSSSheet.cssRules[dummyVar02].style.setProperty("align-items", "center");
		// 
		// /* Index for div.background {} rules */
		// dummyVar03 = CSSSheet.insertRule("div.background {}", CSSSheet.cssRules.length);
		// CSSSheet.cssRules[dummyVar03].style.setProperty("text-align", "center");
		// CSSSheet.cssRules[dummyVar03].style.setProperty("padding", "2rem 0rem");
		// CSSSheet.cssRules[dummyVar03].style.setProperty("background-color", "rgba(255, 80, 80, 1)");
		// CSSSheet.cssRules[dummyVar03].style.setProperty("border", "2px solid " + "rgba(175, 0, 0, 1)");
		// CSSSheet.cssRules[dummyVar03].style.setProperty("border-radius", "2.5rem");
		// 
		// /* Index for div.greeting {} rules */
		// dummyVar04 = CSSSheet.insertRule("div.greeting {}", CSSSheet.cssRules.length);
		// CSSSheet.cssRules[dummyVar04].style.setProperty("padding", "2rem");
		// CSSSheet.cssRules[dummyVar04].style.setProperty("color", "rgba(255, 255, 255, 1)");
		// CSSSheet.cssRules[dummyVar04].style.setProperty("font-size", "15pt");
		// 
		// /* Index for div.name {} rules */
		// dummyVar05 = CSSSheet.insertRule("div.name {}", CSSSheet.cssRules.length);
		// CSSSheet.cssRules[dummyVar05].style.setProperty("padding", "1.5rem");
		// CSSSheet.cssRules[dummyVar05].style.setProperty("background-color", "rgba(255, 255, 255, 1)");
		// CSSSheet.cssRules[dummyVar05].style.setProperty("font-weight", "800");
		// CSSSheet.cssRules[dummyVar05].style.setProperty("font-size", "40pt");
		// 
		// for (i = 3; i < CSSSheet.cssRules.length; i += 1) {
		// dummyVar01 += CSSSheet.cssRules[i].cssText + " ";
		// }
		// 
		// /* 
		// Required target markup 
		// 
		// <div class="background">
		// <div class="greeting"><span>Hello</span></div>
		// <div class="name"><span>Nikita Sirwani</span></div>
		// </div>
		// */
		// 
		// /* Declaration of semantic tags */
		// 
		// /* <template> tag declaration */
		// semanticTags.templateTag = document.createElement("template");
		// 
		// /* <style> tag declaration */
		// semanticTags.styleTag = document.createElement("style");
		// 
		// /* <div> tag declaration */
		// semanticTags.divTag = document.createElement("div");
		// 
		// /* <span> tag declaration */
		// semanticTags.spanTag = document.createElement("span");
		// 
		// /* <content> tag declaration */
		// semanticTags.contentTag = document.createElement("content");
		// 
		// /* Declaration of tags used in markup */
		// 
		// /* <template> tag in markup */
		// markupTags.templateDiv = semanticTags.templateTag.cloneNode(true);
		// 
		// /* <style> tag in markup */
		// markupTags.styleTag = semanticTags.styleTag.cloneNode("true");
		// markupTags.styleTag.textContent = dummyVar01; /* CSS lines are fed into <style> tag */
		// 
		// /* <div class="background"> tag in markup */
		// markupTags.backgroundDiv = semanticTags.divTag.cloneNode(true);
		// markupTags.backgroundDiv.classList.add("background");
		// 
		// /* <div class="greeting"> tag in markup */
		// markupTags.greetingDiv = semanticTags.divTag.cloneNode(true);
		// markupTags.greetingDiv.classList.add("greeting");
		// 
		// /* <div class="greeting"> : <span> tag in markup */
		// markupTags.greetingSpan = semanticTags.spanTag.cloneNode(true);
		// markupTags.greetingSpan.textContent = "Hello";
		// markupTags.greetingDiv.appendChild(markupTags.greetingSpan);
		// 
		// /* <div class="name"> tag in markup */
		// markupTags.nameDiv = semanticTags.divTag.cloneNode(true);
		// markupTags.nameDiv.classList.add("name");
		// 
		// /* <div class="name"> : <span> tag in markup */
		// markupTags.nameSpan = semanticTags.spanTag.cloneNode(true);
		// markupTags.nameSpan.textContent = "Nikita Sirwani";
		// markupTags.nameDiv.appendChild(markupTags.nameSpan);
		// 
		// /* <div class="name"> : <span> tag in markup */
		// //  markupTags.contentTag = semanticTags.contentTag.cloneNode(true);
		// //  markupTags.nameDiv.appendChild(markupTags.contentTag);
		// 
		// markupTags.backgroundDiv.appendChild(markupTags.greetingDiv);
		// markupTags.backgroundDiv.appendChild(markupTags.nameDiv);
		// 
		// markupTags.templateDiv.content.appendChild(markupTags.styleTag);
		// markupTags.templateDiv.content.appendChild(markupTags.backgroundDiv);
		// 
		// var shadow = document.querySelectorAll("div#name-tag")[0].attachShadow({
		// mode: "open"
		// });
		// 
		// shadow.appendChild(document.importNode(markupTags.templateDiv.content, true));

		/* End : Separating content from Presentation */

		/* -------1------- */

		/* -------2------- */

		/* Start: Request and Cancel Animation Frames */

		/* Animation Canvas Element */
		// dummyVar01 = document.querySelectorAll("div.animation-canvas")[0];
		// 
		// /* flag */
		// var flag = false;
		// 
		// function callFunction() {
		// 
		// /* 
		// Get Parameter for Stop Condition 
		// Font Size in this case
		// */
		// function getFontSize() {
		// /*
		// If font size is undefined
		// Retrieve it from window getComputedStyle() function
		// Else
		// Retrieve the value from stylesheet
		// */
		// if (CSSSheet.cssRules[3].style.fontSize === "") {
		// return parseInt(window.getComputedStyle(dummyVar01, null).fontSize);
		// } else {
		// return parseInt(CSSSheet.cssRules[3].style.fontSize);
		// }
		// };
		// 
		// /* Parser */
		// dummyVar05 = getFontSize();
		// 
		// /* 
		// If flag is true
		// Perform Ascending Function
		// */
		// if (flag) {
		// 
		// /* Stop Condition */
		// if (dummyVar05 >= 180) {
		// window.cancelAnimationFrame(dummyVar03);
		// }
		// 
		// /* Perform the animation */
		// else {
		// dummyVar05 += 4;
		// CSSSheet.cssRules[3].style.setProperty("font-size", dummyVar05 + "px");
		// if (dummyVar05 <= 180) {
		// CSSSheet.cssRules[3].style.setProperty("transform", "rotate(" + dummyVar05 + "deg)");
		// }
		// 
		// /* ID of animation frame */
		// dummyVar03 = window.requestAnimationFrame(callFunction);
		// }
		// }
		// 
		// /* 
		// If flag is false
		// Perform Descending Function
		// */
		// else {
		// 
		// /* Stop Condition */
		// if (dummyVar05 <= 0) {
		// window.cancelAnimationFrame(dummyVar03);
		// }
		// 
		// /* Perform the animation */
		// else {
		// dummyVar05 -= 4;
		// CSSSheet.cssRules[3].style.setProperty("font-size", dummyVar05 + "px");
		// if (dummyVar05 <= 180) {
		// CSSSheet.cssRules[3].style.setProperty("transform", "rotate(" + dummyVar05 + "deg)");
		// }
		// 
		// /* ID of animation frame */
		// dummyVar03 = window.requestAnimationFrame(callFunction);
		// }
		// }
		// }
		// 
		// /* Event Listener */
		// dummyVar01.addEventListener("click", function () {
		// window.requestAnimationFrame(callFunction);
		// flag = !flag;
		// }, false);

		/* End: Request and Cancel Animation Frames */

		/* Start: window.requestanimationframe with time duration */

		/* Animation Canvas Element */
		// dummyVar01 = document.querySelectorAll("div.animation-canvas")[0];

		// /* Start Time */
		// dummyVar02 = 0;

		// /* CSS Rule index for keyframe rule */
		// dummyVar03 = [];

		// /* CSS Rule index for Animation Canvas Element */
		// dummyVar04 = [];

		// /* Animation ID */
		// dummyVar09 = 0;

		// /* Keyframe rule in CSS Stylesheet */
		// for (i = 0; i < CSSSheet.cssRules.length; i += 1) {
		// if (CSSSheet.cssRules[i].type === 7) {
		// dummyVar03[i] = i;
		// }
		// }

		// dummyVar03 = dummyVar03.filter(function (elements) {
		// return elements !== undefined;
		// });

		// for (i = 0; i < CSSSheet.cssRules.length; i += 1) {
		// if (CSSSheet.cssRules[i].type === 1) {
		// if (CSSSheet.cssRules[i].selectorText.indexOf("div.animation-canvas") >= 0) {
		// dummyVar04[i] = i;
		// }
		// }
		// }

		// dummyVar04 = dummyVar04.filter(function (elements) {
		// return elements !== undefined;
		// });

		// /* Function to scale the letter over user defined time */
		// function scaleAlternate() {

		// /* Start Time */
		// dummyVar05 = 0;

		// function doAnimation(timestamp, element, factor, duration) {

		// /* Timestamp */
		// dummyVar06 = timestamp;

		// /* Runtime */
		// dummyVar07 = dummyVar06 - dummyVar05;

		// /* Progress */
		// dummyVar08 = dummyVar07 / duration;
		// dummyVar08 = Math.min(dummyVar08, 1);

		// /* Animate here */
		// CSSSheet.cssRules[dummyVar04[0]].style.setProperty("transform", "scale(" + (factor * dummyVar08) + ", " + (factor * dummyVar08) + ")");

		// /* 
		// * 
		// * Condition to check the animation playstate 
		// * 
		// * true = playing
		// * false = end
		// * 
		// */

		// if (dummyVar08 < duration) {
		// dummyVar09 = window.requestAnimationFrame(function (timestamp) {
		// doAnimation(timestamp, element, factor, duration);
		// });
		// } else {
		// window.cancelAnimationFrame(dummyVar09);
		// }
		// }

		// dummyVar09 = window.requestAnimationFrame(function (timestamp) {
		// dummyVar05 = timestamp;
		// doAnimation(dummyVar05, dummyVar01, 1, 500);
		// });
		// }

		// /* Event Listeners here */
		// document.body.addEventListener("click", function () {
		// scaleAlternate()
		// }, false);

		/* End: window.requestanimationframe with time duration */

		/* -------2------- */

		/* -------3------- */

		/* Start: Blob pratice */

		/* input textbox */
		// dummyVar01 = document.querySelectorAll("input[type=\"text\"]")[0];

		/* submit button */
		// dummyVar02 = document.querySelectorAll("span.button")[0];
		// dummyVar02.setAttribute("tabindex", "0");

		/* populate the input text into blob */
		// function populateIntoBlob(populator) {

		/* blob */

		// dummyVar03 = new Blob([populator], {
		// type: "text/plain"
		// });

		// readBlob(dummyVar03);
		// }

		// function readBlob(blob) {
		//
		// /* file reader */
		// dummyVar05 = new FileReader();
		//
		// dummyVar05.readAsDataURL(blob);
		// dummyVar05.addEventListener("loadend", function () {
		// console.log(this.result);
		// }, false);
		// }

		// dummyVar02.addEventListener("click", function () {

		/* blob populator */
		// dummyVar04 = this.previousElementSibling.value;

		// populateIntoBlob(dummyVar04);
		// }, false);

		// dummyVar02.addEventListener("keyup", function (event) {
		// if (event.keyCode === 32 || event.keyCode === 13) {
		//
		// /* blob populator */
		// dummyVar04 = this.previousElementSibling.value;
		//
		// populateIntoBlob(dummyVar04);
		// }
		// }, false);

		/* End: Blob pratice */

		/* -------3------- */

		/* -------4------- */

		/* Start: Promise */

		/* Buttons */
		// dummyVar02 = document.querySelectorAll("span.button");

		// dummyVar02[2].addEventListener("click", function () {
		//  promiseFunction();
		// }, false);

		/* Promise function */
		// function promiseFunction() {

		/* Promise variable */
		// dummyVar01 = new Promise(function (resolve, reject) {
		// dummyVar02[0].addEventListener("click", function () {
		// resolve("The promise is resolved by the button \"" + this.textContent + "\"");
		// }, false);
		// dummyVar02[1].addEventListener("click", function () {
		// reject("The promise is rejected by the button \"" + this.textContent + "\"");
		// }, false);
		// });
		//
		// dummyVar01.then(function successMessage(message) {
		// console.log(message)
		// }).catch(function failureMessage(message) {
		// console.log(message);
		// });

		// promiseFunction();
		// }
		// promiseFunction();

		/* End: Promise */

		/* -------4------- */

		/* -------5------- */

		/* Start: HTML IndexedDB */

		// function indexedDBInteraction() {
		// /* Buttons Array */
		// dummyVar06 = document.querySelectorAll("span.button");
		//
		// /* Button 1 */
		// dummyVar01 = dummyVar06[0];
		//
		// /* Button 2 */
		// dummyVar02 = dummyVar06[1];
		//
		// /* Button 3 */
		// dummyVar03 = dummyVar06[2];
		//
		// /* Button 4 */
		// dummyVar04 = dummyVar06[3];
		//
		// /* Button 5 */
		// dummyVar05 = dummyVar06[4];
		//
		// /* JSON Data */
		// const employeeData = [
		// {
		// "id": "10619461",
		// "name": "Manprit Singh",
		// "contact": "+91 889 884 2226",
		// "project": "Reliant"
		// }, {
		// "id": "10619647",
		// "name": "Usama Ansari",
		// "contact": "+91 809 772 8350",
		// "project": "CAM"
		// }, {
		// "id": "10620243",
		// "name": "Tanvi Lad",
		// "contact": "+91 902 223 7199",
		// "project": "CAM"
		// }, {
		// "id": "10620841",
		// "name": "Nikita Sirwani",
		// "contact": "+91 808 737 8899",
		// "project": "Petrogas"
		// }
		// ]
		//
		// /* database object */
		// var
		// db = {
		// "instance": {},
		// "name": "employeeDatabase",
		// "version": 1,
		// "storeName": "Employees",
		// "index": ""
		// },
		// transaction,
		// objectStore;
		//
		// /* IDBRequest */
		// var request = window.indexedDB.open(db["name"], db["version"]);
		//
		// console.log(request);
		//
		// request.addEventListener("error", function () {
		// console.log("Some Error in Database");
		// }, false);
		//
		// request.addEventListener("upgradeneeded", function (event) {
		// console.log("Upgrade Needed triggered");
		//
		// // db["instance"] = this.result;
		// //
		// // /* objectStore creation */
		// // objectStore = db["instance"].createObjectStore("Employee", {
		// // keyPath: "id"
		// // });
		// //
		// // /* index creation */
		// // db["index"] = objectStore.createIndex(db["instance"], db["instance"], {
		// // unique: true
		// // })
		// //
		// // for (var i in employeeData) {
		// // objectStore.add(employeeData[i]);
		// // }
		//
		// console.log(db);
		//
		// }, false);
		//
		// request.addEventListener("success", function (event) {
		// // console.log("Database Open Successful");
		// //
		// // /* "this" is the IDBRequest */
		// // db["instance"] = this;
		// //
		// // transaction = db["instance"].transaction(db["instance"], "readwrite");
		// //
		// // console.log(db);
		// //
		// }, false);
		//
		// function read() {
		// console.log("inside read");
		//
		// // var
		// // transaction = db.instance.transaction("employee"),
		// // objectStore = transaction.objectStore("employee"),
		// // objectRequest = objectStore.get("10620243");
		// //
		// // console.log("in here");
		// //
		// // objectRequest.addEventListener("error", function () {
		// // console.log("Object Request not found");
		// // }, false);
		// //
		// // objectRequest.addEventListener("success", function () {
		// // console.log("Here are the details");
		// // console.log("PS Number: " + objectRequest.result.id);
		// // }, false);
		//
		// }
		//
		// dummyVar01.addEventListener("click", function () {
		// read();
		// }, false);
		// }
		//
		// indexedDBInteraction();

		/* End: HTML IndexedDB */

		/* -------5------- */

		/* -------6------- */

		/* Start: JavaScript Debounce */

		// /* Debounce handler Object */
		// dummyVar01 = {};
		// 
		// /* Debounce Function */
		// dummyVar01.debounce = function (targetFunction, wait, immediate) {
		// 
		// /* Animation ID */
		// dummyVar02 = 0;
		// 
		// /* Animation start time */
		// dummyVar03 = 0;
		// return function () {
		// 
		// /* Context */
		// dummyVar04 = this;
		// 
		// /* Array of Arguments */
		// dummyVar05 = arguments;
		// 
		// /*
		// * 
		// * This function is called to
		// * the debounce trigger
		// *
		// */
		// function later() {
		// dummyVar02 = 0;
		// 
		// /* 
		// * 
		// * IF condition to check whether 
		// * changes must be performed 
		// * immediately or with a delay
		// *
		// */
		// if (!immediate) {
		// 
		// /* Apply context and arguments to the target function */
		// targetFunction.apply(dummyVar04, dummyVar05);
		// }
		// }
		// 
		// /* Flag to call debounced function */
		// dummyVar06 = immediate && !dummyVar02;
		// 
		// /* Delimiter / progress of window.requestanimationframe */
		// dummyVar07 = 0;
		// 
		// window.cancelAnimationFrame(dummyVar02);
		// 
		// function doAnimation(timestamp, duration) {
		// 
		// /* Duration */
		// dummyVar08 = duration;
		// 
		// /* Timestamp */
		// dummyVar09 = timestamp;
		// 
		// /*
		// * 
		// * Progress is calculated as follows
		// * 
		// * 			  (timestamp - startTime)
		// * Progress = -------------------------
		// * 					duration
		// * 
		// */
		// 
		// dummyVar07 = (dummyVar09 - dummyVar03) / dummyVar08;
		// 
		// if (dummyVar07 > 1) {
		// window.cancelAnimationFrame(dummyVar02);
		// later();
		// } else {
		// dummyVar02 = window.requestAnimationFrame(function (timestamp) {
		// doAnimation(timestamp, dummyVar08);
		// });
		// }
		// }
		// 
		// dummyVar02 = window.requestAnimationFrame(function (timestamp) {
		// dummyVar03 = timestamp;
		// doAnimation(timestamp, wait || 200);
		// });
		// 
		// if (dummyVar06) {
		// targetFunction.apply(dummyVar04, dummyVar05);
		// }
		// };
		// };
		// 
		// /* To test the debounce function */
		// 
		// dummyVar01.nonImmediate = function () {
		// document.body.innerHTML = ""
		// document.body.style.backgroundColor = "rgba(" + Math.floor(Math.random() * 255) + ", " + Math.floor(Math.random() * 255) + ", " + Math.floor(Math.random() * 255) + ", 1)"
		// document.body.innerHTML = "Color changed when scrolled";
		// };
		// 
		// dummyVar01.immediate = function () {
		// document.body.innerHTML = "";
		// document.body.style.backgroundColor = "rgba(" + Math.floor(Math.random() * 255) + ", " + Math.floor(Math.random() * 255) + ", " + Math.floor(Math.random() * 255) + ", 1)";
		// document.body.innerHTML = "Color changed when resized";
		// };
		// 
		// /* Event Listeners */
		// 
		// /* 
		// * 
		// * When the window is scrolled
		// * the nonImmediate function is invoked
		// * 
		// * The changes take place after the
		// * window is scrolled and delay is elapsed
		// * 
		// */
		// window.addEventListener("wheel", dummyVar01.debounce(dummyVar01.nonImmediate, 400, false), false);
		// 
		// /* 
		// * 
		// * When the window is resized
		// * the immediate function is invoked
		// * 
		// * The changes take place immediately after 
		// * window is resized without any delay
		// * 
		// */
		// window.addEventListener("resize", dummyVar01.debounce(dummyVar01.immediate, 400, true), false);

		/* End: JavaScript Debounce */

		/* -------6------- */

		/* -------7------- */

		/* Start: JavaScript Command Pattern */


		/* End: JavaScript Command Pattern */

		/* -------7------- */
}, false);
