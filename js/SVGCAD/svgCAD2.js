window.document.addEventListener("DOMContentLoaded", function () {
	(function () {
		var
			nameSpace = "http://www.w3.org/2000/svg",

			fixedConsole = window.document.querySelectorAll("div.fixed-console")[0],

			svgSemantic = {
				tag: window.document.createElementNS(nameSpace, "svg"),
				group: window.document.createElementNS(nameSpace, "g"),
				path: window.document.createElementNS(nameSpace, "path"),
				line: window.document.createElementNS(nameSpace, "line"),
				circle: window.document.createElementNS(nameSpace, "circle"),
				polygon: window.document.createElementNS(nameSpace, "polygon"),
				ellipse: window.document.createElementNS(nameSpace, "ellipse"),
				image: window.document.createElementNS(nameSpace, "image"),
				polyLine: window.document.createElementNS(nameSpace, "polyline"),
				text: window.document.createElementNS(nameSpace, "text")
			},

			svgMarkup = {
				tag: window.document.querySelectorAll("svg")[1]
			},

			eventObject = {};

		svgMarkup.viewBox = {
			string: svgMarkup.tag.getAttribute("viewBox")
		};

		svgMarkup.viewBox.startX = parseInt(svgMarkup.viewBox.string.split(" ")[0]);
		svgMarkup.viewBox.startY = parseInt(svgMarkup.viewBox.string.split(" ")[1]);
		svgMarkup.viewBox.endX = parseInt(svgMarkup.viewBox.string.split(" ")[2]);
		svgMarkup.viewBox.endY = parseInt(svgMarkup.viewBox.string.split(" ")[3]);

		svgMarkup.graphic = {
			group: svgSemantic.group.cloneNode(true)
		};

		svgMarkup.graphic.group.setAttribute("id", "graphic");

		(function svgCanvas() {
			svgMarkup.canvas = {
				group: svgSemantic.group.cloneNode(true),
				polygon: {
					element: svgSemantic.polygon.cloneNode(true),
					pointsString: svgMarkup.viewBox.startX + ", " + svgMarkup.viewBox.startY + ", " + svgMarkup.viewBox.endX + ", " + svgMarkup.viewBox.startY + ", " + svgMarkup.viewBox.endX + ", " + svgMarkup.viewBox.endY + ", " + svgMarkup.viewBox.startX + ", " + svgMarkup.viewBox.endY
				}

			};

			svgMarkup.canvas.group.setAttribute("id", "canvas");

			svgMarkup.canvas.polygon.element.setAttribute("points", svgMarkup.canvas.polygon.pointsString);

			svgMarkup.canvas.group.appendChild(svgMarkup.canvas.polygon.element);

		})();

		(function createGrid() {
			svgMarkup.grid = {
				group: svgSemantic.group.cloneNode(true),
				vertical: svgSemantic.group.cloneNode(true),
				horizontal: svgSemantic.group.cloneNode(true),
				line: svgSemantic.line.cloneNode(true)
			};

			svgMarkup.grid.group.setAttribute("id", "grid");
			svgMarkup.grid.vertical.setAttribute("id", "vertical-grid-lines");
			svgMarkup.grid.horizontal.setAttribute("id", "horizontal-grid-lines");

			for (let i = svgMarkup.viewBox.startX; i <= svgMarkup.viewBox.endX; i += 10) {
				svgMarkup.grid.line.setAttribute("x1", i);
				svgMarkup.grid.line.setAttribute("y1", svgMarkup.viewBox.startY);
				svgMarkup.grid.line.setAttribute("x2", i);
				svgMarkup.grid.line.setAttribute("y2", svgMarkup.viewBox.endY);

				if (i !== svgMarkup.viewBox.startX && i !== svgMarkup.viewBox.endX && i % 50 === 0) {
					svgMarkup.grid.line.classList.add("grid-fifty");
				} else {
					svgMarkup.grid.line.classList.remove("grid-fifty");
				}

				svgMarkup.grid.vertical.appendChild(svgMarkup.grid.line.cloneNode(true));
			}

			for (let i = svgMarkup.viewBox.startY; i <= svgMarkup.viewBox.endY; i += 10) {
				svgMarkup.grid.line.setAttribute("x1", svgMarkup.viewBox.startX);
				svgMarkup.grid.line.setAttribute("y1", i);
				svgMarkup.grid.line.setAttribute("x2", svgMarkup.viewBox.endX);
				svgMarkup.grid.line.setAttribute("y2", i);

				if (i !== svgMarkup.viewBox.startY && i !== svgMarkup.viewBox.endY && i % 50 === 0) {
					svgMarkup.grid.line.classList.add("grid-fifty");
				} else {
					svgMarkup.grid.line.classList.remove("grid-fifty");
				}

				svgMarkup.grid.horizontal.appendChild(svgMarkup.grid.line.cloneNode(true));
			}

			svgMarkup.grid.group.appendChild(svgMarkup.grid.vertical);
			svgMarkup.grid.group.appendChild(svgMarkup.grid.horizontal);
		})();

		function cadDrafter(activeView) {

			if (activeView === undefined) {
				activeView = "default";
			}

			svgMarkup.drafter = {
				group: svgSemantic.group.cloneNode(true),
				vertical: svgSemantic.line.cloneNode(true),
				horizontal: svgSemantic.line.cloneNode(true),
				square: {
					element: svgSemantic.polygon.cloneNode(true),
					points: ""
				}
			};

			svgMarkup.drafter.group.setAttribute("id", "drafter");
			svgMarkup.drafter.horizontal.setAttribute("id", "horizontal");
			svgMarkup.drafter.vertical.setAttribute("id", "vertical");

			function drafterType(event, activeView) {
				var userPoint = svgMarkup.tag.createSVGPoint();
				userPoint.x = event.clientX;
				userPoint.y = event.clientY;

				var svgPoint = userPoint.matrixTransform(svgMarkup.tag.getScreenCTM().inverse());

				svgPoint.x = Math.round(svgPoint.x);
				svgPoint.y = Math.round(svgPoint.y);

				var
					switchCase = {
						"default": function () {
							svgMarkup.drafter.vertical.setAttribute("x1", svgPoint.x);
							svgMarkup.drafter.vertical.setAttribute("y1", svgPoint.y - 30);
							svgMarkup.drafter.vertical.setAttribute("x2", svgPoint.x);
							svgMarkup.drafter.vertical.setAttribute("y2", svgPoint.y + 30);

							svgMarkup.drafter.horizontal.setAttribute("x1", svgPoint.x - 30);
							svgMarkup.drafter.horizontal.setAttribute("y1", svgPoint.y);
							svgMarkup.drafter.horizontal.setAttribute("x2", svgPoint.x + 30);
							svgMarkup.drafter.horizontal.setAttribute("y2", svgPoint.y);

							svgMarkup.drafter.square.points =
								(svgPoint.x - 5) + ", " + (svgPoint.y - 5) + ", " +
								(svgPoint.x + 5) + ", " + (svgPoint.y - 5) + ", " +
								(svgPoint.x + 5) + ", " + (svgPoint.y + 5) + ", " +
								(svgPoint.x - 5) + ", " + (svgPoint.y + 5);

							svgMarkup.drafter.square.element.setAttribute("points", svgMarkup.drafter.square.points);

						},
						"line": function () {}

					},

					defCase = null;

				if (switchCase[activeView]) {
					defCase = switchCase[activeView];
				} else {
					defCase = switchCase["default"];
				}
				return defCase();
			}

			svgMarkup.tag.addEventListener("mousemove", function (event) {
				drafterType(event, activeView);
			}, false);

			svgMarkup.drafter.group.appendChild(svgMarkup.drafter.vertical);
			svgMarkup.drafter.group.appendChild(svgMarkup.drafter.horizontal);
			svgMarkup.drafter.group.appendChild(svgMarkup.drafter.square.element);

			svgMarkup.tag.addEventListener("mouseleave", function () {
				svgMarkup.drafter.vertical.removeAttribute("x1");
				svgMarkup.drafter.vertical.removeAttribute("y1");
				svgMarkup.drafter.vertical.removeAttribute("x2");
				svgMarkup.drafter.vertical.removeAttribute("y2");

				svgMarkup.drafter.horizontal.removeAttribute("x1");
				svgMarkup.drafter.horizontal.removeAttribute("y1");
				svgMarkup.drafter.horizontal.removeAttribute("x2");
				svgMarkup.drafter.horizontal.removeAttribute("y2");

				svgMarkup.drafter.square.element.removeAttribute("points");
			}, false);

		}
		cadDrafter("default");

		function draw(activeView, points) {

			// svgMarkup.graphic.intermediateGraphic = undefined;

			var
				switchCase = {
					"line": function () {
						svgMarkup.graphic.intermediateGraphic = svgSemantic.line.cloneNode(true);
						for (let i = 0; i < points.length; i += 1) {
							svgMarkup.graphic.intermediateGraphic.setAttribute("x" + (i + 1), points[i].x);
							svgMarkup.graphic.intermediateGraphic.setAttribute("y" + (i + 1), points[i].y);
						}
						svgMarkup.graphic.group.appendChild(svgMarkup.graphic.intermediateGraphic);
					},
					"circle": function () {
						svgMarkup.graphic.intermediateGraphic = svgSemantic.circle.cloneNode(true);
						var radius = 0;
						radius = Math.sqrt(Math.pow(((points[1].x) - points[0].x), 2) + Math.pow(((points[1].y) - points[0].y), 2));
						svgMarkup.graphic.intermediateGraphic.setAttribute("cx", points[0].x);
						svgMarkup.graphic.intermediateGraphic.setAttribute("cy", points[0].y);
						svgMarkup.graphic.intermediateGraphic.setAttribute("r", radius);
						svgMarkup.graphic.group.appendChild(svgMarkup.graphic.intermediateGraphic);
					}
				},
				defCase;


			if (switchCase[activeView]) {
				defCase = switchCase[activeView];
			} else {
				defCase = switchCase["default"];
			}
			return defCase();
		}

		function executeCommand(activeView) {
			var
				switchCase = {
					"line": function () {
						var clickCount = 0;
						var points = [];

						eventObject.lineEvent = function (event) {

							clickCount += 1;

							var userPoint = svgMarkup.tag.createSVGPoint();
							userPoint.x = event.clientX;
							userPoint.y = event.clientY;

							var svgPoint = userPoint.matrixTransform(svgMarkup.tag.getScreenCTM().inverse());

							if (clickCount === 2) {
								points[clickCount - 1] = {};
								points[clickCount - 1].x = Math.round(svgPoint.x / 5) * 5;
								points[clickCount - 1].y = Math.round(svgPoint.y / 5) * 5;

								fixedConsole.textContent = "Line Drawn";
								draw(activeView, points);
								cancelCommand(activeView);
							} else {
								fixedConsole.textContent = "Specify Second Point";

								points[clickCount - 1] = {};
								points[clickCount - 1].x = Math.round(svgPoint.x / 5) * 5;
								points[clickCount - 1].y = Math.round(svgPoint.y / 5) * 5;
							}
						};

						fixedConsole.textContent = "Specify First Point";
						svgMarkup.tag.addEventListener("click", eventObject.lineEvent, false);
					},
					"circle": function () {
						var clickCount = 0;
						var points = [];

						eventObject.circleEvent = function (event) {

							clickCount += 1;

							var userPoint = svgMarkup.tag.createSVGPoint();
							userPoint.x = event.clientX;
							userPoint.y = event.clientY;

							var svgPoint = userPoint.matrixTransform(svgMarkup.tag.getScreenCTM().inverse());

							if (clickCount === 2) {
								points[clickCount - 1] = {};
								points[clickCount - 1].x = Math.round(svgPoint.x / 5) * 5;
								points[clickCount - 1].y = Math.round(svgPoint.y / 5) * 5;

								fixedConsole.textContent = "Line Drawn";
								draw(activeView, points);
								cancelCommand(activeView);
							} else {
								fixedConsole.textContent = "Specify Second Point";

								points[clickCount - 1] = {};
								points[clickCount - 1].x = Math.round(svgPoint.x / 5) * 5;
								points[clickCount - 1].y = Math.round(svgPoint.y / 5) * 5;
							}
						};

						fixedConsole.textContent = "Specify First Point";
						svgMarkup.tag.addEventListener("click", eventObject.circleEvent, false);
					},
					"polygon": function () {

					},
					"default": function () {

					}
				},
				defCase;

			if (switchCase[activeView]) {
				defCase = switchCase[activeView];
			} else {
				defCase = switchCase["default"];
			}
			return defCase();
		}

		function cancelCommand(activeView) {
			var
				switchCase = {
					"line": function () {
						fixedConsole.textContent = "";
						svgMarkup.tag.removeEventListener("click", eventObject.lineEvent, false);
					},
					"circle": function () {
						fixedConsole.textContent = "";
						svgMarkup.tag.removeEventListener("click", eventObject.circleEvent, false);
					},
					"polygon": function () {

					}
				},
				defCase;

			if (switchCase[activeView]) {
				defCase = switchCase[activeView];
			} else {
				defCase = switchCase["default"];
			}

			return defCase();

		}

		(function eventListeners() {
			var
				command = {
					line: window.document.querySelectorAll("div.command-button")[0],
					polygon: window.document.querySelectorAll("div.command-button")[1],
					circle: window.document.querySelectorAll("div.command-button")[2],
					intersection: window.document.querySelectorAll("div.command-button")[3]
				},
				activeView = "default";

			command.line.addEventListener("click", function () {
				activeView = "line"
				executeCommand(activeView);
			}, false);

			command.circle.addEventListener("click", function () {
				activeView = "circle"
				executeCommand(activeView);
			}, false);


			command.intersection.addEventListener("click", function () {
				var firstLine = window.document.querySelectorAll("g#graphic *")[0];
				var secondLine = window.document.querySelectorAll("g#graphic *")[1];

				var firstPoints = [];
				var secondPoints = [];

				for (let i = 0; i < firstLine.getTotalLength(); i += 0.1) {
					// firstPoints[Math.round(i*10)] = firstLine.getPointAtLength(i);
					firstPoints[Math.round(i * 10)] = firstLine.getPointAtLength(i);
				}

				for (let i = 0; i < secondLine.getTotalLength(); i += 0.1) {
					// secondPoints[Math.round(i*10)] = secondLine.getPointAtLength(i);
					secondPoints[Math.round(i * 10)] = secondLine.getPointAtLength(i);
				}

				// console.log(firstPoints);
				// console.log(secondPoints);

				var intersectionMap = new Map();

				var counter = 0;

				for (let i = 0; i < firstPoints.length; i += 1) {
					for (let j = 0; j < secondPoints.length; j += 1) {
						// if (Math.round(firstPoints[i].x) === Math.round(secondPoints[j].x)) {
						if (firstPoints[i].x === secondPoints[j].x) {
							// if (Math.round(firstPoints[i].y) === Math.round(secondPoints[j].y)) {
							if (firstPoints[i].y === secondPoints[j].y) {
								intersectionMap.set("x" + (counter + 1), Math.round(firstPoints[i].x));
								intersectionMap.set("y" + (counter + 1), Math.round(firstPoints[i].y));
								counter += 1;
							} else {}
						}
					}
				}

				if (intersectionMap.size === 0) {
					console.log("Graphics do not intersect");
				} else {
					console.log("Graphics intersect");
					console.log(intersectionMap);
				}

			}, false);


			window.addEventListener("keydown", function (event) {
				if (event.keyCode === 27) {
					cancelCommand(activeView);
					activeView = "default";
				}
			}, false);

		})();

		svgMarkup.tag.appendChild(svgMarkup.canvas.group);
		svgMarkup.tag.appendChild(svgMarkup.grid.group);
		svgMarkup.tag.appendChild(svgMarkup.drafter.group);
		svgMarkup.tag.appendChild(svgMarkup.graphic.group);

	})();
}, false);
