window.document.addEventListener("DOMContentLoaded", function () {
	(function () {
		"use strict";
		var nameSpace = "http://www.w3.org/2000/svg";
		var svg = {
			tag: document.querySelectorAll("svg")[1],
		};

		var indicationPointClone = document.createElementNS(nameSpace, "circle");
		var graphicLineClone = document.createElementNS(nameSpace, "line");
		var fixedConsole = document.querySelectorAll("div.fixed-console")[0];

		var clickEvent = null;

		svg.viewBox = {
			string: svg.tag.getAttribute("viewBox")
		};

		svg.viewBox.startX = parseInt(svg.viewBox.string.split(" ")[0]);
		svg.viewBox.startY = parseInt(svg.viewBox.string.split(" ")[1]);
		svg.viewBox.endX = parseInt(svg.viewBox.string.split(" ")[2]);
		svg.viewBox.endY = parseInt(svg.viewBox.string.split(" ")[3]);

		function svgCanvas() {
			svg.canvas = {};

			svg.canvas.group = document.createElementNS(nameSpace, "g");
			svg.canvas.group.setAttribute("id", "canvas-holder");
			svg.canvas.element = document.createElementNS(nameSpace, "polygon")
			svg.canvas.element.setAttribute("id", "canvas");

			var polygonPoints = "";
			polygonPoints = svg.viewBox.startX + ", " + svg.viewBox.startY + ", " +
				svg.viewBox.endX + ", " + svg.viewBox.startY + ", " +
				svg.viewBox.endX + ", " + svg.viewBox.endY + ", " +
				svg.viewBox.startX + ", " + svg.viewBox.endY;
			svg.canvas.element.setAttribute("points", polygonPoints);
			svg.canvas.group.appendChild(svg.canvas.element);

			svg.tag.appendChild(svg.canvas.group);
		}
		svgCanvas();

		svg.graphic = {};
		svg.graphic.group = document.createElementNS(nameSpace, "g");
		svg.graphic.group.setAttribute("id", "graphic");

		svg.graphic.designGroup = document.createElementNS(nameSpace, "g");
		svg.graphic.designGroup.classList.add("graphic-design");

		svg.graphic.exportGroup = document.createElementNS(nameSpace, "g");
		svg.graphic.exportGroup.classList.add("graphic-export");
		svg.graphic.exportGroup.classList.add("hidden");

		var activeView = "default";

		function comments() {
			/* Keyboard Inputs */
			// var commandObject = {
			// line: 76,
			// circle: 67,
			// rect: 82,
			// move: 77,
			// erase: 69
			// };
			// 
			// var keyMap = new Map(Object.entries(commandObject));
			// 
			// var inputBox = document.querySelectorAll("div.form-element input[type=\"text\"]")[0];
			// 
			// /* Debounce handler Object */
			// var myFunctions = {};
			// 
			// /* Debounce Function */
			// myFunctions.debounce = function (targetFunction, wait, immediate) {
			// 
			// /*dummyVar02*/
			// /* Animation ID */
			// var animationID = 0;
			// 
			// /*dummyVar03*/
			// /* Animation start time */
			// var startTime = 0;
			// return function () {
			// 
			// /*dummyVar04*/
			// /* Context */
			// var context = this;
			// 
			// /*dummyVar05*/
			// /* Array of Arguments */
			// var argsArray = arguments;
			// 
			// /*
			// * 
			// * This function is called to
			// * the debounce trigger
			// *
			// */
			// function later() {
			// animationID = 0;
			// 
			// /* 
			// * 
			// * IF condition to check whether 
			// * changes must be performed 
			// * immediately or with a delay
			// *
			// */
			// if (!immediate) {
			// 
			// /* Apply context and arguments to the target function */
			// targetFunction.apply(context, argsArray);
			// }
			// }
			// 
			// /*dummyVar06*/
			// /* Flag to call debounced function */
			// var debounceFlag = immediate && !dummyVar02;
			// 
			// /*dummyVar07*/
			// /* Delimiter / progress of window.requestanimationframe */
			// var progress = 0;
			// 
			// window.cancelAnimationFrame(animationID);
			// 
			// function doAnimation(timestamp, duration) {
			// 
			// /*dummyVar08*/
			// /* Duration */
			// var duration = duration;
			// 
			// /*dummyVar09*/
			// /* Timestamp */
			// var timeStamp = timestamp;
			// 
			// /*
			// * 
			// * Progress is calculated as follows
			// * 
			// * 			  (timestamp - startTime)
			// * Progress = -------------------------
			// * 					duration
			// * 
			// */
			// 
			// progress = (timeStamp - startTime) / duration;
			// 
			// if (progress > 1) {
			// window.cancelAnimationFrame(animationID);
			// later();
			// } else {
			// animationID = window.requestAnimationFrame(function (timestamp) {
			// doAnimation(timestamp, duration);
			// });
			// }
			// }
			// 
			// animationID = window.requestAnimationFrame(function (timestamp) {
			// startTime = timestamp;
			// doAnimation(timestamp, wait || 200);
			// });
			// 
			// if (debounceFlag) {
			// targetFunction.apply(context, argsArray);
			// }
			// };
			// };
			// 
			// inputBox.addEventListener("keyup", myFunctions.debounce(function (event) {
			// if (inputBox.value !== "") {
			// console.log(inputBox.value.trim());
			// }
			// }, 250, false), false);
			/*
			--------------------------------------------------
			Keyboard Inputs
			--------------------------------------------------
			*/

			/* To study SVGGeometryElement */
			// var mySVGPoint = svg.tag.createSVGPoint();
			// mySVGPoint.x = svg.viewBox.startX;
			// mySVGPoint.y = svg.viewBox.startY;
			//
			// console.log(svg.outBound.isPointInFill(mySVGPoint));
			// console.log(svg.outBound.isPointInStroke(mySVGPoint));
			/*
			--------------------------------------------------
			SVGGeometryElement
			--------------------------------------------------
			*/

			/* Add a pivot point */
			// svg.tag.addEventListener("contextmenu", function (event) {
			// event.preventDefault();
			// var userPoint = svg.tag.createSVGPoint();
			// var svgPoint;
			// var userCircle = document.createElementNS(nameSpace, "circle");
			//
			// userPoint.x = event.clientX;
			// userPoint.y = event.clientY;
			//
			// svgPoint = userPoint.matrixTransform(svg.tag.getScreenCTM().inverse());
			//
			// userCircle.setAttribute("cx", Math.round(svgPoint.x / 10) * 10);
			// userCircle.setAttribute("cy", Math.round(svgPoint.y / 10) * 10);
			// userCircle.setAttribute("r", "5");
			//
			// svg.group.appendChild(userCircle);
			//
			// }, false);
			//
			// svg.tag.addEventListener("mousedown", function(event) {}, false);
			/*
			--------------------------------------------------
			Pivot point
			--------------------------------------------------
			*/

			/* Enable Zoom : Copy this code in event listeners function */

			// svg.tag.addEventListener("wheel", function (event) {
			// if (event.deltaY > 0) {
			//
			// svg.viewBox.startX = (Math.round(svg.viewBox.startX * 1.025) * 5) / 5;
			// svg.viewBox.startY = (Math.round(svg.viewBox.startY * 1.025) * 5) / 5;
			// svg.viewBox.endX = (Math.round(svg.viewBox.endX * 1.025) * 5) / 5;
			// svg.viewBox.endY = (Math.round(svg.viewBox.endY * 1.025) * 5) / 5;
			//
			// svg.tag.setAttribute("viewBox", svg.viewBox.startX + " " + svg.viewBox.startY + " " + svg.viewBox.endX + " " + svg.viewBox.endY);
			//
			// var polygonPoints = svg.viewBox.startX + ", " + svg.viewBox.startY + ", " +
			// svg.viewBox.endX + ", " + svg.viewBox.startY + ", " +
			// svg.viewBox.endX + ", " + svg.viewBox.endY + ", " +
			// svg.viewBox.startX + ", " + svg.viewBox.endY;
			// svg.canvas.element.setAttribute("points", polygonPoints);
			// svg.tag.removeChild(svg.grid.group);
			// createGrid();
			// // console.log("Zoom out");
			// } else {
			// svg.viewBox.startX = (Math.round(svg.viewBox.startX * 0.975) * 5) / 5;
			// svg.viewBox.startY = (Math.round(svg.viewBox.startY * 0.975) * 5) / 5;
			// svg.viewBox.endX = (Math.round(svg.viewBox.endX * 0.975) * 5) / 5;
			// svg.viewBox.endY = (Math.round(svg.viewBox.endY * 0.975) * 5) / 5;
			//
			// svg.tag.setAttribute("viewBox", svg.viewBox.startX + " " + svg.viewBox.startY + " " + svg.viewBox.endX + " " + svg.viewBox.endY);
			//
			// var polygonPoints = svg.viewBox.startX + ", " + svg.viewBox.startY + ", " +
			// svg.viewBox.endX + ", " + svg.viewBox.startY + ", " +
			// svg.viewBox.endX + ", " + svg.viewBox.endY + ", " +
			// svg.viewBox.startX + ", " + svg.viewBox.endY;
			// svg.canvas.element.setAttribute("points", polygonPoints);
			// svg.tag.removeChild(svg.grid.group);
			// createGrid();
			// // console.log("Zoom in");
			// }
			// }, false);
			/* 
			--------------------------------------------------
			Enable Zoom
			--------------------------------------------------
			*/

		}

		function handleDrag() {
			var selectedElement = null;
			var userPoint = svg.tag.createSVGPoint();
			var svgPoint = null;

			svg.tag.addEventListener("mousedown", function (event) {
				selectedElement = event.target;
			}, false);

			svg.tag.addEventListener("mousemove", function (event) {
				userPoint.x = event.clientX;
				userPoint.y = event.clientY;
				svgPoint = userPoint.matrixTransform(svg.tag.getScreenCTM().inverse());
				console.log(svgPoint);
				// if (selectedElement && selectedElement.classList.contains("draggable")) {
				// selectedElement.setAttribute("cx", Math.round(svgPoint.x / 10) * 10);
				// selectedElement.setAttribute("cx", Math.round(svgPoint.x / 5) * 5);
				// selectedElement.setAttribute("cx", svgPoint.x);
				// selectedElement.setAttribute("cy", Math.round(svgPoint.y / 10) * 10);
				// selectedElement.setAttribute("cy", Math.round(svgPoint.y / 5) * 5);
				// selectedElement.setAttribute("cy", svgPoint.y);
				// }
				// if (selectedElement === startPoint) {
				// line.setAttribute("x1", selectedElement.getAttribute("cx"));
				// line.setAttribute("y1", selectedElement.getAttribute("cy"));
				// }
				// if (selectedElement === endPoint) {
				// line.setAttribute("x2", selectedElement.getAttribute("cx"));
				// line.setAttribute("y2", selectedElement.getAttribute("cy"));
				// }
			}, false);

			svg.tag.addEventListener("mouseup", function (event) {
				selectedElement = null;
			}, false);
		}

		function drawVector(activeView, vectorCommand) {

			function drawPoints(event, targetGroup, showPoint) {
				var userPoint = svg.tag.createSVGPoint();
				userPoint.x = event.clientX;
				userPoint.y = event.clientY;

				var svgPoint = userPoint.matrixTransform(svg.tag.getScreenCTM().inverse());
				var pointIndicator = document.createElementNS(nameSpace, "circle");
				pointIndicator.classList.add("point-indicator");

				/* If Snap to Grid is off */

				// pointIndicator.setAttribute("cx", Math.round(svgPoint.x));
				// pointIndicator.setAttribute("cy", Math.round(svgPoint.y));

				/* If Snap to Grid is on */

				pointIndicator.setAttribute("cx", Math.round(svgPoint.x / 5) * 5);
				pointIndicator.setAttribute("cy", Math.round(svgPoint.y / 5) * 5);

				pointIndicator.setAttribute("r", "5");

				if (showPoint) {
					targetGroup.appendChild(pointIndicator);
					svg.graphic.group.appendChild(targetGroup);
				}
				return pointIndicator;
			}

			function drawLine(points, targetGroup) {
				var lineElement = document.createElementNS(nameSpace, "line");
				lineElement.setAttribute("x1", points[0].getAttribute("cx"));
				lineElement.setAttribute("y1", points[0].getAttribute("cy"));
				lineElement.setAttribute("x2", points[1].getAttribute("cx"));
				lineElement.setAttribute("y2", points[1].getAttribute("cy"));

				targetGroup.appendChild(lineElement);
				svg.graphic.designGroup.appendChild(targetGroup);

				vectorCommand.parentElement.classList.remove("hidden");
				vectorCommand.parentElement.nextElementSibling.classList.add("hidden");
			}

			function drawCircle(points, targetGroup) {
				var circleElement = document.createElementNS(nameSpace, "circle");
				circleElement.setAttribute("cx", points[0].getAttribute("cx"));
				circleElement.setAttribute("cy", points[0].getAttribute("cy"));
				var radius = Math.sqrt(Math.pow((points[1].getAttribute("cx") - points[0].getAttribute("cx")), 2) + Math.pow((points[1].getAttribute("cy") - points[0].getAttribute("cy")), 2))
				circleElement.setAttribute("r", radius);

				targetGroup.appendChild(circleElement.cloneNode(true));
				svg.graphic.designGroup.appendChild(targetGroup);

				vectorCommand.parentElement.classList.remove("hidden");
				vectorCommand.parentElement.nextElementSibling.classList.add("hidden");
			}

			function drawPolygon(points, targetGroup) {
				if (points.length > 2) {
					var polygonElement = document.createElementNS(nameSpace, "polygon");
					var polygonPointsString = "";
					for (let i = 0; i < points.length; i += 1) {
						if (i === 0) {
							polygonPointsString = points[i].getAttribute("cx") + ", " + points[i].getAttribute("cy");
						} else {
							polygonPointsString += ", " + points[i].getAttribute("cx") + ", " + points[i].getAttribute("cy");
						}
					}
					polygonElement.setAttribute("points", "");
					polygonElement.setAttribute("points", polygonPointsString);

					targetGroup.appendChild(polygonElement.cloneNode(true));
					svg.graphic.designGroup.appendChild(targetGroup);

					vectorCommand.parentElement.classList.remove("hidden");
					vectorCommand.parentElement.nextElementSibling.classList.add("hidden");
				}
			}

			function removeClickEvent(node) {
				node.removeEventListener("click", clickEvent, false);
			}

			var switchCase = {
					"line": function () {

						var points = [];
						var clickCount = 0;
						svg.graphic.newgraphicGroup = document.createElementNS(nameSpace, "g");
						svg.graphic.newgraphicGroup.classList.add("graphic-line");
						svg.graphic.newgraphicGroup.classList.add("draw-in-progress");

						fixedConsole.textContent = "Specify first point";
						clickEvent = function (event) {
							clickCount += 1;
							if (clickCount === 2) {
								points[1] = drawPoints(event, svg.graphic.newgraphicGroup, true);
								drawLine(points, svg.graphic.newgraphicGroup);
								svg.graphic.newgraphicGroup.classList.remove("draw-in-progress");
								fixedConsole.textContent = "Line drawn";
								removeClickEvent(svg.tag);
							} else {
								points[0] = drawPoints(event, svg.graphic.newgraphicGroup, true);
								fixedConsole.textContent = "Specify second point";
							}
						};
						svg.tag.addEventListener("click", clickEvent, false);
						// document.querySelectorAll("div.cancel-command")[0].addEventListener("click", function() {
						// svg.tag.removeEventListener("click", clickEvent, false);
						// }, false);
					},
					"polygon": function () {
						var points = [];
						var clickCount = 0;
						svg.graphic.newgraphicGroup = document.createElementNS(nameSpace, "g");
						svg.graphic.newgraphicGroup.classList.add("graphic-line");
						svg.graphic.newgraphicGroup.classList.add("draw-in-progress");
						var polygonElement = document.createElementNS(nameSpace, "polygon");

						fixedConsole.innerHTML = "Specify Polygon Points<br />Press Enter to complete the operation";
						clickEvent = function (event) {
							points[clickCount] = drawPoints(event, svg.graphic.newgraphicGroup, true);
							clickCount += 1;
						};
						svg.tag.addEventListener("click", clickEvent, false);

						window.addEventListener("keydown", function (event) {
							if (event.keyCode === 13) {
								drawPolygon(points, svg.graphic.newgraphicGroup);
								fixedConsole.textContent = "Polygon Drawn";
								removeClickEvent(svg.tag);
							}
						}, false);
					},
					"circle": function () {
						var points = [];
						var clickCount = 0;
						svg.graphic.newgraphicGroup = document.createElementNS(nameSpace, "g");
						svg.graphic.newgraphicGroup.classList.add("graphic-circle");
						svg.graphic.newgraphicGroup.classList.add("draw-in-progress");

						function removeClickEvent(node) {
							node.removeEventListener("click", clickEvent, false);
						}

						fixedConsole.textContent = "Specify the center";
						clickEvent = function (event) {
							clickCount += 1;
							if (clickCount === 2) {
								points[1] = drawPoints(event, svg.graphic.newgraphicGroup, false);
								drawCircle(points, svg.graphic.newgraphicGroup);
								svg.graphic.newgraphicGroup.classList.remove("draw-in-progress");
								fixedConsole.textContent = "Circle drawn";
								removeClickEvent(svg.tag);
							} else {
								points[0] = drawPoints(event, svg.graphic.newgraphicGroup, true);
								fixedConsole.textContent = "Specify the distance from the center";
							}
						};
						svg.tag.addEventListener("click", clickEvent, false);
					},
					"move": function () {
						alert("Move module not yet created");
					},
					"erase": function () {
						alert("Erase module not yet created");
					},
					"default": function () {
						console.log("The module does not exist");
					}
				},
				defCase;

			if (switchCase[activeView]) {
				defCase = switchCase[activeView];
			} else {
				defCase = switchCase["default"];
			}
			return defCase();
		}

		(function eventListeners() {
			var lineCommand = document.querySelectorAll("div.command-button")[0];
			var polygonCommand = document.querySelectorAll("div.command-button")[1];
			var circleCommand = document.querySelectorAll("div.command-button")[2];
			var exportCommand = document.querySelectorAll("div.command-button")[3];
			// var moveCommand = document.querySelectorAll("div.command-button")[3];
			// var eraseCommand = document.querySelectorAll("div.command-button")[4];

			var cancelCommand = document.querySelectorAll("div.cancel-command")[0];

			lineCommand.addEventListener("click", function () {
				lineCommand.parentElement.classList.add("hidden");
				lineCommand.parentElement.nextElementSibling.classList.remove("hidden");
				activeView = "line";
				drawVector(activeView, lineCommand);
			}, false);

			polygonCommand.addEventListener("click", function () {
				polygonCommand.parentElement.classList.add("hidden");
				polygonCommand.parentElement.nextElementSibling.classList.remove("hidden");
				activeView = "polygon";
				drawVector(activeView, polygonCommand);
			}, false);

			circleCommand.addEventListener("click", function () {
				circleCommand.parentElement.classList.add("hidden");
				circleCommand.parentElement.nextElementSibling.classList.remove("hidden");
				activeView = "circle";
				drawVector(activeView, circleCommand);
			}, false);

			exportCommand.addEventListener("click", function () {
				exportCommand.parentElement.classList.add("hidden");
				exportCommand.parentElement.nextElementSibling.classList.remove("hidden");
				activeView = "export";

				svg.graphic.designGroup.classList.add("hidden");
				svg.graphic.exportGroup.classList.remove("hidden");

				var drawnElements = []
				var designedNodes = svg.tag.querySelectorAll("g.graphic-design g *");
				for (let i = 0; i < designedNodes.length; i += 1) {
					if (!designedNodes[i].classList.contains("point-indicator")) {
						drawnElements[i] = designedNodes[i].cloneNode(true);
					}
				}
				drawnElements = drawnElements.filter(function (elements) {
					return elements !== undefined;
				});

				// svg.graphic.group.removeChild(svg.graphic.exportGroup);
				// svg.graphic.exportGroup = svg.graphic.exportGroup.cloneNode(true);
				console.log(svg.graphic.exportGroup);
				for (let i = 0; i < drawnElements.length; i += 1) {
					svg.graphic.exportGroup.appendChild(drawnElements[i]);
				}
				svg.graphic.group.appendChild(svg.graphic.exportGroup);
			}, false);

			/*
			// moveCommand.addEventListener("click", function () {
			// activeView = "move";
			// drawVector(activeView);
			// }, false);
			*/

			/*
			// eraseCommand.addEventListener("click", function () {
			// activeView = "erase";
			// drawVector(activeView);
			// }, false);
			*/

			cancelCommand.addEventListener("click", function () {
				cancelCommand.parentElement.classList.add("hidden");
				cancelCommand.parentElement.previousElementSibling.classList.remove("hidden");

				if (activeView === "export") {
					svg.graphic.designGroup.classList.remove("hidden");
					svg.graphic.exportGroup.classList.add("hidden");
				} else {
					activeView = "default";

					if (svg.graphic.group.querySelectorAll("g.draw-in-progress").length !== 0) {
						svg.graphic.group.removeChild(svg.graphic.group.querySelectorAll("g.draw-in-progress")[0]);
					}

					svg.tag.removeEventListener("click", clickEvent, false);
					fixedConsole.textContent = "";
				}
			}, false);

			svg.tag.addEventListener("click", function (event) {
				console.log(event.target.parentElement);
			}, false);

			svg.tag.addEventListener("contextmenu", function (event) {
				event.preventDefault();
			}, false);

		})();

		svg.graphic.group.appendChild(svg.graphic.designGroup);
		svg.graphic.group.appendChild(svg.graphic.exportGroup);
		svg.tag.appendChild(svg.graphic.group);

		(function cadDrafter() {

			var userPoint = null;
			var svgPoint = null;

			svg.drafter = {};
			svg.drafter.group = document.createElementNS(nameSpace, "g");
			svg.drafter.group.setAttribute("id", "drafter-group");

			svg.drafter.horizontal = document.createElementNS(nameSpace, "line");
			svg.drafter.horizontal.setAttribute("id", "horizontal-drafter");
			svg.drafter.vertical = document.createElementNS(nameSpace, "line");
			svg.drafter.vertical.setAttribute("id", "vertical-drafter");

			svg.drafter.group.appendChild(svg.drafter.horizontal);
			svg.drafter.group.appendChild(svg.drafter.vertical);
			svg.tag.insertBefore(svg.drafter.group, svg.graphic.group);

			userPoint = svg.tag.createSVGPoint();
			svg.tag.addEventListener("mousemove", function (event) {

				userPoint.x = event.clientX;
				userPoint.y = event.clientY;

				svgPoint = userPoint.matrixTransform(svg.tag.getScreenCTM().inverse());

				/* Full canvas Drafter */

				// svg.drafter.horizontal.setAttribute("x1", svg.viewBox.startX);
				// // svg.drafter.horizontal.setAttribute("y1", Math.round(svgPoint.y));
				// svg.drafter.horizontal.setAttribute("y1", Math.round(svgPoint.y / 5) * 5);
				// svg.drafter.horizontal.setAttribute("x2", svg.viewBox.endX);
				// // svg.drafter.horizontal.setAttribute("y2", Math.round(svgPoint.y));
				// svg.drafter.horizontal.setAttribute("y2", Math.round(svgPoint.y / 5) * 5);

				// // svg.drafter.vertical.setAttribute("x1", Math.round(svgPoint.x));
				// svg.drafter.vertical.setAttribute("x1", Math.round(svgPoint.x / 5) * 5);
				// svg.drafter.vertical.setAttribute("y1", svg.viewBox.startY);
				// // svg.drafter.vertical.setAttribute("x2", Math.round(svgPoint.x));
				// svg.drafter.vertical.setAttribute("x2", Math.round(svgPoint.x / 5) * 5);
				// svg.drafter.vertical.setAttribute("y2", svg.viewBox.endY);

				/* Clipped Drafter */

				svg.drafter.horizontal.setAttribute("x1", svgPoint.x - 30);
				svg.drafter.horizontal.setAttribute("y1", Math.round(svgPoint.y));
				// svg.drafter.horizontal.setAttribute("y1", Math.round(svgPoint.y / 5) * 5);
				svg.drafter.horizontal.setAttribute("x2", svgPoint.x + 30);
				svg.drafter.horizontal.setAttribute("y2", Math.round(svgPoint.y));
				// svg.drafter.horizontal.setAttribute("y2", Math.round(svgPoint.y / 5) * 5);

				svg.drafter.vertical.setAttribute("x1", Math.round(svgPoint.x));
				// svg.drafter.vertical.setAttribute("x1", Math.round(svgPoint.x / 5) * 5);
				svg.drafter.vertical.setAttribute("y1", svgPoint.y - 30);
				svg.drafter.vertical.setAttribute("x2", Math.round(svgPoint.x));
				// svg.drafter.vertical.setAttribute("x2", Math.round(svgPoint.x / 5) * 5);
				svg.drafter.vertical.setAttribute("y2", svgPoint.y + 30);

			}, false);

			svg.tag.addEventListener("mouseleave", function () {
				svg.drafter.horizontal.removeAttribute("x1");
				svg.drafter.horizontal.removeAttribute("y1");
				svg.drafter.horizontal.removeAttribute("x2");
				svg.drafter.horizontal.removeAttribute("y2");
				svg.drafter.vertical.removeAttribute("x1");
				svg.drafter.vertical.removeAttribute("y1");
				svg.drafter.vertical.removeAttribute("x2");
				svg.drafter.vertical.removeAttribute("y2");
			}, false);

		})();

		function createGrid() {

			svg.grid = {};

			svg.grid.group = document.createElementNS(nameSpace, "g");
			svg.grid.group.classList.add("grid-group");

			svg.grid.vertical = document.createElementNS(nameSpace, "g");
			svg.grid.vertical.setAttribute("id", "vertical-grid-lines");

			svg.grid.horizontal = document.createElementNS(nameSpace, "g");
			svg.grid.horizontal.setAttribute("id", "horizontal-grid-lines");

			svg.grid.lineClone = document.createElementNS(nameSpace, "line");

			for (let i = svg.viewBox.startX; i <= svg.viewBox.endX; i += 10) {
				svg.grid.line = svg.grid.lineClone.cloneNode(true);
				svg.grid.line.setAttribute("x1", i);
				svg.grid.line.setAttribute("y1", svg.viewBox.startY);
				svg.grid.line.setAttribute("x2", i);
				svg.grid.line.setAttribute("y2", svg.viewBox.endY);
				if (i !== svg.viewBox.startX && i !== svg.viewBox.endX && i % 50 === 0) {
					svg.grid.line.classList.add("grid-fifty");
				}
				svg.grid.vertical.appendChild(svg.grid.line);
			}

			for (let i = svg.viewBox.startY; i <= svg.viewBox.endY; i += 10) {
				svg.grid.line = svg.grid.lineClone.cloneNode(true);
				svg.grid.line.setAttribute("x1", svg.viewBox.startX);
				svg.grid.line.setAttribute("y1", i);
				svg.grid.line.setAttribute("x2", svg.viewBox.endX);
				svg.grid.line.setAttribute("y2", i);
				if (i !== svg.viewBox.startY && i !== svg.viewBox.endY && i % 50 === 0) {
					svg.grid.line.classList.add("grid-fifty");
				}
				svg.grid.horizontal.appendChild(svg.grid.line);
			}

			svg.grid.group.appendChild(svg.grid.vertical);
			svg.grid.group.appendChild(svg.grid.horizontal);
			svg.tag.insertBefore(svg.grid.group, svg.drafter.group);

		}
		createGrid();

	})();
}, false);
