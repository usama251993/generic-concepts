/*
    This Script is used to run Select Boxes in the page
    The structure of the select box must be strictly as follows :
    
            <!-- Outermost Selectbox container -->
    +-----  <div class="select-box">
    |
    |           <!-- To close select box when clicked outside -->
    |+----      <div class="select-shadow hidden"></div>
    |        
    |           <!-- Display Selected Option -->
    |+----      <div class="selected-option">
    ||       
    ||              <!-- Populated by JavaScript -->
    ||+---          <span></span>
    |+----      </div> <!-- <div class="selected-option"> ends -->
    |        
    |           <!-- To hold all the options -->
    |+----      <div class="options-holder hidden">
    ||        
    ||              <!-- Option -->
    ||+---          <div class="option">
    |||+--              <span>Option 1</span>
    ||||                
    ||||                <!-- If option length exceeds select box length -->
    ||||                <!-- Tooltip is displayed -->
    ||||                <div class="option-tooltip hidden">
    ||||                    <!-- Populated by JavaScript -->
    ||||+-                  <span></span>
    |||+--              </div> <!-- <div class="option-tooltip"> ends -->
    ||+---          </div> <!-- <div class="option"> ends -->
    |+----      </div> <!-- <div class="options-holder hidden"> ends -->
    |        
    |           <!-- Arrow for select dropdown -->
    |+----      <div class="select-arrow">
    ||+---          <span class="hidden"><i class="fa fa-angle-up"></i></span>
    ||+---          <span class="visible"><i class="fa fa-angle-down"></i></span>
    |+----      </div> <~-- <div class="select-arrow"> ends -->
    +----- </div> <!-- <div class="select-box"> ends -->
    
*/

// This function is invoked when one select box is being executed
function selectElement() {
    "use strict";

    function selectReady(element) {

        // Select object
        var select = {
                shadow: element.querySelectorAll("div.select-shadow")[0],
                currentOption: element.querySelectorAll("div.selected-option")[0],
                arrow: element.querySelectorAll("div.select-arrow")[0],
                optionsHolder: element.querySelectorAll("div.options-holder")[0],
                options: element.querySelectorAll("div.option"),
                optionTooltip: element.querySelectorAll("div.option-tooltip"),
                status: false
            },

            // Stylesheet
            selectStyleSheet,

            // Animation rule array
            type7Rules = [],

            // For Event Listeners
            optionClick,
            optionMouseEnter,
            optionMouseLeave,
            optionAnimationEnd,

            // Iterator variables
            i,
            j;

        // To interact with Select.css
        selectStyleSheet = document.querySelectorAll("link[href*=\"Select.css\"]")[0].sheet;

        for (i = 0; i < selectStyleSheet.cssRules.length; i += 1) {
            if (selectStyleSheet.cssRules[i].type === 7) {

                // Get animation rules from stylesheet
                type7Rules[i] = selectStyleSheet.cssRules[i];
            }
        }

        // Remove the empty elements from the array
        type7Rules = type7Rules.filter(function (elements) {
            return elements !== undefined;
        });

        // Populate the Tooltip content
        for (i = 0; i < select.optionTooltip.length; i += 1) {
            select.optionTooltip[i].children[0].textContent = select.options[i].children[0].textContent;
        }

        // Function to either show or hide the selectbox options
        // Also to toggle the direction of select arrow
        function toggleOptions() {
            if (!select.status) {
                select.optionsHolder.classList.remove("hidden");
                select.shadow.classList.remove("hidden");
                select.arrow.children[0].classList.remove("hidden");
                select.arrow.children[1].classList.add("hidden");
            } else {
                select.optionsHolder.classList.add("hidden");
                select.shadow.classList.add("hidden");
                select.arrow.children[0].classList.add("hidden");
                select.arrow.children[1].classList.remove("hidden");
            }
            select.status = !select.status;
        }

        // This function is invoked when an option is clicked
        function selectOption(element) {

            // Change the text content of the selected option
            // Populate it with the value of the option user has clicked
            select.currentOption.children[0].textContent = element.children[0].textContent;

            // Hide options when done
            toggleOptions();
        }

        optionClick = function () {

            // Invoke the above function
            selectOption(this);

            // Remove the tooltip
            this.children[0].nextElementSibling.classList.remove("animate-fade-in");
            this.children[0].nextElementSibling.classList.add("hidden");
        };

        optionMouseEnter = function () {

            // If contents of the option are exceed the selectbox width
            if (this.children[0].offsetWidth < this.children[0].scrollWidth) {

                // Display the tooltip
                this.children[0].nextElementSibling.classList.add("animate-fade-in");
                this.children[0].nextElementSibling.classList.remove("hidden", "animate-fade-out");
            }
        };

        optionMouseLeave = function () {

            // Hide the tooltip
            if (this.children[0].offsetWidth < this.children[0].scrollWidth) {
                this.children[0].nextElementSibling.classList.add("animate-fade-out");
                this.children[0].nextElementSibling.classList.remove("animate-fade-in");
            }
        };

        optionAnimationEnd = function (event) {
            if (event.animationName === type7Rules[1].name) {
                this.children[0].nextElementSibling.classList.remove("animate-fade-out");
                this.children[0].nextElementSibling.classList.add("hidden");
            }
        };

        // Event Listeners for options    
        for (i = 0; i < select.options.length; i += 1) {

            // When the select option is clicked
            select.options[i].onclick = optionClick;

            // When the mouse pointer hovers over an option
            select.options[i].addEventListener("mouseenter", optionMouseEnter, false);

            // When the mouse pointer leaves an option
            select.options[i].addEventListener("mouseleave", optionMouseLeave, false);

            // For smooth fade out effect
            select.options[i].addEventListener("animationend", optionAnimationEnd, false);
        }

        // Event Listeners for select box
        // When the select box is clicked    
        select.currentOption.addEventListener("click", function () {
            toggleOptions();
        }, false);

        // When select arrow is clicked
        select.arrow.addEventListener("click", function () {
            toggleOptions();
        }, false);

        // When clicked outside the options
        select.shadow.addEventListener("click", function () {
            select.status = true;
            toggleOptions();
        }, false);

        window.addEventListener("keydown", function (event) {

            // When Esc key is pressed
            if (event.keyCode === 27) {
                if (select.status === true) {

                    // Hide the options
                    toggleOptions();
                }
            }
        }, false);
    }

    // This section is used to detect which select box must be triggered
    var
        selectBoxes = document.querySelectorAll("div.select-box"),
        activeSelect,
        i;

    activeSelect = function () {
        selectReady(this);
    };

    for (i = 0; i < selectBoxes.length; i += 1) {
        selectBoxes[i].querySelectorAll("div.selected-option span")[0].textContent = selectBoxes[i].querySelectorAll("div.option")[0].children[0].textContent;
        selectBoxes[i].addEventListener("mouseenter", activeSelect, false);
    }
}
