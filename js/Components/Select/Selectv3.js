function selectElement() {
  "use strict";

  (function renderSelectBox() {
    console.log("Rendering Elements");
    let selectObject = {
      boxes: []
    };

    let nameSpaceURI = "http://www.w3.org/2000/svg";
    let svgTagArray = ["circle", "ellipse", "g", "line", "path", "pattern", "polygon", "polyline", "rect", "text"];
    let semanticTags = {
      divTag: window.document.createElement("div"),
      spanTag: window.document.createElement("span"),
      objectTag: window.document.createElement("object"),
      svg: {
        tag: window.document.createElementNS(nameSpaceURI, "svg"),
      }
    };
    let markupTags = [];

    window.document.querySelectorAll("select-v3").forEach((element) => {
      let tempObject = {};
      Object.defineProperties(tempObject, {
        "element": {
          value: element,
          writable: true
        },
        "options": {
          value: [],
          writable: true
        }
      })
      element.querySelectorAll("*").forEach((option) => {
        tempObject.options.push(option);
      })
      selectObject.boxes.push(tempObject);
    });

    svgTagArray.forEach((svgTag) => {
      semanticTags.svg[svgTag] = window.document.createElementNS(nameSpaceURI, svgTag);
    });

    Object.freeze(semanticTags);

    if (selectObject.hasOwnProperty("boxes")) {
      let tempObject = {};

      function createBaseMarkup(tempObject, markupTags, selectBox) {
        Object.defineProperties(tempObject, {

          // <div class="select-box"></div>
          "boxDIV": {
            value: semanticTags.divTag.cloneNode(false),
            writable: true
          },

          "parametersDIV": {
            value: semanticTags.divTag.cloneNode(false),
            writable: true
          },

          // <div class="select-shadow hidden"></div>
          "shadowDIV": {
            value: semanticTags.divTag.cloneNode(false),
            writable: true
          },

          // <div class="selected-option">
          "selectedOptionDIV": {
            value: semanticTags.divTag.cloneNode(false),
            writable: true
          },

          // <div class="options-holder hidden"></div>
          "optionsHolderDIV": {
            value: semanticTags.divTag.cloneNode(false),
            writable: true
          },

          // <div class="select-arrow"></div>
          "arrowDIV": {
            value: semanticTags.divTag.cloneNode(false),
            writable: true
          },

          // <span class="placeholder-span"></span>
          "placeholderSPAN": {
            value: semanticTags.spanTag.cloneNode(false),
            writable: true
          }
        });

        tempObject.boxDIV.classList.add("select-box");
        tempObject.parametersDIV.classList.add("select-parameters");
        tempObject.shadowDIV.classList.add("select-shadow", "hidden");
        tempObject.selectedOptionDIV.classList.add("selected-option");
        tempObject.optionsHolderDIV.classList.add("options-holder", "hidden");
        tempObject.arrowDIV.classList.add("select-arrow");
        tempObject.placeholderSPAN.classList.add("placeholder-span");
      }

      function createToggleIcon(tempObject, markupTags, selectBox) {

        // SVG Arrow
        Object.defineProperties(tempObject, {

          // <object></object>
          "arrowObject": {
            value: semanticTags.objectTag.cloneNode(false),
            writable: true
          },

          // <svg xmlns=""></svg>
          "arrowParentSVG": {
            value: semanticTags.svg.tag.cloneNode(false),
            writable: true
          },

          // <svg xmlns=""></svg>
          "arrowChildSVG": {
            value: semanticTags.svg.tag.cloneNode(false),
            writable: true
          },

          // <g class="arrow-group"></g>
          "arrowGroup": {
            value: semanticTags.svg.g.cloneNode(false),
            writable: true
          },

          // <line <!--attributes here-->></line>
          "arrowLines": {
            value: [],
            writable: true
          },

          "lineComponent": {
            value: semanticTags.svg.line.cloneNode(false),
            writable: true
          },

          "polygonComponent": {
            value: semanticTags.svg.polygon.cloneNode(false),
            writable: true
          }

        });

        tempObject.arrowParentSVG.setAttribute("xmlns", nameSpaceURI);
        tempObject.arrowChildSVG.setAttribute("preserveAspectRatio", "xMidYMid meet");
        tempObject.arrowChildSVG.setAttribute("viewBox", "0 0 300 150");
        tempObject.arrowGroup.classList.add("arrow-group");

        let polygonPoints = [0, 0, 150, 100, 300, 0, 300, 50, 150, 150, 0, 50, 0, 0];

        tempObject.polygonComponent.setAttribute("points", polygonPoints.join(", "));
        tempObject.arrowGroup.appendChild(tempObject.polygonComponent.cloneNode(false));

        tempObject.arrowChildSVG.appendChild(tempObject.arrowGroup);
        tempObject.arrowParentSVG.appendChild(tempObject.arrowChildSVG);
        tempObject.arrowDIV.appendChild(tempObject.arrowParentSVG);

      }

      function populateOptions(tempObject, markupTags, selectBox) {
        selectBox.options.forEach((option) => {

          // <div class="option"></div>
          tempObject.optionDIV = semanticTags.divTag.cloneNode(false);
          tempObject.optionDIV.classList.add("option");

          // <span><!--Content here--></span>
          tempObject.optionSPAN = semanticTags.spanTag.cloneNode(false);
          tempObject.optionSPAN.textContent = option.textContent;

          // Markup Updated
          tempObject.optionDIV.appendChild(tempObject.optionSPAN);

          // Markup Updated
          tempObject.optionsHolderDIV.appendChild(tempObject.optionDIV.cloneNode(true));
        });

        tempObject.placeholderSPAN.textContent = selectBox.options[0].textContent;

      }

      function updateBaseMarkup(tempObject, markupTags, selectBox) {
        // Base Markup updated
        // div.select-box
        // ├ div.select-parameters
        // | ├ div.select-shadow
        // | ├ div.selected-option
        // | | └ span.placeholder-span
        // | └  div.select-arrow
        // |    └ svg
        // ├ div.options-holder.hidden
        // | ├ div.option
        // | | └ span <!--Content-->
        // | ├ div.option
        // | | └ span <!--Content-->
        // | :
        // └ <!--continues for each option-->

        tempObject.selectedOptionDIV.appendChild(tempObject.placeholderSPAN);
        tempObject.parametersDIV.appendChild(tempObject.shadowDIV);
        tempObject.parametersDIV.appendChild(tempObject.selectedOptionDIV);
        tempObject.parametersDIV.appendChild(tempObject.arrowDIV);


        tempObject.boxDIV.appendChild(tempObject.parametersDIV);
        tempObject.boxDIV.appendChild(tempObject.optionsHolderDIV);

        markupTags.push(tempObject.boxDIV);
      }

      selectObject.boxes.forEach((selectBox) => {
        createBaseMarkup(tempObject, markupTags, selectBox);
        createToggleIcon(tempObject, markupTags, selectBox);
        populateOptions(tempObject, markupTags, selectBox);
        updateBaseMarkup(tempObject, markupTags, selectBox);
        selectBox.element.parentElement.replaceChild(markupTags[markupTags.length - 1], selectBox.element);
      });

      markupTags.forEach((selectBox) => {
        let selectObject = {};
        Object.defineProperties(selectObject, {
          "box": {
            value: selectBox,
            writable: true
          },
          "shadow": {
            value: selectBox.querySelectorAll("div.select-shadow")[0],
            writable: true
          },
          "selected": {
            value: selectBox.querySelectorAll("div.selected-option")[0],
            writable: true
          },
          "arrow": {
            value: selectBox.querySelectorAll("div.select-arrow")[0],
            writable: true
          },
          "holder": {
            value: selectBox.querySelectorAll("div.options-holder")[0],
            writable: true
          },
          "options": {
            value: [],
            writable: true
          }
        });
        selectBox.querySelectorAll("div.option").forEach((option) => {
          selectObject.options.push(option);
        });
        selectBehaviour(selectObject);
      })
    }

  })();

  function selectBehaviour(selectObject) {

    function toggleSelectBox(isOpen, event) {
      let animationObject = {};
      let animDuration = 250;
      if (isOpen) {

        selectObject.arrow.children[0].animate([
          { transform: "rotate(180deg)", offset: 0 },
          { transform: "rotate(0deg)", offset: 1 }
        ], {
            duration: (selectObject.options.length > 12) ? animDuration * selectObject.options.length : animDuration * 12,
            fill: "forwards"
          }
        );

        selectObject.options.forEach((option, index) => {

          animationObject = option.animate([
            { transform: "translateY(0%)", offset: 0 },
            { transform: "translateY(-" + (index) * 100 + "%)", offset: 1 }
          ], {
              duration: (selectObject.options.length > 12) ? animDuration * index : animDuration * 12,
              fill: "forwards"
            });
        });

        animationObject.addEventListener("finish", (event) => {
          selectObject.holder.classList.add("hidden");
          selectObject.shadow.classList.add("hidden");
        }, false);

        isOpen = !isOpen;

      } else {

        selectObject.holder.classList.remove("hidden");
        selectObject.shadow.classList.remove("hidden");

        selectObject.arrow.children[0].animate([
          { transform: "rotate(0deg)", offset: 0 },
          { transform: "rotate(180deg)", offset: 1 }
        ], {
            duration: (selectObject.options.length > 12) ? animDuration * selectObject.options.length : animDuration * 12,
            fill: "forwards"
          }
        );

        selectObject.options.forEach((option, index) => {
          animationObject = option.animate([
            { transform: "translateY(-" + (index) * 100 + "%)", offset: 0 },
            { transform: "translateY(0%)", offset: 1 }
          ], {
              duration: (selectObject.options.length > 12) ? animDuration * index : animDuration * 12,
              fill: "forwards"
            }
          );
        });

        animationObject.addEventListener("finish", (event) => {
        }, false);

      }
      event.stopPropagation();
    }

    selectObject.box.addEventListener("click", (event) => {
      let isOpen = (selectObject.holder.classList.contains("hidden")) ? false : true;
      toggleSelectBox(isOpen, event);
    }, false);

    selectObject.shadow.addEventListener("click", (event) => {
      toggleSelectBox(true, event);
    }, false);

    function clickOption(option) {
      option.addEventListener("click", (event) => {
        selectObject.selected.textContent = option.textContent;
        toggleSelectBox(true, event);
      }, false)
    }

    selectObject.options.forEach((option) => {
      clickOption(option);
    });

  }

}
