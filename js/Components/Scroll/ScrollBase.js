function scrollFunction() {
    "use strict";
    var
        topOffset,
        leftOffset,
        timeoutVar;

    /**
     * This function is used to perform smooth scrolling
     * 
     * @param element 
     *      destination element till where the page must be scrolled
     * 
     * @param timeFactor
     *      0.01 for fastest scroll effect
     *      0.99 for slowest scroll effect
     */

    function scrollToDiv(element, timeFactor) {
        clearInterval(timeoutVar);
        topOffset = self.pageYOffset; // Get scroll offset from top of viewport
        leftOffset = self.pageXOffset; // Get scroll offset from left of viewport

        // If it is required to scroll to the top of the page
        if (element === "top") {
            element = 0;
        }

        // If it is required to scroll to the bottom of the page
        else if (element === "bottom") {
            element = document.body.scrollHeight;
        }

        // If it is required to scroll to specific element
        else {
            element = element.offsetTop;
        }

        // Check the scrolling condition and perform scroll
        function checkScroll() {
            if (Math.ceil(topOffset) === element) { // Stop scrolling when reached at the destination top offset
                clearInterval(timeoutVar);
            } else { 
                
                // Increase top offset of according to the timeFactor
                topOffset = topOffset * timeFactor + element * (1 - timeFactor);
                
                // Scroll to the newly generated value of topOffset
                window.scrollTo(leftOffset, Math.ceil(topOffset) + 1);
            }
        }

        // Call the checkScroll function repeatedly in interval of 1ms for smooth effect
        timeoutVar = window.setInterval(function () {
            checkScroll();
        }, 1);
    }

    // Stop scrolling when mouse wheel is rotated
    window.addEventListener("wheel", function () {
        clearInterval(timeoutVar);
    }, false);

    /**
     * Declare the Event Listeners here
     * element.addEventListener {scrollToDiv(|element to be scrolled to|, |timeFactor|);}
    */
    
    // --------------- Event Listeners : starts ---------------
    
    var
        linkElements = document.querySelectorAll(".scroller-element"),
        effectElements = document.querySelectorAll(".effect-element"),
        topNavigation = document.querySelectorAll("div.navigate-top")[0],
        i,
        j;

    topNavigation.addEventListener("click", function () {
        scrollToDiv("top", 0.975, "!important");
    }, false);

    function effectiveDiv() {
        for (j = 0; j < linkElements.length; j += 1) {
            if (linkElements[j] === this) {
                scrollToDiv(effectElements[j], 0.975);
            }
        }
    }

    for (i = 0; i < linkElements.length; i += 1) {
        linkElements[i].addEventListener("click", effectiveDiv, false);
    }
    
    // --------------- Event Listeners : ends ---------------
}
