function navbarChange(event) {
    "use strict";
    var navbarElement = document.querySelectorAll("div[class$=-nav]")[0],
        topOffsetHeight = parseInt(window.getComputedStyle(navbarElement, null).height) * 0.6 + navbarElement.previousElementSibling.getBoundingClientRect().height,
        topNavigation = document.querySelectorAll("div.navigate-top")[0];

    if (self.pageYOffset <= 2) {
        topNavigation.classList.add("hidden");
    } else {
        topNavigation.classList.remove("hidden");
    }

    if (self.pageYOffset <= topOffsetHeight) {
        navbarElement.classList.remove("fixed-nav");
        navbarElement.classList.add("relative-nav");
    } else {
        navbarElement.classList.remove("relative-nav");
        navbarElement.classList.add("fixed-nav");
    }
}
