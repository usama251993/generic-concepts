function contextReady() {
    "use strict";

    var
        contextElement = document.querySelectorAll(".context-menu-holder")[0],
        contextMenuElements = document.querySelectorAll(".context-menu-item"),
        preventDefaultMenu,
        i;

    function displayContextMenu() {

        // Make the context menu visible
        contextElement.classList.remove("hidden");
        contextElement.classList.add("visible");

        // Context menu must move along with scrolling of the page
        contextElement.style.position = "absolute";

        // --------------- This section is used to avoid the context menu to display out of viewport ---------------

        // Condition 1
        // When pointer is on the bottom right end of the screen
        if (
            // Pointer is towards the bottom of the screen
            ((event.clientY + contextElement.offsetHeight) >= (window.innerHeight)) &&
            // Pointer is towards the right end of the screen
            ((event.clientX + contextElement.offsetWidth) >= (window.innerWidth - 25))
        ) {
            // Context menu must open on the right and above the mouse pointer
            contextElement.style.top = event.clientY - contextElement.offsetHeight + self.pageYOffset;
            contextElement.style.left = event.clientX - contextElement.offsetWidth + self.pageXOffset;
        }

        // Condition 2
        // When pointer is on the bottom left end of the screen
        else if (
            // Pointer is towards bottom left of the screen
            ((event.clientY + contextElement.offsetHeight) >= (window.innerHeight))
        ) {
            // Context menu must open on the right and above the mouse pointer
            contextElement.style.top = event.clientY - contextElement.offsetHeight + self.pageYOffset;
            contextElement.style.left = event.clientX + self.pageXOffset;
        }

        // Condition 3
        // When pointer is on the top right end of the screen
        else if (
            // Pointer is towards top right of the screen
            ((event.clientX + contextElement.offsetWidth) >= (window.innerWidth - 25))
        ) {
            // Context menu must open on the right and below mouse pointer
            contextElement.style.top = event.clientY + self.pageYOffset;
            contextElement.style.left = event.clientX - contextElement.offsetWidth + self.pageXOffset;
        }

        // Condition 4
        // Default case
        else {

            // Context menu must open on the left and below mouse pointer
            contextElement.style.top = event.clientY + self.pageYOffset;
            contextElement.style.left = event.clientX + self.pageXOffset;
        }
        // --------------- Section Ends ---------------
    }

    function closeContextMenu() {

        // Hide the context menu
        contextElement.classList.add("hidden");
        contextElement.classList.remove("visible");

        // Remove the positioning of the context menu
        contextElement.removeAttribute("style");
    }

    preventDefaultMenu = function (event) {
        event.preventDefault();
    };

    document.querySelectorAll("div.context-sensitive")[0].addEventListener("contextmenu", function (event) {
        event.preventDefault();
        displayContextMenu();
    }, false);

    // Prevent browser's context menu to be displayed on custom context menu
    for (i = 0; i < contextMenuElements.length; i += 1) {
        contextMenuElements[i].addEventListener("contextmenu", preventDefaultMenu, false);
    }

    // Close the context menu when clicked anywhere
    document.querySelectorAll("body")[0].addEventListener("click", function (event) {
        if (contextElement.classList.contains("visible")) {
            event.preventDefault();
            closeContextMenu();
        }
    }, false);

    // Close the context menu when "Esc" key is pressed
    window.addEventListener("keydown", function (event) {
        if (event.keyCode === 27) {
            event.preventDefault();
            closeContextMenu();
        }
    }, false);
}
