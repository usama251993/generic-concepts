function modalElement() {
    "use strict";

    function modalReady(element) {
        var
            modal = {
                trigger: element.querySelectorAll("div.modal-button")[0],
                container: element.querySelectorAll("div.modal-container")[0],
                box: element.querySelectorAll("div.modal-box"),
                contentHolder: element.querySelectorAll("div.modal-content-holder")[0],
                closeButton: element.querySelectorAll("div.modal-close-button")[0]
            };

        function closeModalBox() {
            modal.container.classList.add("hidden");
        }

        function openModalBox() {
            modal.container.classList.remove("hidden");
            window.addEventListener("keydown", function (event) {
                if (event.keyCode === 27) {
                    closeModalBox();
                }
            }, false);
        }

        modal.trigger.addEventListener("click", function () {
            openModalBox();
        }, false);

        modal.closeButton.addEventListener("click", function () {
            closeModalBox();
        }, false);
    }
    var
        modals = document.querySelectorAll("div.modal-button"),
        activeModal,
        i;
    activeModal = function () {
        modalReady(this.parentElement);
    };

    for (i = 0; i < modals.length; i += 1) {
        modals[i].addEventListener("mouseenter", activeModal, false);
    }
}
