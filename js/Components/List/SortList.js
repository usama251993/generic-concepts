function listReady() {
	"use strict";
	var
		// Get elements to be sorted
		contentElements = document.querySelectorAll("div.element span"),
		
		// Get the control element
		buttonElement = document.querySelectorAll("div.sort-button span"),
		
		// Flag to decide whether ascending or descending sort is to be performed
		orderElementFlag,
		
		// Dummy Variables
		operationElements1 = [],
		operationElements2 = [],
		
		// Iterator count
		i;
	
	function sortElements() {
		
		// Get the elements to be sorted in the dummy variable
		for (i = 0; i < contentElements.length; i += 1) {
			operationElements1[i] = contentElements[i].textContent;
		}
		
		
		if (orderElementFlag) {
			
			// They are arranged in ascending order
			// So sort them in descending order
			operationElements2 = operationElements1.sort().reverse();
			
			// Change the value of flag
			orderElementFlag = false;
			
			// Must show a downward arrow
			buttonElement[1].classList.add("hidden");
			buttonElement[1].classList.remove("visible");
			buttonElement[2].classList.remove("hidden");
			buttonElement[2].classList.add("visible");
		} else {
			
			// They are not arranged in ascending order
			// So sort them in ascending order
			operationElements2 = operationElements1.sort();
			
			// Change the value of flag
			orderElementFlag = true;
			
			// Must show an upward arrow
			buttonElement[1].classList.remove("hidden");
			buttonElement[1].classList.add("visible");
			buttonElement[2].classList.add("hidden");
			buttonElement[2].classList.remove("visible");

		}
		
		// Change the value of elements according to the flag operations
		for (i = 0; i < operationElements2.length; i += 1) {
			contentElements[i].textContent = operationElements2[i];
		}
		
		// Clear the dummy variables
		operationElements1 = [];
		operationElements2 = [];
	}
	
	// Trigger on click
	buttonElement[0].parentElement.addEventListener("click", function () {
		sortElements();
	}, false);
}
