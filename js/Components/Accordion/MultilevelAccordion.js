function multiAccordionElement() {
    "use strict";


    function multiAccordionReady(element) {
        var
            /* Accordion Object */
            accordion = {
                components: element.querySelectorAll("div.accordion-component"),
                titles: element.querySelectorAll("div.accordion-title-holder")
            },

            animationParam = {},
            animationID = 0,
            clickedElement,

            /* Iterator */
            i;

        function hideOthers(childElement) {
            for (i = 0; i < accordion.titles.length; i += 1) {
                if (childElement !== accordion.titles[i]) {
                    accordion.titles[i].nextElementSibling.classList.add("hidden");
                }
            }
        }

        function toggleAccordion(childElement) {
            if (childElement.nextElementSibling.classList.contains("hidden")) {
                childElement.nextElementSibling.classList.remove("hidden");

            } else {
                childElement.nextElementSibling.classList.add("hidden");
            }

            hideOthers(childElement);
        }

        // function toggleIcon(childElement) {
        // var
        // svgParentElement = childElement.querySelectorAll("svg")[0],
        // svgChildElement = svgParentElement.querySelectorAll("svg")[0],
        // pathElements = svgChildElement.querySelectorAll("path"),
        // flag = false,
        // animationID = 0,
        // CSSSheet = document.querySelectorAll("link[href*=\"MultilevelAccordion.css\"]")[0].sheet,
        // ruleCSS = [],
        // animationParam = {},
        // i;
        //
        // function cleanMarkup() {
        // if (childElement.nextElementSibling.classList.contains("hidden")) {
        // console.log(childElement.nextElementSibling.nodeName + " class = \"" + childElement.nextElementSibling.className + "\" now visible");
        // } else {
        // console.log(childElement.nextElementSibling.nodeName + " class = \"" + childElement.nextElementSibling.className + "\" now hidden");
        // }
        // var paths = document.querySelectorAll("path");
        // for (i = 0; i < paths.length; i += 1) {
        // paths[i].removeAttribute("id");
        // }
        // pathElements[0].setAttribute("id", "toggle-minus");
        // pathElements[1].setAttribute("id", "toggle-plus");
        // }
        //
        // cleanMarkup();
        //
        // for (i = 0; i < CSSSheet.cssRules.length; i += 1) {
        // if (CSSSheet.cssRules[i].selectorText.indexOf("path#toggle-") > -1) {
        // ruleCSS[i] = i;
        // }
        // }
        //
        // ruleCSS = ruleCSS.filter(function (elements) {
        // return elements !== undefined;
        // });
        //
        // animationParam.degreeMinus = 0;
        // animationParam.degreePlus = 0;
        // animationParam.rate = 15;
        //
        // function animateToggle(plusOrMinus) {
        // if (flag) {
        // if (animationParam.degreeMinus >= 180 && animationParam.degreePlus >= 270) {
        // window.cancelAnimationFrame(animationID);
        // } else {
        // animationParam.degreeMinus += animationParam.rate; /* Any Number */
        // animationParam.degreePlus += animationParam.rate * 1.5; /* Any Number */
        //
        // if (animationParam.degreeMinus > 180) {
        // animationParam.degreeMinus = 180;
        // }
        // if (animationParam.degreePlus > 270) {
        // animationParam.degreePlus = 270;
        // }
        //
        // CSSSheet.cssRules[ruleCSS[0]].style.setProperty("transform", "rotate(" + animationParam.degreeMinus * plusOrMinus + "deg)");
        //
        // CSSSheet.cssRules[ruleCSS[1]].style.setProperty("transform", "rotate(" + animationParam.degreePlus * plusOrMinus + "deg)");
        //
        // animationID = window.requestAnimationFrame(function () {
        // animateToggle(plusOrMinus);
        // });
        // }
        // } else {
        //
        // if (animationParam.degreeMinus <= 0 && animationParam.degreePlus <= 0) {
        // window.cancelAnimationFrame(animationID);
        // } else {
        //
        // animationParam.degreeMinus -= animationParam.rate; /* Any Number */
        // animationParam.degreePlus -= animationParam.rate * 1.5; /* Any Number */
        //
        // if (animationParam.degreeMinus < 0) {
        // animationParam.degreeMinus = 0;
        // }
        // if (animationParam.degreePlus < 0) {
        // animationParam.degreePlus = 0;
        // }
        //
        // CSSSheet.cssRules[ruleCSS[0]].style.setProperty("transform", "rotate(" + animationParam.degreeMinus * plusOrMinus * -1 + "deg)");
        // CSSSheet.cssRules[ruleCSS[1]].style.setProperty("transform", "rotate(" + animationParam.degreePlus * plusOrMinus * -1 + "deg)");
        //
        // animationID = window.requestAnimationFrame(function () {
        // animateToggle(plusOrMinus);
        // });
        // }
        // }
        // }
        //
        // window.requestAnimationFrame(function () {
        // var plusOrMinus = Math.random() > 0.5 ? 1 : -1;
        // animateToggle(plusOrMinus);
        // });
        // flag = !flag;
        // }

        for (i = 0; i < accordion.titles.length; i += 1) {

            /* Invoke the function when accordion title is clicked */
            accordion.titles[i].onclick = function () {
                clickedElement = this;
                window.requestAnimationFrame(function (timestamp) {
                    // toggleIcon(clickedElement);
                    toggleAccordion(clickedElement);
                });
            };
        }
    }

    var
        accordions = document.querySelectorAll("div.accordion"),
        activeAccordion,
        i;
    activeAccordion = function () {
        multiAccordionReady(this);
        // toggleReady(this);
    };

    for (i = 0; i < accordions.length; i += 1) {
        accordions[i].addEventListener("mouseenter", activeAccordion, false);
    }
}
