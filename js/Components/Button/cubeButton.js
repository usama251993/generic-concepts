function buttonReady() {
  "use strict";

  let boxHolder = window.document.querySelectorAll("div.box-holder")[0];
  let boxElement = boxHolder.querySelectorAll("div.box")[0];
  let boxFaces = boxHolder.querySelectorAll("div.box-face");
  let cubeButtonSheet = {};
  // let isEverythingPositioned = false;
  let faceDimension = {
    front: { width: 0, height: 0, },
    right: { width: 0, height: 0, },
    top: { width: 0, height: 0, },
    back: { width: 0, height: 0, },
    left: { width: 0, height: 0, },
    bottom: { width: 0, height: 0, }
  };
  let maxDimensions = {
    height: 0,
    width: 0,
    depth: 0
  };

  for (let sheet of window.document.styleSheets) {
    if (sheet.href.indexOf("cubeButton.css") > 0) cubeButtonSheet = sheet;
  }

  boxFaces.forEach((face) => {
    faceDimension[face.getAttribute("id").slice(5)].height = face.clientHeight;
    faceDimension[face.getAttribute("id").slice(5)].width = face.clientWidth;
  });

  function computeMaxDimensions(faceDimension) {
    let tempArray = [];

    tempArray.push(faceDimension.front.height);
    tempArray.push(faceDimension.right.height);
    tempArray.push(faceDimension.back.height);
    tempArray.push(faceDimension.left.height);
    maxDimensions.height = Math.max(...tempArray);

    tempArray = [];
    tempArray.push(faceDimension.front.width);
    tempArray.push(faceDimension.top.width);
    tempArray.push(faceDimension.back.width);
    tempArray.push(faceDimension.bottom.width);
    maxDimensions.width = Math.max(...tempArray);

    tempArray = [];
    tempArray.push(faceDimension.right.width);
    tempArray.push(faceDimension.top.height);
    tempArray.push(faceDimension.left.width);
    tempArray.push(faceDimension.bottom.height);
    maxDimensions.depth = Math.max(...tempArray);

  }

  computeMaxDimensions(faceDimension);

  let styleObject = {
    back: {
      dimensions: {
        width: maxDimensions.width + "px",
        height: maxDimensions.height + "px"
      },
      location: {
        top: "0px",
        left: (3 * maxDimensions.width / 2 + maxDimensions.depth) + "px"
      },
      morph: {
        transformOrigin: "center center",
        transform: "rotateY(180deg)"
      }
    },
    right: {
      dimensions: {
        width: maxDimensions.depth + "px",
        height: maxDimensions.height + "px"
      },
      location: {
        top: "0px",
        left: (3 * maxDimensions.width / 2) + "px"
      },
      morph: {
        transformOrigin: "right center",
        transform: "rotateY(90deg)"
      }
    },
    top: {
      dimensions: {
        width: maxDimensions.width + "px",
        height: maxDimensions.depth + "px"
      },
      location: {
        top: "0px",
        left: (maxDimensions.width / 2) + "px"
      },
      morph: {
        transformOrigin: "top center",
        transform: "rotateX(90deg) translateZ(" + (maxDimensions.depth - maxDimensions.height) / -2 + "px)"
      }
    },
    left: {
      dimensions: {
        width: maxDimensions.depth + "px",
        height: maxDimensions.height + "px"
      },
      location: {
        top: "0px",
        left: (maxDimensions.width / -2) + "px"
      },
      morph: {
        transformOrigin: "left center",
        transform: "rotateY(-90deg)"
      }
    },
    bottom: {
      dimensions: {
        width: maxDimensions.width + "px",
        height: maxDimensions.depth + "px"

      },
      location: {
        top: "0px",
        left: (maxDimensions.width / -2 - maxDimensions.depth) + "px"
      },
      morph: {
        transformOrigin: "bottom center",
        transform: "rotateX(-90deg) translateZ(" + (maxDimensions.depth - maxDimensions.height) / -2 + "px)"
      }
    },
    front: {
      dimensions: {
        width: maxDimensions.width + "px",
        height: maxDimensions.height + "px"
      },
      location: {
        top: "0px",
        left: (3 * maxDimensions.width / - 2 - maxDimensions.depth) + "px"
      },
      morph: {
        transformOrigin: "center center",
        transform: "translateZ(" + maxDimensions.depth + "px)"
      }
    }
  }

  function modifyRule(face, propertyText, propertyValue) {
    let ruleIndex = 0;
    for (let rule of cubeButtonSheet.cssRules) {
      if (rule.selectorText.indexOf(face) > 0) {
        let ruleClone = rule;
        let propertiesObject = {};
        let ruleString = "";

        propertiesObject["index"] = ruleIndex;
        Object.defineProperties(propertiesObject, {
          "ruleIndex": {
            value: ruleIndex,
            writable: false
          }
        });

        for (let eachStyle of ruleClone.style) {
          propertiesObject[eachStyle] = ruleClone.style.getPropertyValue(eachStyle);
        }

        propertiesObject[propertyText] = propertyValue;

        ruleString = ruleClone.selectorText + "{";
        for (let property in propertiesObject) {
          if (property === "ruleIndex") { } else {
            ruleString += property + ":" + propertiesObject[property] + ";";
          }
        }
        ruleString += "}";

        cubeButtonSheet.deleteRule(propertiesObject.ruleIndex);
        cubeButtonSheet.insertRule(ruleString, propertiesObject.ruleIndex);
      }
      ruleIndex += 1;
    }
  }

  function styleFaces(styleObject) {
    for (let face in styleObject) {
      for (let propertyGroup in styleObject[face]) {
        for (let property in styleObject[face][propertyGroup]) {
          let propertyText = property.replace(/([A-Z])/g, (g) => `-${g[0].toLowerCase()}`);
          modifyRule(face, propertyText, styleObject[face][propertyGroup][property]);
        }
      }
    }
  }

  styleFaces(styleObject);

  function confirmAction(confirmation, animObject, animDuration) {
    let intervalID = 0;
    let timeoutID = 0;
    confirmation.addEventListener("click", () => {
      if (confirmation.textContent.indexOf("Yes") >= 0) {
        animObject = boxElement.animate([
          {
            transform: "rotateX(0deg) rotateY(180deg) rotateZ(0deg) translateX(0px) translateY(0px) translateZ(0px)",
            offset: 0
          }, {
            transform: "rotateX(0deg) rotateY(90deg) rotateZ(0deg) translateX(0px) translateY(0px) translateZ(" + maxDimensions.depth / -2 + "px)",
            offset: 1
          }
        ], { duration: animDuration, fill: "forwards" });
        intervalID = window.setInterval(() => {
          animObject = boxElement.animate([
            {
              transform: window.getComputedStyle(boxElement).transform,
              offset: 0
            }, {
              transform: "rotateX(0deg) rotateY(0deg) rotateZ(0deg) translateX(0px) translateY(0px) translateZ(0px)",
              offset: 1
            }
          ], { duration: animDuration, fill: "forwards" });
          timeoutID = window.setTimeout(() => {
            window.clearInterval(intervalID);
          }, animDuration);
        }, animDuration * 4);
      } else {
        animObject = boxElement.animate([
          {
            transform: "rotateX(0deg) rotateY(180deg) rotateZ(0deg) translateX(0px) translateY(0px) translateZ(0px)",
            offset: 0
          }, {
            transform: "rotateX(0deg) rotateY(270deg) rotateZ(0deg) translateX(0px) translateY(0px) translateZ(" + maxDimensions.depth / -2 + "px)",
            // transform: "rotateX(0deg) rotateY(-90deg) rotateZ(0deg)",
            offset: 1
          }
        ], { duration: animDuration, fill: "forwards" });
        intervalID = window.setInterval(() => {
          animObject = boxElement.animate([
            {
              transform: window.getComputedStyle(boxElement).transform,
              offset: 0
            }, {
              transform: "rotateX(0deg) rotateY(0deg) rotateZ(0deg)",
              offset: 1
            }
          ], { duration: animDuration, fill: "forwards" });
          timeoutID = window.setTimeout(() => {
            window.clearInterval(intervalID);
          }, animDuration);
        }, animDuration * 4);
      }
    }, false);
  }

  function clickFace(face) {
    let faceName = face.getAttribute("id").slice(5);
    let animObject = {};
    let animDuration = 500;
    let mySwitchCase = {
      "front": () => {
        face.addEventListener("click", () => {
          animObject = boxElement.animate([
            {
              transform: window.getComputedStyle(boxElement).transform,
              offset: 0
            }, {
              transform: "rotateX(0deg) rotateY(-180deg) rotateZ(0deg) translateX(0px) translateY(0px) translateZ(0px)",
              offset: 1
            }
          ], { duration: animDuration, fill: "forwards" });
        }, false);
      },
      "back": () => {
        face.querySelectorAll("div.confirm").forEach((confirmation) => {
          confirmAction(confirmation, animObject, animDuration);
        });
      },
      "right": () => {},
      "left": () => { },
      "top": () => { },
      "bottom": () => { },
      "default": (() => { console.log("Error!"); })
    },
      defCase;

    if (mySwitchCase[faceName]) {
      defCase = mySwitchCase[faceName];
    } else { defCase = mySwitchCase["default"]; }

    return defCase();
  }

  boxFaces.forEach((face) => {
    clickFace(face);
  });

  // window.addEventListener("resize", (eventData) => {
  //   for (let face in styleObject) {
  //     for (let propertyGroup in styleObject[face]) {
  //       if (propertyGroup === "dimensions") {
  //         for (let property in styleObject[face][propertyGroup]) {
  //           let propertyText = property.replace(/([A-Z])/g, (g) => `-${g[0].toLowerCase()}`);
  //           modifyRule(face, propertyText, "auto");
  //         }
  //       }
  //     }
  //   }
  //   computeMaxDimensions(faceDimension);
  //   styleFaces();
  //   console.log(maxDimensions);
  // }, false);

  function* positionFace() {
    let generatorClosedString = "Generator closed";
    return generatorClosedString;
  }

  let positionGenerator = positionFace();

  boxFaces.forEach((face) => {
    // let nextInstance = positionGenerator.next();
    // let nextValueFunction = nextInstance.value;
    // nextValueFunction(face);
    // if (face === boxFaces[boxFaces.length - 1]) isEverythingPositioned = true;
  });

  function animateBox() {
    boxElement.animate([
      { transform: "none", offset: 0 }, { transform: "rotateX(360deg) rotateY(-180deg) rotateZ(0deg)", offset: 1 }
    ], { duration: 5000, iterations: Infinity, direction: "alternate-reverse" })
  }

  // animateBox();

  function showFace(faceName) {

    //   let positionAnimation = {};
    //   let positionDuration = 1000;

    //   let mySwitchCase = {

    //     "face-front": () => {
    //       positionAnimation = boxElement.animate([
    //         {
    //           transform: window.getComputedStyle(boxElement).transform,
    //           offset: 0
    //         }, {
    //           transform: "rotateX(0deg) rotateY(0deg) rotateZ(0deg)",
    //           offset: 1
    //         }
    //       ], { duration: positionDuration, fill: "forwards" });
    //     },

    //     "face-back": () => {
    //       positionAnimation = boxElement.animate([
    //         {
    //           transform: window.getComputedStyle(boxElement).transform,
    //           offset: 0
    //         }, {
    //           transform: "rotateX(0deg) rotateY(180deg) rotateZ(0deg)",
    //           offset: 1
    //         }
    //       ], { duration: positionDuration, fill: "forwards" });
    //     },

    //     "face-right": () => {
    //       positionAnimation = boxElement.animate([
    //         {
    //           transform: window.getComputedStyle(boxElement).transform,
    //           offset: 0
    //         }, {
    //           transform: "rotateX(0deg) rotateY(-90deg) rotateZ(0deg) translateX(0px) translateY(0px) translateZ(" + maxDimensions.depth / -2 + "px)",
    //           offset: 1
    //         }
    //       ], { duration: positionDuration, fill: "forwards" });
    //     },

    //     "face-left": () => {
    //       positionAnimation = boxElement.animate([
    //         {
    //           transform: window.getComputedStyle(boxElement).transform,
    //           offset: 0
    //         }, {
    //           transform: "rotateX(0deg) rotateY(90deg) rotateZ(0deg) translateX(0px) translateY(0px) translateZ(" + maxDimensions.depth / -2 + "px)",
    //           offset: 1
    //         }
    //       ], { duration: positionDuration, fill: "forwards" });
    //     },

    //     "face-top": () => {
    //       positionAnimation = boxElement.animate([
    //         {
    //           transform: window.getComputedStyle(boxElement).transform,
    //           offset: 0
    //         }, {
    //           transform: "rotateX(-90deg) rotateY(0deg) rotateZ(0deg) translateZ(-" + maxDimensions.height / 2 + "px)",
    //           offset: 1
    //         }
    //       ], { duration: positionDuration, fill: "forwards" });
    //     },

    //     "face-bottom": () => {
    //       positionAnimation = boxElement.animate([
    //         {
    //           transform: window.getComputedStyle(boxElement).transform,
    //           offset: 0
    //         }, {
    //           transform: "rotateX(90deg) rotateY(0deg) rotateZ(0deg) translateZ(" + (maxDimensions.height / 2 - maxDimensions.depth) + "px)",
    //           offset: 1
    //         }
    //       ], { duration: positionDuration, fill: "forwards" });
    //     },

    //     "default": (() => { console.log("Error!"); })
    //   },
    //     defCase;

    //   if (mySwitchCase[faceName]) {
    //     defCase = mySwitchCase[faceName];
    //   } else { defCase = mySwitchCase["default"]; }

    //   return defCase();
  }

  let radios = window.document.querySelectorAll("input[type=\"radio\"]");
  radios.forEach((radio) => {
    radio.addEventListener("change", (event) => {
      // showFace(event.target.value);
    }, false);
  });

}