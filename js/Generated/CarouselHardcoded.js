function pageReady() {
    "use strict";
    var
        carousel = {
            arrowLeft: document.querySelectorAll("div#left")[0],
            arrowRight: document.querySelectorAll("div#right")[0],
            contents: document.querySelectorAll("div.carousel-content"),
            buttons: document.querySelectorAll("div.carousel-button")
        },
        i,
        j;

    function changeContent(direction) {
        for (i = 0; i < carousel.contents.length; i += 1) {
            if (carousel.contents[i].classList.contains("visible")) {
                carousel.contents[i].classList.add("hidden");
                carousel.contents[i].classList.remove("visible");
                carousel.buttons[i].classList.remove("active");
                if (direction) {
                    if (i === (carousel.contents.length - 1)) {
                        carousel.contents[0].classList.add("visible");
                        carousel.contents[0].classList.remove("hidden");
                        carousel.buttons[0].classList.add("active");
                        break;
                    } else {
                        carousel.contents[i + 1].classList.add("visible");
                        carousel.contents[i + 1].classList.remove("hidden");
                        carousel.buttons[i + 1].classList.add("active");
                        
                        break;
                    }
                } else {
                    if (i === 0) {
                        carousel.contents[carousel.contents.length - 1].classList.add("visible");
                        carousel.contents[carousel.contents.length - 1].classList.remove("hidden");
                        carousel.buttons[carousel.contents.length - 1].classList.add("active");
                        break;
                    } else {
                        carousel.contents[i - 1].classList.add("visible");
                        carousel.contents[i - 1].classList.remove("hidden");
                        carousel.buttons[i - 1].classList.add("active");
                        break;
                    }
                }
            }
        }
    }

    function changeButtons(event) {
        for (i = 0; i < carousel.buttons.length; i += 1) {
            carousel.buttons[i].classList.remove("active");
        }
        event.target.classList.add("active");
        for (i = 0; i < carousel.buttons.length; i += 1) {
            if (carousel.buttons[i].classList.contains("active")) {
                carousel.contents[i].classList.add("visible");
                carousel.contents[i].classList.remove("hidden");
            } else {
                carousel.contents[i].classList.add("hidden");
                carousel.contents[i].classList.remove("visible");
            }
        }
    }

    carousel.arrowLeft.addEventListener("click", function () {
        changeContent(false);
    }, false);

    carousel.arrowRight.addEventListener("click", function () {
        changeContent(true);
    }, false);

    for (i = 0; i < carousel.buttons.length; i += 1) {
        carousel.buttons[i].addEventListener("click", function (event) {
            changeButtons(event);
        }, false);
    }
}

window.addEventListener("load", function () {
    "use strict";
    pageReady();
}, false);
