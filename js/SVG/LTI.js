function logoReady() {
    "use strict";

    var
        svgParentElement = document.querySelectorAll("svg#logo-svg")[0],
        svgChildElement = svgParentElement.querySelectorAll("svg")[0],
        verticalPaths = svgChildElement.querySelectorAll("path[id*=\"vertical\"]"),
        horizontalPaths = svgChildElement.querySelectorAll("path[id*=\"horizontal\"]"),
        plusOrMinus = -1,
        i;


    function logoAnimate() {
        for (i = 0; i < verticalPaths.length; i += 1) {
            verticalPaths[i].animate([{
                opacity: 1,
                offset: 0
            }, {
                opacity: 0,
                offset: 1
            }], {
                duration: 0,
                fill: "forwards"
            });
        }

        for (i = 0; i < horizontalPaths.length; i += 1) {
            horizontalPaths[i].animate([{
                opacity: 1,
                offset: 0
            }, {
                opacity: 0,
                offset: 1
            }], {
                duration: 0,
                fill: "forwards"
            });
        }
        for (i = 0; i < horizontalPaths.length; i += 1) {
            if (i === horizontalPaths.length - 1) plusOrMinus = 1;
            horizontalPaths[i].animate([{
                transform: "translate(" + plusOrMinus * horizontalPaths[i].getTotalLength() + "px, 0px)",
                opacity: 0,
                offset: 0
            }, {
                transform: "translate(0px, 0px)",
                opacity: 0.5,
                offset: 0.5
            }, {
                transform: "translate(0px, 0px)",
                opacity: 1,
                offset: 1
            }], {
                delay: 0,
                duration: 500,
                fill: "forwards",
                iterations: 1
            });
        }

        plusOrMinus = -1;

        for (i = 0; i < verticalPaths.length; i += 1) {
            if (i !== 0) plusOrMinus = 1
            verticalPaths[i].animate([{
                transform: "translate(0px, " + plusOrMinus * verticalPaths[i].getTotalLength() + "px)",
                opacity: 0,
                offset: 0
            }, {
                transform: "translate(0px, 0px)",
                opacity: 0.5,
                offset: 0.5
            }, {
                transform: "translate(0px, 0px)",
                opacity: 1,
                offset: 1
            }], {
                delay: (i * 250),
                duration: 750,
                fill: "forwards",
                iterations: 1
            });
        }
        
        plusOrMinus = -1;
            
    }

    svgParentElement.addEventListener("click", function () {
        logoAnimate();
    }, false);
}

document.addEventListener("DOMContentLoaded", function () {
    "use strict";
    logoReady();
}, false);
