function bluetoothReady() {
    "use strict";
    var
        svgParentElement = document.querySelectorAll("svg#bluetooth-svg")[0],
        svgChildElement = svgParentElement.querySelectorAll("svg")[0],
        pathElements = svgChildElement.querySelectorAll("path[id*=\"bluetooth-\"]"),
        flag = false,
        animationID = 0,
        CSSSheet = document.querySelectorAll("link[href*=\"SVGToggle.css\"]")[0].sheet,
        ruleCSS = [],
        animationParam = {},
        i;

    /* Start: Using JavaScript animate() function */

    // function options(duration, fill) {
    // /*
    // Reference:
    // https://developer.mozilla.org/en-US/docs/Web/API/Element/animate
    // */
    // return {
    // delay: 0,
    // direction: "normal",
    // duration: duration,
    // easing: "linear",
    // endDelay: 0,
    // fill: fill,
    // iterationStart: 0.0,
    // iterations: 1
    // };
    // }
    //
    // function bluetoothAnimation() {
    // if (flag) {
    // /* 
    // flag = true indicates Bluetooth is ON
    // So turn the Bluetooth OFF and change the flag to false
    // */
    // bluetoothOff();
    // flag = !flag;
    // } else {
    // /* 
    // flag = false indicates Bluetooth is OFF
    // So turn the Bluetooth ON and change the flag to true
    // */
    // bluetoothOn();
    // flag = !flag;
    // }
    // }
    //
    // function bluetoothOn() {
    // pathElements[0].animate([{
    // stroke: "rgba(0, 0, 0, 1)",
    // fill: "rgba(0, 0, 0, 0)",
    // offset: 0
    // }, {
    // stroke: "rgba(0, 0, 0, 0)",
    // fill: "url(#bluetooth-grad)",
    // offset: 1
    // }], options(500, "forwards"));
    // pathElements[1].animate([{
    // strokeDasharray: pathElements[1].getTotalLength(),
    // strokeDashoffset: pathElements[1].getTotalLength(),
    // stroke: "rgba(0, 0, 0, 1)",
    // strokeWidth: "1px",
    // filter: "none",
    // offset: 0
    // }, {
    // strokeDasharray: pathElements[1].getTotalLength(),
    // strokeDashoffset: 0,
    // stroke: "rgba(255, 255, 255, 1)",
    // strokeWidth: "5px",
    // filter: "url(#bluetooth-shadow)",
    // offset: 1
    // }], options(500, "forwards"));
    // }
    //
    // function bluetoothOff() {
    // pathElements[0].animate([{
    // stroke: "rgba(0, 0, 0, 0)",
    // fill: "url(#bluetooth-grad)",
    // offset: 0
    // }, {
    // stroke: "rgba(0, 0, 0, 1)",
    // fill: "rgba(0, 0, 0, 0)",
    // offset: 1
    // }], options(500, "forwards"));
    // pathElements[1].animate([{
    // strokeDasharray: pathElements[1].getTotalLength(),
    // strokeDashoffset: 0,
    // stroke: "rgba(255, 255, 255, 1)",
    // strokeWidth: "5px",
    // filter: "url(#bluetooth-shadow)",
    // offset: 0
    // }, {
    // stroke: "rgba(0, 0, 0, 1)",
    // strokeWidth: "1px",
    // filter: "none",
    // offset: 1
    // }], options(500, "forwards"));
    // }

    /* End: Using JavaScript animate() function */

    /* Start: Using requestAnimationFrame */
    
    /* Set the indices of CSS Rules according to the CSS Stylesheet */
    for (i = 0; i < CSSSheet.cssRules.length; i += 1) {
        if(CSSSheet.cssRules[i].selectorText.indexOf("path#bluetooth-") > -1)
            ruleCSS[i] = i;
    }
    
    /* Filter out the undefined entries in the array */
    ruleCSS = ruleCSS.filter(function(elements) {
        return elements != undefined;
    });
    
    /* 
    Set the initial animation parameters for capsule
    opacity = 0 // because when bluetooth is off, it must be filled white/transparent
    stroke-width = 1px
    */
    animationParam.bCapsuleFillOpacity = 0;
    animationParam.bCapsuleStrokeWidth = 1;
    
    function animateBluetoothCapsule() {
        
        /* flag = true indicates capsule must be toggled to ON */
        if (flag) {
            
            /* 
            Stop Condition
            opacity = 1
            stroke-width = 0px
            */
            if (
                animationParam.bCapsuleFillOpacity === 1 &&
                animationParam.bCapsuleStrokeWidth === 0
            ) {
                
                /* Not required in this case because path animation is responsible for ending the toggle animation */
                // window.cancelAnimationFrame(animationID);
            } 
            
            /*
            Toggle the capsule 
            Fill the capsule with gradient color
            Shrink the stroke to zero width
            */
            else {
                
                /* Increase/decrease the parameters according to the requirements of animation */
                animationParam.bCapsuleFillOpacity += Math.random() / 10;
                animationParam.bCapsuleStrokeWidth -= Math.random() / 10;
                
                /* Set the limits of increment/decrement */
                if (animationParam.bCapsuleFillOpacity > 1) animationParam.bCapsuleFillOpacity = 1;
                if (animationParam.bCapsuleStrokeWidth < 0) animationParam.bCapsuleStrokeWidth = 0;
                
                /* Set the modified parameters in the StyleSheet */
                CSSSheet.cssRules[ruleCSS[0]].style.setProperty("fill", "url(#bluetooth-grad)");
                CSSSheet.cssRules[ruleCSS[0]].style.setProperty("opacity", animationParam.bCapsuleFillOpacity);
                CSSSheet.cssRules[ruleCSS[0]].style.setProperty("stroke-width", animationParam.bCapsuleStrokeWidth + "px");
                
                /* Command for animation */
                animationID = window.requestAnimationFrame(function () {
                    animateBluetoothCapsule();
                });
            }
        } 
        
        /* flag = false indicates capsule must be toggled to OFF */
        else {
            
            /* 
            Stop Condition
            opacity = 0
            stroke-width = 1px
            */
            if (
                animationParam.bCapsuleFillOpacity === 0 &&
                animationParam.bCapsuleStrokeWidth === 1
            ) {
                
                /* Not required in this case because path animation is responsible for ending the toggle animation */
                // window.cancelAnimationFrame(animationID);
                
                /* Set the final values of animation parametes after the animation is complete */
                CSSSheet.cssRules[ruleCSS[0]].style.setProperty("opacity", "1");
                CSSSheet.cssRules[ruleCSS[0]].style.setProperty("fill", "none");
            } 
            
            /*
            Toggle the capsule 
            Revert the gradient color and stroke width            
            */
            else {
                
                /* Increment/decrement */
                animationParam.bCapsuleFillOpacity -= Math.random() / 10;
                animationParam.bCapsuleStrokeWidth += Math.random() / 10;
                
                /* Limitation */
                if (animationParam.bCapsuleFillOpacity < 0) animationParam.bCapsuleFillOpacity = 0;
                if (animationParam.bCapsuleStrokeWidth > 1) animationParam.bCapsuleStrokeWidth = 1;
                
                /* Setting */
                CSSSheet.cssRules[ruleCSS[0]].style.setProperty("opacity", animationParam.bCapsuleFillOpacity);
                CSSSheet.cssRules[ruleCSS[0]].style.setProperty("stroke-width", animationParam.bCapsuleStrokeWidth + "px");
                
                /* Command */
                animationID = window.requestAnimationFrame(function () {
                    animateBluetoothCapsule();
                });
            }
        }
    }

    /* 
    Set the initial animation parameters for bluetooth path
    stroke = rgba(0, 0, 0, 1)
    stroke-width = 1px
    stroke-dasharray = Length of bluetooth path
    stroke-dashoffset = Length of bluetooth path
    */
    animationParam.bStroke = 0;
    animationParam.bStrokeWidth = 1;
    animationParam.bStrokeDasharray = pathElements[1].getTotalLength();
    animationParam.bStrokeDashoffset = pathElements[1].getTotalLength();
    
    function animateBluetoothPath() {
        
        /* flag = true indicates path must be toggled to ON */
        if (flag) {
            
            /* 
            Stop Condition 
            stroke-width = 5px
            stroke-dashoffset = 0
            */
            if (
                animationParam.bStrokeWidth === 5 &&
                animationParam.bStrokeDashoffset === 0
            ) {
                window.cancelAnimationFrame(animationID);
                
                /* Set the final values of animation parametes after the animation is complete */
                CSSSheet.cssRules[ruleCSS[1]].style.setProperty("stroke", "rgba(255, 255, 255, 1)");
            } 
            
            /*
            Toggle the path 
            Increase stroke width to 5px
            Gradually change the stroke from black to white
            Gradually decrease the dash offset from path length to 0
            */
            else {
                
                /* Increment/decrement */
                animationParam.bStroke += Math.floor(Math.random() * 12);
                animationParam.bStrokeWidth += Math.random();
                animationParam.bStrokeDashoffset -= Math.random() * 15;

                /* Limitation */
                if (animationParam.bStroke > 255) animationParam.bStroke = 255;
                if (animationParam.bStrokeWidth > 5) animationParam.bStrokeWidth = 5;
                if (animationParam.bStrokeDashoffset < 0) animationParam.bStrokeDashoffset = 0;

                /* Setting */
                CSSSheet.cssRules[ruleCSS[1]].style.setProperty("stroke", "rgba(" + animationParam.bStroke + ", " + animationParam.bStroke + ", " + animationParam.bStroke + ", 1)");
                CSSSheet.cssRules[ruleCSS[1]].style.setProperty("stroke-width", animationParam.bStrokeWidth);
                CSSSheet.cssRules[ruleCSS[1]].style.setProperty("stroke-dasharray", animationParam.bStrokeDasharray);
                CSSSheet.cssRules[ruleCSS[1]].style.setProperty("stroke-dashoffset", animationParam.bStrokeDashoffset);

                /* Command */
                animationID = window.requestAnimationFrame(function () {
                    animateBluetoothPath();
                });
            }
        } 
        
        /* flag = false indicates path must be toggled to OFF */
        else {
            
            /* 
            Stop Condition 
            stroke-width = 1px
            stroke-dashoffset = path length
            */
            if (                
                animationParam.bStrokeWidth === 1 &&
                animationParam.bStrokeDashoffset === pathElements[1].getTotalLength()
            ) {
                // window.cancelAnimationFrame(animationID);
                CSSSheet.cssRules[ruleCSS[1]].style.setProperty("stroke", "rgba(0, 0, 0, 1)");
                CSSSheet.cssRules[ruleCSS[1]].style.setProperty("stroke-width", "1px");
                CSSSheet.cssRules[ruleCSS[1]].style.setProperty("stroke-dasharray", animationParam.bStrokeDasharray);
                CSSSheet.cssRules[ruleCSS[1]].style.setProperty("stroke-dashoffset", 0);
            } else {
                animationParam.bStroke -= Math.floor(Math.random() * 10);
                animationParam.bStrokeWidth -= Math.random();
                animationParam.bStrokeDashoffset += Math.random() * 100;

                if (animationParam.bStroke < 0) animationParam.bStroke = 0;
                if (animationParam.bStrokeWidth < 1) animationParam.bStrokeWidth = 1;
                if (animationParam.bStrokeDashoffset > pathElements[1].getTotalLength()) animationParam.bStrokeDashoffset = pathElements[1].getTotalLength();

                CSSSheet.cssRules[ruleCSS[1]].style.setProperty("stroke", "rgba(" + animationParam.bStroke + ", " + animationParam.bStroke + ", " + animationParam.bStroke + ", 1)");
                CSSSheet.cssRules[ruleCSS[1]].style.setProperty("stroke-width", animationParam.bStrokeWidth);
                CSSSheet.cssRules[ruleCSS[1]].style.setProperty("stroke-dasharray", animationParam.bStrokeDasharray);
                CSSSheet.cssRules[ruleCSS[1]].style.setProperty("stroke-dashoffset", animationParam.bStrokeDashoffset);
                animationID = window.requestAnimationFrame(function () {
                    animateBluetoothPath();
                });
            }
        }
    }

    /* End: Using requestAnimationFrame */

    /* Animate on click using animate() function */
    // svgParentElement.addEventListener("click", function () {
    // bluetoothAnimation();
    // }, false);

    /* Animate on click using window requestAnimationFrame */
    svgParentElement.addEventListener("click", function () {
        window.requestAnimationFrame(function () {
            animateBluetoothCapsule();
            animateBluetoothPath();
        });
        flag = !flag;
    }, false);

    /* 
    Animate on hover using animate() function
    Obsolete in this case
    */
    // svgParentElement.addEventListener("mouseenter", function () {
    // arrowsAnimation();
    // }, false);
    // svgParentElement.addEventListener("mouseleave", function () {
    // arrowsAnimation();
    // }, false);

    /* 
    Animate on hover using requestAnimationFrame
    Obsolete in this case
    */
    // svgParentElement.addEventListener("mouseenter", function () {
    // window.requestAnimationFrame(function () {
    // animateBluetoothCapsule();
    // animateBluetoothPath();
    // });
    // flag = !flag;
    // }, false);
    // svgParentElement.addEventListener("mouseleave", function () {
    // window.requestAnimationFrame(function () {
    // animateBluetoothCapsule();
    // animateBluetoothPath();
    // });
    // flag = !flag;
    // }, false);

}

document.addEventListener("DOMContentLoaded", function () {
    "use strict";
    bluetoothReady();
}, false);
