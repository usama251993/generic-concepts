function toggleReady() {
    "use strict";
    var
        svgParentElement = document.querySelectorAll("svg")[0],
        svgChildElement = svgParentElement.querySelectorAll("svg")[0],
        pathElements = svgChildElement.querySelectorAll("path[id*=\"toggle-\"]"),
        flag = false,
        animationID = 0,
        CSSSheet = document.querySelectorAll("link[href*=\"Toggle.css\"]")[0].sheet,
        ruleCSS = [],
        animationParam = {},
        i;

    /* Start: Using JavaScript animate() function */

    // function options(duration, fill) {
    // /*
    // Reference:
    // https://developer.mozilla.org/en-US/docs/Web/API/Element/animate
    // */
    // return {
    // delay: 0,
    // direction: "normal",
    // duration: duration,
    // easing: "linear",
    // endDelay: 0,
    // fill: fill,
    // iterationStart: 0.0,
    // iterations: 1
    // };
    // }
    //
    // function toggleAnimation() {
    // if (flag) {
    // /* 
    // flag = true indicates toggle is " - "
    // So change the toggle to " + " and flag to false
    // */
    // toggleToPlus();
    // flag = !flag;
    // } else {
    // /* 
    // flag = false indicates toggle is " + "
    // So change the toggle to " - " the flag to true
    // */
    // toggleToMinus();
    // flag = !flag;
    // }
    // }
    //
    // function toggleToMinus() {
    // pathElements[0].animate([{
    // transform: "rotate(0deg)",
    // offset: 0
    // }, {
    // transform: "rotate(180deg)",
    // offset: 1
    // }], options(250, "forwards"));
    // 
    // pathElements[1].animate([{
    // transform: "rotate(0deg)",
    // offset: 0
    // }, {
    // transform: "rotate(270deg)",
    // offset: 1
    // }], options(250, "forwards"));
    // }
    //
    // function toggleToPlus() {
    // pathElements[0].animate([{
    // transform: "rotate(180deg)",
    // offset: 0
    // }, {
    // transform: "rotate(360deg)",
    // offset: 1
    // }], options(250, "forwards"));
    //        
    // pathElements[1].animate([{
    // transform: "rotate(270deg)",
    // offset: 0
    // }, {
    // transform: "rotate(540deg)",
    // offset: 1
    // }], options(250, "forwards"));
    // }

    /* End: Using JavaScript animate() function */

    /* Start: Using requestAnimationFrame */

    /* Set the indices of CSS Rules according to the CSS Stylesheet */
    for (i = 0; i < CSSSheet.cssRules.length; i += 1) {
        if (CSSSheet.cssRules[i].selectorText.indexOf("path#toggle-") > -1)
            ruleCSS[i] = i;
    }

    /* Filter out the undefined entries in the array */
    ruleCSS = ruleCSS.filter(function (elements) {
        return elements !== undefined;
    });

    /* 
    Set the initial animation parameters 
    Degree of rotation set to 0
    */
    animationParam.degreeMinus = 0;
    animationParam.degreePlus = 0;
    animationParam.rate = 15;

    function animateToggle(plusOrMinus) {

        /* flag = true indicates toggle is " - " */
        if (flag) {

            /* 
            Stop Condition
            rotate = 180deg
            */
            if (animationParam.degreeMinus >= 180 && animationParam.degreePlus >= 270) {
                window.cancelAnimationFrame(animationID);
            }

            /* Change the toggle */
            else {

                /* 
                Rate of rotation 
                Math.random() function is used to set a random rate of rotation
                Use of definite values is acceptable
                */
                // animationParam.degree += Math.floor(Math.random() * 10); /* Any Number */
                animationParam.degreeMinus += animationParam.rate; /* Any Number */
                animationParam.degreePlus += animationParam.rate * 1.5; /* Any Number */

                /* Prevent degree of rotation above 180 */
                if (animationParam.degreeMinus > 180) animationParam.degreeMinus = 180;
                if (animationParam.degreePlus > 270) animationParam.degreePlus = 270;

                /* Set the corresponding value in StyleSheet */
                CSSSheet.cssRules[ruleCSS[0]].style.setProperty("transform", "rotate(" + animationParam.degreeMinus * plusOrMinus + "deg)");

                CSSSheet.cssRules[ruleCSS[1]].style.setProperty("transform", "rotate(" + animationParam.degreePlus * plusOrMinus + "deg)");

                /* Animate command for recursive animation */
                animationID = window.requestAnimationFrame(function () {
                    animateToggle(plusOrMinus);
                });
            }
        }

        /* flag = false indicates arrow must be rotated towards left */
        else {

            /* 
            Stop Condition
            rotate = 0deg
            */
            if (animationParam.degreeMinus <= 0 && animationParam.degreePlus <= 0) {
                window.cancelAnimationFrame(animationID);
            }


            /* Rotate the arrow */
            else {

                /* 
                Rate of rotation 
                Math.random() function is used to set a random rate of rotation
                Use of definite values is acceptable
                */
                // animationParam.degree -= Math.floor(Math.random() * 10); /* Any Number */
                animationParam.degreeMinus -= animationParam.rate; /* Any Number */
                animationParam.degreePlus -= animationParam.rate * 1.5; /* Any Number */

                /* Prevent degree of rotation below 0 */
                if (animationParam.degreeMinus < 0) animationParam.degreeMinus = 0;
                if (animationParam.degreePlus < 0) animationParam.degreePlus = 0;

                /* Set the corresponding value in StyleSheet */
                CSSSheet.cssRules[ruleCSS[0]].style.setProperty("transform", "rotate(" + animationParam.degreeMinus * plusOrMinus * -1 + "deg)");
                CSSSheet.cssRules[ruleCSS[1]].style.setProperty("transform", "rotate(" + animationParam.degreePlus * plusOrMinus * -1 + "deg)");

                /* Animate command for recursive animation */
                animationID = window.requestAnimationFrame(function () {
                    animateToggle(plusOrMinus);
                });
            }
        }
    }

    /* End: Using requestAnimationFrame */

    /* Animate on click using animate() function */
    // svgParentElement.addEventListener("click", function () {
    // toggleAnimation();
    // }, false);

    /* Animate on click using window requestAnimationFrame */
    svgParentElement.addEventListener("click", function () {
        window.requestAnimationFrame(function () {
            var plusOrMinus = Math.random() > 0.5 ? 1 : -1;
            animateToggle(plusOrMinus);
        });
        flag = !flag;
    }, false);

    /* 
    Animate on hover using animate() function
    Obsolete in this case
    */
    // svgParentElement.addEventListener("mouseenter", function () {
    // arrowsAnimation();
    // }, false);
    // svgParentElement.addEventListener("mouseleave", function () {
    // arrowsAnimation();
    // }, false);

    /* 
    Animate on hover using requestAnimationFrame
    Obsolete in this case
    */
    // svgParentElement.addEventListener("mouseenter", function () {
    // window.requestAnimationFrame(animateArrows);
    // flag = !flag;
    // }, false);
    // svgParentElement.addEventListener("mouseleave", function () {
    // window.requestAnimationFrame(animateArrows);
    // flag = !flag;
    // }, false);

}

document.addEventListener("DOMContentLoaded", function () {
    "use strict";
    toggleReady();
}, false);
