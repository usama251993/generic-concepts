function arrowsReady() {
    "use strict";
    var
        svgParentElement = document.querySelectorAll("svg#arrows-svg")[0],
        svgChildElement = svgParentElement.querySelectorAll("svg")[0],
        pathElements = svgChildElement.querySelectorAll("path[id*=\"-line\"]"),
        flag = false,
        animationID = 0,
        CSSSheet = document.querySelectorAll("link[href*=\"SVGToggle.css\"]")[0].sheet,
        ruleCSS = [],
        animationParam = {},
        i;

    /* Start: Using JavaScript animate() function */

    // function options(duration, fill) {
    // /*
    // Reference:
    // https://developer.mozilla.org/en-US/docs/Web/API/Element/animate
    // */
    // return {
    // delay: 0,
    // direction: "normal",
    // duration: duration,
    // easing: "linear",
    // endDelay: 0,
    // fill: fill,
    // iterationStart: 0.0,
    // iterations: 1
    // };
    // }
    //
    // function arrowsAnimation() {
    // if (flag) {
    // /* 
    // flag = true indicates hamburger is expanded 
    // So close the hamburger and change the flag to false
    // */
    // arrowsToLeft();
    // flag = !flag;
    // } else {
    // /* 
    // flag = false indicates hamburger is collapsed 
    // So open the hamburger and change the flag to true
    // */
    // arrowsToRight();
    // flag = !flag;
    // }
    // }
    //
    // function arrowsToLeft() {
    // pathElements[0].animate([{
    // transform: "rotate(90deg)",
    // opacity: 1,
    // offset: 0
    // }, {
    // transform: "rotate(0deg)",
    // opacity: 1,
    // offset: 1
    // }], options(750, "forwards"));
    //
    // pathElements[1].animate([{
    // transform: "rotate(-90deg)",
    // opacity: 1,
    // offset: 0
    // }, {
    // transform: "rotate(0deg)",
    // opacity: 1,
    // offset: 1
    // }], options(750, "forwards"));
    // }
    //
    // function arrowsToRight() {
    // pathElements[0].animate([{
    // transform: "rotate(0deg)",
    // offset: 0
    // }, {
    // transform: "rotate(90deg)",
    // offset: 1
    // }], options(500, "forwards"));
    //
    // pathElements[1].animate([{
    // transform: "rotate(0deg)",
    // opacity: 1,
    // offset: 0
    // }, {
    // transform: "rotate(-90deg)",
    // opacity: 1,
    // offset: 1
    // }], options(500, "forwards"));
    // }

    /* End: Using JavaScript animate() function */

    /* Start: Using requestAnimationFrame */

    /* Set the indices of CSS Rules according to the CSS Stylesheet */
    for (i = 0; i < CSSSheet.cssRules.length; i += 1) {
        if(CSSSheet.cssRules[i].selectorText.indexOf("path#arrow-") > -1)
            ruleCSS[i] = i;
    }
    
    /* Filter out the undefined entries in the array */
    ruleCSS = ruleCSS.filter(function(elements) {
        return elements != undefined;
    });
    
    /* 
    Set the initial animation parameters 
    Degree of rotation set to 0
    */
    animationParam.degree = 0;

    function animateArrows() {
        
        /* flag = true indicates arrow must be rotated towards right */
        if (flag) {
            
            /* 
            Stop Condition
            rotate = 180deg
            */
            if (animationParam.degree >= 180) {
                window.cancelAnimationFrame(animationID);
            } 
            
            /* Rotate the arrow */
            else {
                
                /* 
                Rate of rotation 
                Math.random() function is used to set a random rate of rotation
                Use of definite values is acceptable
                */
                animationParam.degree += Math.floor(Math.random() * 10); /* Any Number */
                
                /* Prevent degree of rotation above 180 */
                if (animationParam.degree > 180) animationParam.degree = 180;
                
                /* Set the corresponding value in StyleSheet */
                CSSSheet.cssRules[ruleCSS[0]].style.setProperty("transform", "rotateY(" + animationParam.degree * 1 + "deg)");
                
                /* Animate command for recursive animation */
                animationID = window.requestAnimationFrame(function () {
                    animateArrows();
                });
            }
        } 
        
        /* flag = false indicates arrow must be rotated towards left */
        else {
            
            /* 
            Stop Condition
            rotate = 0deg
            */
            if (animationParam.degree <= 0) {
                window.cancelAnimationFrame(animationID);
            } 
            
            /* Rotate the arrow */
            else {
                
                /* 
                Rate of rotation 
                Math.random() function is used to set a random rate of rotation
                Use of definite values is acceptable
                */
                animationParam.degree -= Math.floor(Math.random() * 10); /* Any Number */
                
                /* Prevent degree of rotation below 0 */
                if (animationParam.degree < 0) animationParam.degree = 0;
                
                /* Set the corresponding value in StyleSheet */
                CSSSheet.cssRules[ruleCSS[0]].style.setProperty("transform", "rotateY(" + animationParam.degree + "deg)");
                
                /* Animate command for recursive animation */
                animationID = window.requestAnimationFrame(function () {
                    animateArrows();
                });
            }
        }
    }

    /* End: Using requestAnimationFrame */

    /* Animate on click using animate() function */
    // svgParentElement.addEventListener("click", function () {
    // arrowsAnimation();
    // }, false);

    /* Animate on click using window requestAnimationFrame */
    svgParentElement.addEventListener("click", function () {
        animationID = window.requestAnimationFrame(function () {
            animateArrows();
        });
        flag = !flag;
    }, false);

    /* 
    Animate on hover using animate() function
    Obsolete in this case
    */
    // svgParentElement.addEventListener("mouseenter", function () {
    // arrowsAnimation();
    // }, false);
    // svgParentElement.addEventListener("mouseleave", function () {
    // arrowsAnimation();
    // }, false);

    /* 
    Animate on hover using requestAnimationFrame
    Obsolete in this case
    */
    // svgParentElement.addEventListener("mouseenter", function () {
    // window.requestAnimationFrame(animateArrows);
    // flag = !flag;
    // }, false);
    // svgParentElement.addEventListener("mouseleave", function () {
    // window.requestAnimationFrame(animateArrows);
    // flag = !flag;
    // }, false);

}

document.addEventListener("DOMContentLoaded", function () {
    "use strict";
    arrowsReady();
}, false);
