function wifiReady() {
    "use strict";

    var
        svgParentElement = document.querySelectorAll("svg#wifi-svg")[0],
        svgChildElement = svgParentElement.querySelectorAll("svg")[0],
        pathElements = svgChildElement.querySelectorAll("path[id*=\"wifi-\"]"),
        flag = false,
        i;

    function options(delay, duration, fill) {
        /*
        Reference:
        https://developer.mozilla.org/en-US/docs/Web/API/Element/animate
        */
        return {
            delay: delay,
            direction: "normal",
            duration: duration,
            easing: "linear",
            endDelay: 0,
            fill: fill,
            iterationStart: 0.0,
            iterations: 1
        };
    }

    function wifiToggle() {
        if (flag) {
            /* 
            flag = true indicates Wifi is ON
            So turn the Wifi OFF and change the flag to false
            */
            wifiOff();
            flag = !flag;
        } else {
            /* 
            flag = false indicates Wifi is OFF
            So turn the Wifi ON and change the flag to true
            */
            wifiOn();
            flag = !flag;
        }
    }

    // function wifiSearching() {}

    function getColor(index) {
        switch (index) {
            case 0:
                return "rgba(234, 67, 53, 1)";
            case 1:
                return "rgba(251, 188, 5, 1)";
            case 2:
                return "rgba(52, 168, 83, 1)";
            case 3:
                return "rgba(66, 133, 244, 1)";
        }
    }
    
    function wifiOn() {        
        for (i = 0; i < pathElements.length; i += 1) {
            pathElements[i].animate([{
                strokeDasharray: pathElements[i].getTotalLength(),
                strokeDashoffset: 0,
                stroke: "rgba(0, 0, 0, 1)",
                strokeWidth: "1px",
                fill: "rgba(0, 0, 0, 0)",
                offset: 0
            }, {
                strokeDasharray: pathElements[i].getTotalLength(),
                strokeDashoffset: pathElements[i].getTotalLength(),
                stroke: "none",
                strokeWidth: "0px",
                fill: getColor(i),
                offset: 1
            }], options(i * 250, 250, "forwards"));
            // pathElements[i].animate([{
            // fill: "rgba(0, 0, 0, 0)",
            // offset: 0
            // }, {
            // fill: "rgba(0, 0, 0, 1)",
            // offset: 1
            // }], options((i + 1) * 250, 250, "forwards"));
        }
    }

    function wifiOff() {
        for (i = 0; i < pathElements.length; i += 1) {
            pathElements[pathElements.length - 1 - i].animate([{
                strokeDasharray: pathElements[pathElements.length - 1 - i].getTotalLength(),
                strokeDashoffset: pathElements[pathElements.length - 1 - i].getTotalLength(),
                stroke: "none",
                strokeWidth: "0px",
                fill: getColor((pathElements.length - 1 - i)),
                offset: 0
            }, {
                strokeDasharray: pathElements[pathElements.length - 1 - i].getTotalLength(),
                strokeDashoffset: 0,
                stroke: "rgba(0, 0, 0, 1)",
                strokeWidth: "1px",
                fill: "rgba(0, 0, 0, 0)",
                offset: 1
            }], options(i * 250, 250, "forwards"));
        }
    }

    /* Toggle on click */
    svgParentElement.addEventListener("click", function () {
        wifiToggle();
    }, false);
}

document.addEventListener("DOMContentLoaded", function () {
    wifiReady();
}, false);
