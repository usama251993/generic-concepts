function hamburgerReady() {

	"use strict";

	var
		svgParentElement = document.querySelectorAll("svg#hamburger-svg")[0],
		svgChildElement = svgParentElement.querySelectorAll("svg")[0],
		pathElements = svgChildElement.querySelectorAll("path[id*=\"hamburger-line-\"]"),
		translateDiff = "",
		CSSSheet = document.querySelectorAll("link[href*=\"SVGToggle.css\"]")[0].sheet,
		ruleCSS = [],
		animationParam = {},
		animationID = 0,
		flag = false,
		i;

	/* 
	Calculate the translation distance
	translateDiff = distance between two adjacent paths of SVG Hamburger
	*/
	translateDiff = pathElements[1].getAttribute("d").split(" ", 3)[2] - pathElements[0].getAttribute("d").split(" ", 3)[2];

	/* Start: Using JavaScript animate() function */

	// /* Function to define animation parameters */
	// function options(duration, fill) {
	// /*
	// Reference:
	// https://developer.mozilla.org/en-US/docs/Web/API/Element/animate
	// */
	// return {
	// delay: 0,
	// direction: "normal",
	// duration: duration,
	// easing: "linear",
	// endDelay: 0,
	// fill: fill,
	// iterationStart: 0.0,
	// iterations: 1
	// };
	// }
	//
	// /* Function to perform SVG animation */
	// function hamburgerAnimation() {
	//
	// /* Random +1 and -1 generator */
	// var plusOrMinus = Math.random() < 0.5 ? -1 : 1;
	// if (flag) {
	// /* 
	// flag = true indicates hamburger is expanded 
	// So close the hamburger and change the flag to false
	// */
	// closeHamburger(plusOrMinus);
	// flag = !flag;
	// } else {
	// /* 
	// flag = false indicates hamburger is collapsed 
	// So open the hamburger and change the flag to true
	// */
	// openHamburger(plusOrMinus);
	// flag = !flag;
	// }
	// }
	//
	// /* Open the hamburger */
	// function openHamburger(plusOrMinus) {
	//
	// /* 
	// Animate the first horizontal line of hamburger 
	// Translate below by the difference calculated
	// Rotate by 45degree
	// Scale the length to 66.67%
	// */
	// pathElements[0].animate([{
	// transform: "translate(0px, 0px) rotate(0deg) scale(1, 1)",
	// offset: 0
	// }, {
	// transform: "translate(0px, " + translateDiff + "px) rotate(" + plusOrMinus * 45 + "deg) scale(" + 2 / 3 + ", 1)",
	// offset: 1
	// }], options(125, "forwards"));
	//
	// /* 
	// Animate the second horizontal line of hamburger 
	// Shrink the length until disappeared
	// */
	// pathElements[1].animate([{
	// transform: "scale(1, 1)",
	// offset: 0
	// }, {
	// transform: "scale(0, 1)",
	// offset: 1
	// }], options(125, "forwards"));
	//
	// /* 
	// Animate the third horizontal line of hamburger
	// Translate above by the difference calculated
	// Rotate by -45degree
	// Scale the length to 66.67%
	// */
	// pathElements[2].animate([{
	// transform: "translate(0px, 0px) rotate(0deg) scale(1, 1)",
	// offset: 0
	// }, {
	// transform: "translate(0px, " + translateDiff * -1 + "px) rotate(" + plusOrMinus * -45 + "deg) scale(" + 2 / 3 + ", 1)",
	// offset: 1
	// }], options(125, "forwards"));
	// }
	//
	// /* Close the hamburger */
	// function closeHamburger(plusOrMinus) {
	//
	// /* Revert the changes made by animation of first line of hamburger */
	// pathElements[0].animate([{
	// transform: "translate(0px, " + translateDiff + "px) rotate(" + plusOrMinus * 45 + "deg) scale(" + 2 / 3 + ", 1)",
	// offset: 0
	// }, {
	// transform: "translate(0px, 0px) rotate(" + plusOrMinus * 180 + "deg) scale(1, 1)",
	// offset: 1
	// }], options(125, "forwards"));
	//
	// /* Revert the changes made by animation of second line of hamburger */
	// pathElements[1].animate([{
	// transform: "scale(0, 1)",
	// offset: 0
	// }, {
	// transform: "scale(1, 1)",
	// offset: 1
	// }], options(125, "forwards"));
	//
	// /* Revert the changes made by animation of third line of hamburger */
	// pathElements[2].animate([{
	// transform: "translate(0px, " + translateDiff * -1 + "px) rotate(" + plusOrMinus * -45 + "deg) scale(" + 2 / 3 + ", 1)",
	// offset: 0
	// }, {
	// transform: "translate(0px, 0px) rotate(" + plusOrMinus * -180 + "deg) scale(1, 1)",
	// offset: 1
	// }], options(125, "forwards"));
	// }

	/* End: Using JavaScript animate() function */

	/* Start: Using requestAnimationFrame */

	/* Set the indices of CSS Rules according to the CSS Stylesheet */
	for (i = 0; i < CSSSheet.cssRules.length; i += 1) {
		if (CSSSheet.cssRules[i].selectorText.indexOf("path#hamburger-") > -1)
			ruleCSS[i] = i;
	}

	/* Filter out the undefined entries in the array */
	ruleCSS = ruleCSS.filter(function (elements) {
		return elements != undefined;
	});

	/* 
	Set the initial animation parameters 
	translate = 0px
	rotate = 0deg
	scale = 1x
	*/

	animationParam.translate = 0;
	animationParam.rotate = 0;
	animationParam.scaleCenter = 1;
	animationParam.scaleSides = 1;


	function animateHamburger() {

		/* For randomization in animation */
		var plusOrMinus = Math.random() > 0.5 ? 1 : -1;

		/* flag = true indicates hamburger must be toggled */
		if (flag) {

			/*
			Stop Condition
			translate = 0px, +/-translateDiff
			rotate = 45deg
			scale = 0 // for middle line
			scale = 66.67% // for the upper and lower lines
			*/
			if (
				animationParam.translate === translateDiff &&
				animationParam.rotate === 45 &&
				animationParam.scaleCenter === 0 &&
				animationParam.scaleSides === 2 / 3
			) {
				window.cancelAnimationFrame(animationID);
			}

			/* Toggle the hamburger */
			else {

				/* Increase/decrease animation paramteres according to requirements */
				animationParam.translate += Math.random() * 10;
				animationParam.rotate += 5;
				animationParam.scaleCenter -= Math.random();
				animationParam.scaleSides -= Math.random() / 10;

				/* Set the limits of increment/decrement */
				if (animationParam.translate > translateDiff) animationParam.translate = translateDiff;
				if (animationParam.rotate > 45) animationParam.rotate = 45;
				if (animationParam.scaleCenter < 0) animationParam.scaleCenter = 0;
				if (animationParam.scaleSides < 2 / 3) animationParam.scaleSides = 2 / 3;

				/* Set the modified parameters in the StyleSheet */
				CSSSheet.cssRules[ruleCSS[0]].style.setProperty("transform", "translate(0px, " + animationParam.translate + "px) rotate(" + animationParam.rotate * plusOrMinus + "deg) scale(" + animationParam.scaleSides + ", 1)");
				CSSSheet.cssRules[ruleCSS[1]].style.setProperty("transform", "scale(" + animationParam.scaleCenter + ", 1)");
				CSSSheet.cssRules[ruleCSS[2]].style.setProperty("transform", "translate(0px, " + animationParam.translate * -1 + "px) rotate(" + animationParam.rotate * plusOrMinus * -1 + "deg) scale(" + animationParam.scaleSides + ", 1)");

				/* Command for animation */
				animationID = window.requestAnimationFrame(function () {
					animateHamburger();
				});
			}
		}

		/* flag = false indicates hamburger must be retoggled */
		else {

			/*
			Stop Condition
			translate = 0px, 0px
			rotate = 0deg
			scale = 1
			*/
			if (
				animationParam.translate === 0 &&
				animationParam.rotate === 0 &&
				animationParam.scaleCenter === 1 &&
				animationParam.scaleSides === 1
			) {
				window.cancelAnimationFrame(animationID);
			} else {

				/* Increment/decrement */
				animationParam.translate -= Math.random() * 10;
				animationParam.rotate -= 5;
				animationParam.scaleCenter += Math.random();
				animationParam.scaleSides += Math.random() / 10;

				/* Limitation */
				if (animationParam.translate < 0) animationParam.translate = 0;
				if (animationParam.rotate < 0) animationParam.rotate = 0;
				if (animationParam.scaleCenter > 1) animationParam.scaleCenter = 1;
				if (animationParam.scaleSides > 1) animationParam.scaleSides = 1;

				/* Settings */
				CSSSheet.cssRules[ruleCSS[0]].style.setProperty("transform", "translate(0px, " + animationParam.translate + "px) rotate(" + animationParam.rotate * plusOrMinus + "deg) scale(" + animationParam.scaleSides + ", 1)");
				CSSSheet.cssRules[ruleCSS[1]].style.setProperty("transform", "scale(" + animationParam.scaleCenter + ", 1)");
				CSSSheet.cssRules[ruleCSS[2]].style.setProperty("transform", "translate(0px, " + animationParam.translate * -1 + "px) rotate(" + animationParam.rotate * plusOrMinus * -1 + "deg) scale(" + animationParam.scaleSides + ", 1)");

				/* Command */
				animationID = window.requestAnimationFrame(function () {
					animateHamburger();
				});
			}
		}
	}

	/* End: Using requestAnimationFrame */

	/* Event Listeners */

	/* Animate on click using JavaScript animate() function */
	// svgParentElement.addEventListener("click", function () {
	// hamburgerAnimation();
	// }, false);

	/* Animate on click using requestAnimationFrame */
	svgParentElement.addEventListener("mousedown", function () {
		window.requestAnimationFrame(function () {
			animateHamburger();
		});
		flag = !flag;
	}, false);

	svgParentElement.addEventListener("mouseup", function () {
		window.requestAnimationFrame(function () {
			animateHamburger();
		});
		flag = !flag;
	}, false);

	//svgParentElement.addEventListener("click", function () {
	//window.requestAnimationFrame(function () {
	//animateHamburger();
	//});
	//flag = !flag;
	//}, false);

	svgParentElement.addEventListener("contextmenu", function (event) {
		event.preventDefault();
		window.requestAnimationFrame(function () {
			animateHamburger();
		});
		flag = !flag;
	}, false);

	/* Animate on hover using JavaScript animate() function */
	// svgParentElement.addEventListener("mouseenter", function () {
	// hamburgerAnimation();
	// }, false);
	// svgParentElement.addEventListener("mouseleave", function () {
	// hamburgerAnimation();
	// }, false);

	/* Animate on hover using requestAnimationFrame */
	// svgParentElement.addEventListener("mouseenter", function () {
	// window.requestAnimationFrame(function () {
	// animateHamburger();
	// });
	// flag = !flag;
	// }, false);
	// svgParentElement.addEventListener("mouseleave", function () {
	// window.requestAnimationFrame(function () {
	// animateHamburger();
	// });
	// flag = !flag;
	// }, false);
}

document.addEventListener("DOMContentLoaded", function () {
	"use strict";
	hamburgerReady();
}, false);
